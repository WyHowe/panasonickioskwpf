﻿using System.Windows.Controls;
using System.Windows.Input;

namespace PanasonicKiosk.Modules.SidePanel.Views
{
    /// <summary>
    /// Interaction logic for ViewA.xaml
    /// </summary>
    public partial class SidePanelBar : UserControl
    {
        public SidePanelBar()
        {
            InitializeComponent();
            //DataContext = vm;
        }

        private void animatedScrollViewer_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}