﻿using PanasonicKiosk.Core.Events;
using PanasonicKiosk.Core.Models;
using Prism.Commands;
using Prism.Events;
using Prism.Modularity;
using Prism.Mvvm;
using Prism.Regions;
using System.Windows.Input;

namespace PanasonicKiosk.Modules.Banner.ViewModels
{
    public class BannerViewModel : BindableBase, INavigationAware
    {
        private IModuleManager _moduleManager;
        private IEventAggregator _eventAggregator;

        private DelegateCommand<CampaignDetailViewModel> loadProductCommand;
        private IRegionManager _regionManager;
        private string _message;

        public string Message
        {
            get { return _message; }
            set { SetProperty(ref _message, value); }
        }
        public ICommand LoadProductListingCommand
        {
            get
            {
                return loadProductCommand;
            }
        }

        private bool canLoadProductScreen(CampaignDetailViewModel Campaign)
        {
            return true;
        }

        private void loadProductListing(CampaignDetailViewModel Campaign)
        {
            _eventAggregator.GetEvent<GetItemsByCampaignEvent>().Publish(Campaign);
        }
        public BannerViewModel(IEventAggregator eventAggregator)
        {
            this._eventAggregator = eventAggregator;
            loadProductCommand = new DelegateCommand<CampaignDetailViewModel>(loadProductListing, canLoadProductScreen);
            Message = "View A from your Prism Module";
        }
        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            throw new System.NotImplementedException();
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            throw new System.NotImplementedException();
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            throw new System.NotImplementedException();
        }
    }
}