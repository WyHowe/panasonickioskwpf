﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Runtime.CompilerServices;

namespace PanasonicKiosk.Core.K9Access
{
    public class K9SerialPortService
    {
        public K9SerialPortService()
        {
            serialPort = new SerialPort();
        }

        private SerialPort _serialPort;

        private SerialPort serialPort
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _serialPort;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_serialPort != null)
                {
                    _serialPort.DataReceived -= serialPort_DataReceived;
                }

                _serialPort = value;
                if (_serialPort != null)
                {
                    _serialPort.DataReceived += serialPort_DataReceived;
                }
            }
        }

        private string mMessage = "";
        private string IncomingData = "";

        // Public Event Receive As EventHandler
        public event ReceiveEventHandler Receive;

        public delegate void ReceiveEventHandler(string msg);

        public int BaudRate
        {
            get
            {
                return serialPort.BaudRate;
            }

            set
            {
                serialPort.BaudRate = value;
            }
        }

        public int PortNumber
        {
            get
            {
                return int.Parse(serialPort.PortName);
            }

            set
            {
                string port = "COM" + value.ToString();
                serialPort.PortName = port;
            }
        }

        public int Parity
        {
            get
            {
                return (int)serialPort.Parity;
            }

            set
            {
                switch (value)
                {
                    case 0:
                        {
                            serialPort.Parity = System.IO.Ports.Parity.None;
                            break;
                        }

                    case 1:
                        {
                            serialPort.Parity = System.IO.Ports.Parity.Odd;
                            break;
                        }

                    case 2:
                        {
                            serialPort.Parity = System.IO.Ports.Parity.Even;
                            break;
                        }

                    case 3:
                        {
                            serialPort.Parity = System.IO.Ports.Parity.Mark;
                            break;
                        }

                    case 4:
                        {
                            serialPort.Parity = System.IO.Ports.Parity.Space;
                            break;
                        }
                }
            }
        }

        public int DataBits
        {
            get
            {
                return serialPort.DataBits;
            }

            set
            {
                serialPort.DataBits = value;
            }
        }

        public int StopBits
        {
            get
            {
                return (int)serialPort.StopBits;
            }

            set
            {
                switch (value)
                {
                    case 0:
                        {
                            serialPort.StopBits = System.IO.Ports.StopBits.None;
                            break;
                        }

                    case 1:
                        {
                            serialPort.StopBits = System.IO.Ports.StopBits.One;
                            break;
                        }

                    case 2:
                        {
                            serialPort.StopBits = System.IO.Ports.StopBits.Two;
                            break;
                        }

                    case 3:
                        {
                            serialPort.StopBits = System.IO.Ports.StopBits.OnePointFive;
                            break;
                        }
                }
            }
        }

        public bool IsOpen
        {
            get
            {
                return serialPort.IsOpen;
            }
        }

        public string Message
        {
            get
            {
                return mMessage;
            }
        }

        public bool Connect(ref string result)
        {
            try
            {
                if (serialPort.IsOpen)
                {
                    serialPort.Close();
                }

                serialPort.Open();
                result = "";
                return true;
            }
            catch (Exception ex)
            {
                result = ex.Message;
                return false;
            }
        }

        public bool Disconnect()
        {
            try
            {
                if (serialPort.IsOpen)
                {
                    serialPort.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Send(string txtMsg)
        {
            try
            {
                txtMsg = txtMsg.Replace(" ", "");
                var newMsg = HexToByte(txtMsg);
                serialPort.Write(newMsg, 0, newMsg.Length);
                System.Threading.Thread.Sleep(100);
                return true;
            }
            catch (Exception ex)
            {
                // MsgBox(ex.Message)
                return false;
            }
        }

        private void serialPort_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            try
            {
                IncomingData = "";
                int Count = serialPort.BytesToRead;
                var Buffer = new byte[Count];
                serialPort.Read(Buffer, 0, Count);
                for (int i = 0, loopTo = Buffer.GetUpperBound(0); i <= loopTo; i++)
                    IncomingData += Buffer[i].ToString("X2");
                Receive?.Invoke(IncomingData);
            }
            catch (TimeoutException t)
            {
                Receive?.Invoke(t.Message);
            }
            catch (Exception ex)
            {
                Receive?.Invoke(ex.Message);
            }
        }

        public List<string> GetPorts()
        {
            var random = SerialPort.GetPortNames();
            var PortNames = new List<string>();
            var FixedTerminalPort = Properties.Settings.Default.DefaultTerminalPort;
            PortNames.Add("COM" + FixedTerminalPort);
            //for (int i = 0, loopTo = SerialPort.GetPortNames().Length - 1; i <= loopTo; i++)
            //{
            //    PortNames.Add(SerialPort.GetPortNames()[i]);
            //}

            return PortNames;
        }

        private byte[] HexToByte(string msg)
        {
            if (msg.Length % 2 == 0)
            {
                msg = msg.Replace(" ", "");
                var comBuffer = new byte[(int)Math.Round(msg.Length / 2d - 1d + 1)];
                for (int i = 0, loopTo = msg.Length - 1; i <= loopTo; i += 2)
                    comBuffer[(int)Math.Round(i / 2d)] = Convert.ToByte(msg.Substring(i, 2), 16);
                return comBuffer;
            }
            else
            {
                return null;
            }
        }
    }
}