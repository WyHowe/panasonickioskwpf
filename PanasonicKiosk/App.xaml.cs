﻿using PanasonicKiosk.Clients;
using PanasonicKiosk.Clients.SignalRDataProvider.DataProvider;
using PanasonicKiosk.Core.K9Access;
using PanasonicKiosk.Core.Service;
using PanasonicKiosk.Core.Sunmi;
using PanasonicKiosk.Core.Sunmi.Interface;
using PanasonicKiosk.Modules.Banner;
using PanasonicKiosk.Modules.Checkout;
using PanasonicKiosk.Modules.ClearConfirmation;
using PanasonicKiosk.Modules.Footer;
using PanasonicKiosk.Modules.IdleConfirmation;
using PanasonicKiosk.Modules.Landing;
using PanasonicKiosk.Modules.LoadingScreen;
using PanasonicKiosk.Modules.OrderSuccess;
using PanasonicKiosk.Modules.ProductDetails;
using PanasonicKiosk.Modules.ProductListing;
using PanasonicKiosk.Modules.ShoppingCart;
using PanasonicKiosk.Modules.SidePanel;
using PanasonicKiosk.Modules.SubCategoryPanel;
using PanasonicKiosk.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using System.Windows;

namespace PanasonicKiosk
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.Register<ISidePanelService, SidePanelService>();
            containerRegistry.Register<IProductService, ProductService>();
            containerRegistry.Register<ISignalRHubSync, SignalRHubSync>();
            containerRegistry.Register<IContext, SignalRContext>();
            containerRegistry.Register<IAPIAccessService, APIAccessService>();
            containerRegistry.Register<IKioskConnectorService, KioskConnectorService>();
            containerRegistry.Register<ISummiSocketService, SummiSocketService>();
            containerRegistry.Register<IK9CommandService, K9CommandService>();
            containerRegistry.Register<IK9ConnectorService, K9ConnectorService>();
            //containerRegistry.Register<IK9SerialPortService, K9SerialPortService>();
            //containerRegistry.Register<IEmployeeService, EmployeeService>(new ContainerControlledLifetimeManager());
        }

        protected override void ConfigureModuleCatalog(IModuleCatalog moduleCatalog)
        {
            moduleCatalog.AddModule<LandingModule>();
            //moduleCatalog.AddModule<IdleConfirmationModule>();
            moduleCatalog.AddModule<BannerModule>();
            moduleCatalog.AddModule<ProductListingModule>();
            moduleCatalog.AddModule<SidePanelModule>();
            moduleCatalog.AddModule<FooterModule>();
            moduleCatalog.AddModule<SubCategoryPanelModule>();
            moduleCatalog.AddModule<LoadingScreenModule>();
            moduleCatalog.AddModule<ProductDetailsModule>();
            moduleCatalog.AddModule<ShoppingCartModule>();
            moduleCatalog.AddModule<CheckoutModule>();
            moduleCatalog.AddModule<IdleConfirmationModule>(InitializationMode.OnDemand);
            moduleCatalog.AddModule<ClearConfirmationModule>(InitializationMode.OnDemand);
            moduleCatalog.AddModule<OrderSuccessModule>(InitializationMode.OnDemand);
            //moduleCatalog.AddModule<ShoppingCartModule>(InitializationMode.OnDemand);
            //moduleCatalog.AddModule<CheckoutModule>(InitializationMode.OnDemand);

            //moduleCatalog.AddModule(typeof(SoftwareModule), InitializationMode.OnDemand);
            //moduleCatalog.AddModule(typeof(HardwareModule), InitializationMode.OnDemand);
            //moduleCatalog.AddModule(typeof(RequestModule), InitializationMode.OnDemand);
        }

        //for custome region adapter no use for now.
        protected override void ConfigureRegionAdapterMappings(RegionAdapterMappings regionAdapterMappings)
        {
            base.ConfigureRegionAdapterMappings(regionAdapterMappings);
        }
    }
}