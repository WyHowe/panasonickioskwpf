﻿using System;
using System.Text;

namespace PanasonicKiosk.Core.Utility
{
    public class K9Utility
    {
        private byte SEQNO = 0;

        public static byte calculate_bcc(byte[] data)
        {
            int iCount;
            byte BCC = 0;
            BCC = data[0];
            var loopTo = data.Length - 1;
            for (iCount = 1; iCount <= loopTo; iCount++)
                BCC = (byte)(BCC ^ data[iCount]);
            return BCC;
        }

        public static string SetCurrentDate()
        {
            string YY = DateTime.Now.ToString("yy");
            string MM = DateTime.Now.ToString("MM");
            string DD = DateTime.Now.ToString("dd");
            string HH = DateTime.Now.ToString("HH");
            string MI = DateTime.Now.ToString("mm");
            string SS = DateTime.Now.ToString("ss");
            return YY + MM + DD + HH + MI + SS;
        }

        public static string GetToday()
        {
            string YY = DateTime.Now.ToString("yy");
            string MM = DateTime.Now.ToString("MM");
            string DD = DateTime.Now.ToString("dd");
            return YY + MM + DD;
        }

        public static string RespGetStatus(string msg)
        {
            string result = "";
            string status = "";
            if (msg.StartsWith("02"))
            {
                status = msg.Substring(8, 2);
                result = status;
            }

            return result;
        }

        public static string RespGetConnectionTest(string msg)
        {
            string result = "";
            string ServerResp = "";
            if (msg.StartsWith("02"))
            {
                ServerResp = msg.Substring(10, 2);
                result = ServerResp;
            }

            return result;
        }

        public static string RespGetSEQ(string msg)
        {
            string result = "";
            string len = msg.Substring(2, 4);
            string status = msg.Substring(8, 2);
            if (msg.StartsWith("02"))
            {
                result = msg.Substring(6, 2);
            }

            return result;
        }

        public static string RespGetCard(string cmd, string data, ref string SALE_TID, ref string SALE_DT, ref string MASK_PAN, ref string HASH_PAN, ref string RRN, ref string STAN, ref string APPR_CODE, ref string ADD_DATA)
        {
            data = data.Trim();
            string respcode = data.Substring(8, 2);
            SALE_TID = HexToString(data.Substring(10, 16));
            SALE_DT = data.Substring(26, 12);
            SALE_DT = "20" + SALE_DT.Substring(0, 2) + "-" + SALE_DT.Substring(2, 2) + "-" + SALE_DT.Substring(4, 2) + " " + SALE_DT.Substring(6, 2) + ":" + SALE_DT.Substring(8, 2) + ":" + SALE_DT.Substring(10, 2);
            MASK_PAN = data.Substring(38, 20 - 4); // TnG = 20 'CC/DC = 16
            HASH_PAN = HexToString(data.Substring(58, 80));
            if (double.Parse(cmd) == 22d)
            {
                RRN = HexToString(data.Substring(138, 24));
                STAN = data.Substring(162, 6);
                APPR_CODE = HexToString(data.Substring(168, 12));
                string adddata = data.Substring(180);
                ADD_DATA = adddata.Substring(0, adddata.Length - 4);
            }
            else
            {
                RRN = "";
                STAN = "";
                APPR_CODE = "";
                ADD_DATA = "";
            }

            return respcode;
        }

        public static string HexToString(string hex)
        {
            var text = new StringBuilder(hex.Length / 2);
            for (int i = 0, loopTo = hex.Length - 2; i <= loopTo; i += 2)
                text.Append((char)Convert.ToByte(hex.Substring(i, 2), 16));
            return text.ToString();
        }

        public static bool arraycompare(byte[] data1, int data1offset, byte[] data2, int data2offset, int len)
        {
            int i;
            for (i = 0; i < len; i++)
            {
                if (data1[data1offset + i] != data2[data2offset + i])
                    return false;
            }

            return true;
        }

        public static int arraycopy(byte[] source, int soureoffset, byte[] dest, int destoffset, int len)
        {
            System.Array.Copy(source, soureoffset, dest, destoffset, len);
            return destoffset + len;
        }

        public static int arrayfill(byte data, byte[] dest, int destoffset, int len)
        {
            while (len-- > 0)
            {
                dest[destoffset++] = data;
            }

            return destoffset;
        }

        public static int setushort(ushort value, byte[] destbuf, int destbufoffset)
        {
            destbuf[destbufoffset++] = (byte)((value & 0xFF00) >> 8);
            destbuf[destbufoffset++] = (byte)(value & 0x00FF);
            return destbufoffset;
        }

        public static ushort getushort(byte[] data, int dataoffset)
        {
            ushort usResp = 0;

            usResp = data[dataoffset++];
            usResp = (ushort)(usResp << 8);
            usResp += data[dataoffset++];

            return usResp;
        }

        public static int setuint(uint value, byte[] destbuf, int destbufoffset)
        {
            int size = 4;

            while (size-- > 0)
            {
                destbuf[destbufoffset + size] = (byte)(value & 0x000000FF);
                value >>= 8;
            }

            return destbufoffset + 4;
        }

        public static uint getuint(byte[] data, int dataoffset)
        {
            uint usResp = 0;
            int size = 4;

            while (size-- > 0)
            {
                usResp = (uint)(usResp << 8);
                usResp += data[dataoffset++];
            }

            return usResp;
        }

        //public static bool AskYesOrNo(string Contents, string Title)
        //{
        //    if (DialogResult.Yes == MessageBox.Show(Contents + "?", Title, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
        //        return true;
        //    else
        //        return false;
        //}

        //public static void ShowErrorMessge(string Contents, string Title)
        //{
        //    MessageBox.Show(Contents, Title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        //}

        //public static void ShowExclaimMessge(string Contents, string Title)
        //{
        //    MessageBox.Show(Contents, Title, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        //}

        //public static void ShowInfoMessge(string Contents, string Title)
        //{
        //    MessageBox.Show(Contents, Title, MessageBoxButtons.OK, MessageBoxIcon.Information);
        //}

        public static string RemoveWhiteSpace(string InputText)
        {
            InputText = InputText.Trim();
            InputText = InputText.Replace("\t", "");
            return InputText.Replace(" ", "");
        }

        public static string ByteArrayToHexString(byte[] Input, int Offset, int Length, string spacing)
        {
            string strO = "";
            for (int i = Offset; i < Offset + Length; i++)
                strO += String.Format("{0:x2}", (int)Input[i]) + spacing;
            return strO.ToUpper().Trim();
        }

        public static byte[] HexStringToByteArray(string data)
        {
            data = RemoveWhiteSpace(data);

            byte[] endResult = new byte[data.Length / 2];
            for (int i = 0; i < data.Length; i += 2)
            {
                endResult[Math.Abs(i / 2)] = byte.Parse(data.Substring(i, 2), System.Globalization.NumberStyles.HexNumber);
            }
            return endResult;
        }

        public static int HexStringToByteArray(string data, byte[] HexOut, int HexOutOffset)
        {
            int iLen = 0;

            data = RemoveWhiteSpace(data);

            for (int i = 0; i < data.Length; i += 2)
            {
                HexOut[HexOutOffset + iLen] = byte.Parse(data.Substring(i, 2), System.Globalization.NumberStyles.HexNumber);
                iLen += 1;
            }

            return iLen;
        }

        public static byte HexStringToByte(string HexStringByte)
        {
            return byte.Parse(HexStringByte, System.Globalization.NumberStyles.HexNumber);
        }

        public static string ByteToString(byte data)
        {
            return String.Format("{0:x2}", data).ToUpper();
        }

        public static string ByteArrayToAscii(byte[] DataIn, int Offset, int Length)
        {
            return System.Text.ASCIIEncoding.ASCII.GetString(DataIn, Offset, Length);
        }

        public static byte[] ASCiiToByteArray(string DataIn)
        {
            /*
            DataIn = DataIn.Replace("\\n", "\n");
            DataIn = DataIn.Replace("\\r", "\r");
            DataIn = DataIn.Replace("\\t", "\t");
            DataIn = DataIn.Replace("\\0", "\0");
            */

            byte[] byteDataInConverted = System.Text.ASCIIEncoding.ASCII.GetBytes(DataIn);

            return byteDataInConverted;//System.Text.ASCIIEncoding.ASCII.GetBytes(DataIn);
        }

        public static int ASCIIToByteArray(string DataIn, byte[] dest, int destoffset)
        {
            /*
            DataIn = DataIn.Replace("\\n", "\n");
            DataIn = DataIn.Replace("\\r", "\r");
            DataIn = DataIn.Replace("\\t", "\t");
            DataIn = DataIn.Replace("\\0", "\0");
            */

            return System.Text.ASCIIEncoding.ASCII.GetBytes(DataIn, 0, DataIn.Length, dest, destoffset);
        }

        //public static string SaveFile(string StartUpPath, string Extention)
        //{
        //    return SaveFile("", StartUpPath, Extention);
        //}

        //public static string SaveFile(string defaultFileName, string StartUpPath, string Extention)
        //{
        //    string strSaveFileName = "";

        //    SaveFileDialog svd1 = new SaveFileDialog();
        //    if (StartUpPath != null && StartUpPath.Length > 0) svd1.InitialDirectory = StartUpPath;
        //    if (Extention != null && Extention.Length > 0) svd1.Filter = Extention;
        //    if (defaultFileName != null && defaultFileName.Length > 0)
        //        svd1.FileName = defaultFileName;

        //    if (svd1.ShowDialog() == DialogResult.OK)
        //        strSaveFileName = svd1.FileName;

        //    svd1.Dispose();
        //    return strSaveFileName;
        //}

        //public static string SaveFolder(string StartUpPath)
        //{
        //    string strFolderName = "";

        //    FolderBrowserDialog fd1 = new FolderBrowserDialog();

        //    if (StartUpPath != null && StartUpPath.Length > 0) fd1.SelectedPath = StartUpPath;

        //    if (fd1.ShowDialog() == DialogResult.OK)
        //        strFolderName = fd1.SelectedPath;

        //    fd1.Dispose();
        //    return strFolderName;
        //}

        //public static string OpenFile(string StartUpPath, string Extention)
        //{
        //    string strOpenFileName = "";

        //    OpenFileDialog opd1 = new OpenFileDialog();
        //    if (StartUpPath != null && StartUpPath.Length > 0) opd1.InitialDirectory = StartUpPath;
        //    if (Extention != null && Extention.Length > 0) opd1.Filter = Extention;

        //    if (opd1.ShowDialog() == DialogResult.OK)
        //        strOpenFileName = opd1.FileName;

        //    opd1.Dispose();
        //    return strOpenFileName;
        //}

        //public static string[] FileToStringArray(string FileName)
        //{
        //    ArrayList al = new ArrayList();
        //    StreamReader sr = File.OpenText(FileName);
        //    string str = sr.ReadLine();
        //    while (str != null)
        //    {
        //        al.Add(str);
        //        str = sr.ReadLine();
        //    }
        //    sr.Close();
        //    string[] astr = new string[al.Count];
        //    al.CopyTo(astr);
        //    return astr;
        //}

        //public static void ArrayToFile(string FileName, byte[] data, int dataoffset, int datalen)
        //{
        //    FileStream fs = new FileStream(FileName, FileMode.Create, FileAccess.Write);
        //    // Create a Char writer.
        //    StreamWriter w = new StreamWriter(fs);
        //    // Set the StreamWriter file pointer to the end.
        //    w.BaseStream.Seek(0, SeekOrigin.End);
        //    w.BaseStream.Write(data, dataoffset, datalen);

        //    w.Close();
        //    fs.Close();
        //}

        //public static byte[] FileToArray(string FileName)
        //{
        //    FileInfo fInfo = new FileInfo(FileName);
        //    long iFileSize = fInfo.Length;
        //    byte[] bFileBuf = new byte[iFileSize];

        //    StreamReader sr = File.OpenText(FileName);

        //    int iData = sr.BaseStream.Read(bFileBuf, 0, (int)iFileSize);
        //    if (iData < 0)
        //        return null;

        //    sr.Close();

        //    return bFileBuf;
        //}
    }
}