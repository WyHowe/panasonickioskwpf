﻿using System.Windows.Controls;

namespace PanasonicKiosk.Modules.ProductListing.Views
{
    /// <summary>
    /// Interaction logic for ViewA.xaml
    /// </summary>
    public partial class ProductListingView : UserControl
    {
        public ProductListingView()
        {
            InitializeComponent();
            //DataContext = vm;
        }

        private void animatedScrollViewer_ManipulationBoundaryFeedback(object sender, System.Windows.Input.ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}