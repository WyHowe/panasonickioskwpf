﻿using PanasonicKiosk.Core;
using PanasonicKiosk.Modules.IdleConfirmation.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace PanasonicKiosk.Modules.IdleConfirmation
{
    public class IdleConfirmationModule : IModule
    {
        private readonly IRegionManager _regionManager;

        public IdleConfirmationModule(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }

        public void OnInitialized(IContainerProvider containerProvider)
        {
            _regionManager.RegisterViewWithRegion(RegionNames.ConfirmationRegion, typeof(IdleConfirmationView));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            //containerRegistry.RegisterForNavigation<IdleConfirmationView>();
        }
    }
}