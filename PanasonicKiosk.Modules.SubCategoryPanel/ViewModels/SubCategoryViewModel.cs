﻿using PanasonicKiosk.Core;
using PanasonicKiosk.Core.Events;
using PanasonicKiosk.Core.Helper;
using PanasonicKiosk.Core.Models;
using PanasonicKiosk.Core.Service;
using Prism.Commands;
using Prism.Events;
using Prism.Modularity;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Input;

namespace PanasonicKiosk.Modules.SubCategoryPanel.ViewModels
{
    public class SubCategoryViewModel : BindableBase, INavigationAware
    {
        private IProductService _productService;
        private IEventAggregator _eventAggregator;
        private IRegionManager _regionManager;
        private IModuleManager _moduleManager;
        private string _SearchKey;
        private string _CampaignId;
        private string _CampaignName;
        private string _CategoryImageURL = "https://crm.pm.panasonic.com.my/OMSContent/Images/Categories/Actual/";
        public static string DefaultSubCategoryImageLocation = "CategoryImages";
        public static string ApplicatonDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
        public static string SubCategoryImageDirectory = Path.Combine(ApplicatonDirectory, DefaultSubCategoryImageLocation);
        private SidePanelItem CurrentMainCategory;
        private ObservableCollection<SubCategoryDetails> _SubCategories;
        private DelegateCommand<SubCategoryDetails> _loadProductSubCategoryCommand;

        public SubCategoryViewModel(IRegionManager regionManager, IModuleManager moduleManager,
            IProductService productService, IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _productService = productService;
            _moduleManager = moduleManager;
            _regionManager = regionManager;
            _loadProductSubCategoryCommand = new DelegateCommand<SubCategoryDetails>(loadProductSubCategory, canLoadProductSubCategory);

            _eventAggregator.GetEvent<SidePanelBarEvents>().Subscribe(RefreshSubCategoryModel);
            _eventAggregator.GetEvent<SearchItemEvent>().Subscribe(RefreshSubCategoryModelBySearch);
            _eventAggregator.GetEvent<GetItemsByCampaignEvent>().Subscribe(RefreshSubCategoryModelByCampaign);
            _eventAggregator.GetEvent<ClearSearchBoxEvent>().Subscribe(ClearSearchBox);
        }

        public ICommand LoadProductSubCategoryCommand
        {
            get
            {
                return _loadProductSubCategoryCommand;
            }
        }

        public ObservableCollection<SubCategoryDetails> SubCategories
        {
            get
            {
                return _SubCategories;
            }
            set
            {
                SetProperty(ref _SubCategories, value);
            }
        }

        private void ClearSearchBox()
        {
            _SearchKey = "";
            _CampaignId = "";
            _CampaignName = "";
        }

        private async void RefreshSubCategoryModel(SidePanelItem obj)
        {
            CurrentMainCategory = obj;

            var SubCategory = await _productService.GetSubCategoriesProducts(obj);
            if (SubCategory != null && SubCategory.Count > 0)
            {
                foreach (var Category in SubCategory)
                {
                    var CategoryDownload = await ImageUriDownloadHelper.DownloadImageAndSaveAsync(_CategoryImageURL + Category.ImageFileName, Category.ImageFileName, SubCategoryImageDirectory);
                }

                SubCategories = new ObservableCollection<SubCategoryDetails>
                (
                    SubCategory.Select(x => new SubCategoryDetails()
                    {
                        ParentCategoryId = obj.CategoryId,
                        ParentCategoryName = obj.NavigationName,
                        CategoryId = x.Id,
                        CategoryImageFileUrl = _CategoryImageURL + x.ImageFileName,
                        CategoryImageFileAddress = Path.Combine(SubCategoryImageDirectory,x.ImageFileName),
                        CategoryImageFileName = x.ImageFileName,
                        CategoryName = x.Name
                    }).ToList()
                );
              
                

                Guid LastRequestGuid = Guid.NewGuid();
                SubCategorySearchModel rm = new SubCategorySearchModel()
                {
                    LastRequestId = LastRequestGuid,
                    SearchKey = "",
                    SubCategoryDetails = SubCategories.First()
                };

                _eventAggregator.GetEvent<LastListingRequestEvent>().Publish(LastRequestGuid);
                _moduleManager.LoadModule(ModuleNames.ProductListingModule);
                var parameters = new NavigationParameters();
                parameters.Add("Filter", rm);

                if (rm != null)
                    _regionManager.RequestNavigate(RegionNames.TransactionPrimaryRegion, "ProductListingView", parameters);
            }
            //_eventAggregator.GetEvent<SubCategorySelectionEvent>().Publish(rm);
        }

        private async void RefreshSubCategoryModelBySearch(string SearchKey)
        {
            _SearchKey = SearchKey;
            var SearchCategory = await _productService.GetSearchCategories(SearchKey);
            SubCategorySearchModel rm = new SubCategorySearchModel();
            if (SearchCategory.Count > 0)
            {
                //var CategoryImageURL = "https://crm.pm.panasonic.com.my/OMSContent/Images/Categories/Actual/";
                foreach (var Category in SearchCategory)
                {
                    var CategoryDownload = await ImageUriDownloadHelper.DownloadImageAndSaveAsync(_CategoryImageURL + Category.ImageFileName, Category.ImageFileName, SubCategoryImageDirectory);
                }
                SubCategories = new ObservableCollection<SubCategoryDetails>
                (
                    SearchCategory.Select(x => new SubCategoryDetails()
                    {
                        ParentCategoryId = x.ParentCategoryId,
                        ParentCategoryName = x.ParentCategoryName,
                        CategoryId = x.Id,
                        CategoryImageFileUrl = _CategoryImageURL + x.ImageFileName,
                        CategoryImageFileAddress = Path.Combine(SubCategoryImageDirectory, x.ImageFileName),
                        CategoryImageFileName = x.ImageFileName,
                        CategoryName = x.Name
                    }).ToList()
                );
                rm.SubCategoryDetails = SubCategories.First();
                rm.SearchKey = _SearchKey;
                rm.CampaignId = _CampaignId;
                rm.CampaignName = _CampaignName;
            }
            else
            {
                rm.SubCategoryDetails = null;
                rm.SearchKey = _SearchKey;
                rm.CampaignId = _CampaignId;
                rm.CampaignName = _CampaignName;
            }

            Guid LastRequestGuid = Guid.NewGuid();
            rm.LastRequestId = LastRequestGuid;

            _eventAggregator.GetEvent<LastListingRequestEvent>().Publish(LastRequestGuid);
            _moduleManager.LoadModule(ModuleNames.ProductListingModule);
            var parameters = new NavigationParameters();
            parameters.Add("Filter", rm);

            if (rm != null)
                _regionManager.RequestNavigate(RegionNames.TransactionPrimaryRegion, "ProductListingView", parameters);
        }

        private async void RefreshSubCategoryModelByCampaign(CampaignDetailViewModel Campaign)
        {
            _CampaignId = Campaign.CampaignId;
            _CampaignName = Campaign.CampaignName;
            var SearchCategory = await _productService.GetCampaignCategories(_CampaignId);
            SubCategorySearchModel rm = new SubCategorySearchModel();
            if (SearchCategory.Count > 0)
            {

                foreach (var Category in SearchCategory)
                {
                    var CategoryDownload = await ImageUriDownloadHelper.DownloadImageAndSaveAsync(_CategoryImageURL + Category.ImageFileName, Category.ImageFileName, SubCategoryImageDirectory);
                }
                SubCategories = new ObservableCollection<SubCategoryDetails>
                (
                    SearchCategory.Select(x => new SubCategoryDetails()
                    {
                        ParentCategoryId = x.ParentCategoryId,
                        ParentCategoryName = x.ParentCategoryName,
                        CategoryId = x.Id,
                        CategoryImageFileUrl = _CategoryImageURL + x.ImageFileName,
                        CategoryImageFileAddress = Path.Combine(SubCategoryImageDirectory, x.ImageFileName),
                        CategoryImageFileName = x.ImageFileName,
                        CategoryName = x.Name
                    }).ToList()
                );
                rm.SubCategoryDetails = SubCategories.First();
                rm.CampaignId = _CampaignId;
                rm.SearchKey = _SearchKey;
                rm.CampaignName = _CampaignName;
            }
            else
            {
                //rm.SubCategoryDetails = null;
                //rm.SearchKey = _SearchKey;
                //rm.CampaignId = _CampaignId;
                //rm.CampaignName = _CampaignName;
                _CampaignId = "";
                return;
            }

            Guid LastRequestGuid = Guid.NewGuid();
            rm.LastRequestId = LastRequestGuid;

            _eventAggregator.GetEvent<LastListingRequestEvent>().Publish(LastRequestGuid);
            _moduleManager.LoadModule(ModuleNames.ProductListingModule);
            var parameters = new NavigationParameters();
            parameters.Add("Filter", rm);

            if (rm != null)
                _regionManager.RequestNavigate(RegionNames.TransactionPrimaryRegion, "ProductListingView", parameters);
        }

        private bool canLoadProductSubCategory(SubCategoryDetails SubCategory)
        {
            return true;
        }

        private void loadProductSubCategory(SubCategoryDetails SubCategory)
        {
            Guid LastRequestGuid = Guid.NewGuid();
            SubCategorySearchModel rm = new SubCategorySearchModel()
            {
                LastRequestId = LastRequestGuid,
                SearchKey = _SearchKey,
                CampaignId = _CampaignId,
                CampaignName = _CampaignName,
                SubCategoryDetails = SubCategory
            };

            ToggleViewModel ps = new ToggleViewModel()
            {
                IsShow = true,
                Header = "Loading",
                Description = SubCategory.CategoryName,
            };
            _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(ps);
            _eventAggregator.GetEvent<LastListingRequestEvent>().Publish(LastRequestGuid);
            _moduleManager.LoadModule(ModuleNames.ProductListingModule);
            var parameters = new NavigationParameters();
            parameters.Add("Filter", rm);

            if (rm != null)
                _regionManager.RequestNavigate(RegionNames.TransactionPrimaryRegion, "ProductListingView", parameters);
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            throw new NotImplementedException();
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            throw new NotImplementedException();
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            throw new NotImplementedException();
        }
    }
}