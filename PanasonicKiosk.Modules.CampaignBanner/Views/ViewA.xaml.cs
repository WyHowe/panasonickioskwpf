﻿using System.Windows.Controls;

namespace PanasonicKiosk.Modules.CampaignBanner.Views
{
    /// <summary>
    /// Interaction logic for ViewA.xaml
    /// </summary>
    public partial class ViewA : UserControl
    {
        public ViewA()
        {
            InitializeComponent();
        }
    }
}