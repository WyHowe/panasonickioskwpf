﻿using PanasonicKiosk.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PanasonicKiosk.Core.Service
{
    public interface IKioskConnectorService
    {
        KioskDetailReponseModel GetKioskDetail(string KioskMachineId);
        Task<KioskDetailReponseModel> GetKioskDetailAsync(string KioskMachineId);
        List<KioskStateResponseModel> GetStates();
    }
}