﻿using PanasonicKiosk.Modules.Footer.ViewModels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace PanasonicKiosk.Modules.Footer.Views
{
    /// <summary>
    /// Interaction logic for ViewA.xaml
    /// </summary>
    public partial class FooterView : UserControl
    {
        public FooterView()
        {
            InitializeComponent();
            //DataContext = vm;
        }

        private void SearchBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {

                var viewModel = (FooterViewModel)DataContext;

                if (viewModel.SearchItemCommand.CanExecute(null))
                    viewModel.SearchItemCommand.Execute(null);
                //TextBox tBox = (TextBox)sender;
                //DependencyProperty prop = TextBox.TextProperty;

                //BindingExpression binding = BindingOperations.GetBindingExpression(tBox, prop);
                //if (binding != null) { binding.UpdateSource(); }
            }
        }
    }
}