﻿namespace PanasonicKiosk.Core
{
    public static class RegionNames
    {
        public static string ContentRegion = "ContentRegion";
        public static string SplashRegion = "SplashRegion";
        public static string ConfirmationRegion = "ConfirmationRegion";
        public static string ClearConfirmationRegion = "ClearConfirmationRegion";
        public static string BannerRegion = "BannerRegion";
        public static string ProductRegion = "ProductRegion";
        public static string SidePanelRegion = "SidePanelRegion";
        public static string ProductListingRegion = "ProductListingRegion";
        public static string FooterRegion = "FooterRegion";
        public static string TransactionPrimaryRegion = "TransactionPrimaryRegion";
        public static string SearchBarRegion = "SearchBarRegion";
        public static string SubCategoryRegion = "SubCategoryRegion";
        public static string DetailSearchBarRegion = "DetailSearchBarRegion";
        public static string SuccessOrderRegion = "SuccessOrderRegion";
    }

    public static class ModuleNames
    {
        public static string SidePanelModule = "SidePanelModule";
        public static string IdleConfirmationModule = "IdleConfirmationModule";
        public static string ClearConfirmationModule = "ClearConfirmationModule";
        public static string OrderSuccessModule = "OrderSuccessModule";
        public static string CheckoutModule = "CheckoutModule";
        public static string ProductListingModule = "ProductListingModule";
        public static string BannerModule = "BannerModule";
        public static string FooterModule = "FooterModule";
        public static string ShoppingCartModule = "ShoppingCartModule";
        public static string ProductDetailsModule = "ProductDetailsModule";
    }

    public static class ViewName
    {
    }
}