﻿using PanasonicKiosk.Core;
using PanasonicKiosk.Modules.ClearConfirmation.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace PanasonicKiosk.Modules.ClearConfirmation
{
    public class ClearConfirmationModule : IModule
    {
        private readonly IRegionManager _regionManager;

        public ClearConfirmationModule(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }

        public void OnInitialized(IContainerProvider containerProvider)
        {
            _regionManager.RegisterViewWithRegion(RegionNames.ClearConfirmationRegion, typeof(ClearConfirmationView));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
        }
    }
}