﻿using PanasonicKiosk.Core.Models.SignalRModels;
using PanasonicKiosk.Dispatchers;
using PanasonicKiosk.ViewModels;

namespace PanasonicKiosk.Clients
{
    public class SignalRContext : IContext
    {
        public void SendConnectionEvent(MainWindowViewModel mainViewModel, bool connected)
        {
            Execute.OnUIThread(() => SetVMConnectionModel(mainViewModel, connected));
        }

        public void ReceiveMessageEvent(MainWindowViewModel mainViewModel, MyMessage myMessage)
        {
            Execute.OnUIThread(() => SetVMMessageModel(mainViewModel, myMessage));
        }

        public void ReceivePingEvent(MainWindowViewModel mainViewModel, string myMessage)
        {
            Execute.OnUIThread(() => SetVMPingModel(mainViewModel, myMessage));
        }

        private void SetVMMessageModel(MainWindowViewModel mainViewModel, MyMessage myMessage)
        {
            mainViewModel.alertsomething();
            //mainViewModel.MyMessages.Add(myMessage);
        }

        private void SetVMPingModel(MainWindowViewModel mainViewModel, string myMessage)
        {
            mainViewModel.alertsomething();
            //mainViewModel.MyMessages.Add(myMessage);
        }

        private void SetVMConnectionModel(MainWindowViewModel mainViewModel, bool connected)
        {
            if (mainViewModel.ConnectionActive != connected)
            {
                mainViewModel.ConnectionActive = connected;
            }
        }
    }
}