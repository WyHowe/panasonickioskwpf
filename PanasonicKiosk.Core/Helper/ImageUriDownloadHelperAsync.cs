﻿using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace PanasonicKiosk.Core.Helper
{
    public static class ImageUriDownloadHelperAsync
    {
        public static string DefaultProductImageLocation = "ProductsImage";
        public static string ApplicatonDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
        public static string ProductImageDirectory = Path.Combine(ApplicatonDirectory, DefaultProductImageLocation);

        public static async Task<Uri> GetImageUri(string ImageUrl)
        {
            var NoAvailableImagePath = "pack://application:,,,/Assets/no_image_available.png";
            var NoImageUri = new Uri(NoAvailableImagePath, UriKind.RelativeOrAbsolute);
            var NormalizePath = ImageUrl.Replace('/', '\\');
            var PathArray = NormalizePath.Split('\\');
            var FileName = PathArray[PathArray.Length - 1];
            var FilePath = Path.Combine(ProductImageDirectory, FileName);

            if (FileName != "no_image_available.png")
            {
                if (FileExist(FileName))
                {
                    //Compare Md5 Hash
                    //var UrlMd5 = GetHashFromUrlAsync(ImageUrl);
                    //var LocalMd5 = GetHashFromLocalPathAsync(FilePath);
                    //if(UrlMd5 != LocalMd5)
                    //{
                    //    if (DownloadImageAndSave(ImageUrl, FilePath))
                    //    {
                    //        return new Uri(ImageUrl, UriKind.Absolute);
                    //    }
                    //    else
                    //    {
                    //        return NoImageUri;
                    //    }
                    //}
                    return new Uri(FilePath, UriKind.Absolute);
                }
                else
                {
                    if (await DownloadImageAndSave(ImageUrl, FilePath))
                    {
                        return new Uri(ImageUrl, UriKind.Absolute);
                    }
                    else
                    {
                        return NoImageUri;
                    }
                }
            }
            else
            {
                return NoImageUri;
            }
            //return BitmapUri;
        }

        public static bool FileExist(string FileName)
        {
            //string ApplicatonDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

            if (!Directory.Exists(ProductImageDirectory))
            {
                Directory.CreateDirectory(ProductImageDirectory);
            }

            if (!File.Exists(Path.Combine(ProductImageDirectory, FileName)))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static string GetHashFromUrlAsync(string url)
        {
            using (var wc = new WebClient())
            {
                byte[] bytes = wc.DownloadData(url);

                using (var md5 = new MD5CryptoServiceProvider())
                {
                    using (var ms = new MemoryStream(bytes))
                    using (var bs = new BufferedStream(ms, 100_000))
                        return BitConverter.ToString(md5.ComputeHash(bs)).Replace("-", string.Empty);
                }
            }
        }

        public static string GetHashFromLocalPathAsync(string FilePath)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            {
                using (var stream = File.OpenRead(FilePath))
                {
                    var hash = md5.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                }
            }
        }

        public static async Task<bool> DownloadImageAndSave(string ImageUrl, string FilePath)
        {
            using (WebClient webClient = new WebClient())
            {
                try
                {
                    await webClient.DownloadFileTaskAsync(ImageUrl, FilePath);
                    return true;
                    //byte[] data = webClient.DownloadDataAsync(new Uri(ImageUrl));
                    //using (MemoryStream mem = new MemoryStream(data))
                    //{
                    //    using (var ProductImage = Image.FromStream(mem))
                    //    {
                    //        var FileExtension = Path.GetExtension(FilePath);

                    //        //private static string[] ValidImageExtensions = new[] { ".png", ".jpg", ".jpeg", ".bmp", ".gif" };
                    //        switch (FileExtension)
                    //        {
                    //            case ".png":
                    //                ProductImage.Save(FilePath, ImageFormat.Png);
                    //                break;
                    //            case ".jpg":
                    //            case ".jpeg":
                    //                ProductImage.Save(FilePath, ImageFormat.Png);
                    //                break;
                    //            case ".bmp":
                    //                ProductImage.Save(FilePath, ImageFormat.Bmp);
                    //                break;
                    //            case ".gif":
                    //                ProductImage.Save(FilePath, ImageFormat.Gif);
                    //                break;

                    //        }
                    //        return true;
                    //    }
                    //}
                }
                catch (WebException e)
                {
                    if ((HttpWebResponse)e.Response != null)
                    {
                        var statusCode = ((HttpWebResponse)e.Response).StatusCode;

                        if (statusCode == HttpStatusCode.NotFound)
                        {
                            return false;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    return false;
                }
            }
        }
    }
}