﻿using System.Windows.Controls;
using System.Windows.Input;

namespace PanasonicKiosk.Modules.SubCategoryPanel.Views
{
    /// <summary>
    /// Interaction logic for SubCategoryView.xaml
    /// </summary>
    public partial class SubCategoryView : UserControl
    {
        public SubCategoryView()
        {
            InitializeComponent();
            //DataContext = vm;
        }

        private void animatedScrollViewer_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}