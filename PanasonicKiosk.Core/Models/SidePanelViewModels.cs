﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PanasonicKiosk.Core.Models
{
    public class SidePanelItem : BindableBase
    {
        private SolidColorBrush _BackGroundColor;
        private string _GradientBackGroundStart;
        private string _GradientBackGroundEnd;
        private LinearGradientBrush _LinearBackGroundColor;
        private Brush _FontColor;
        private string _IconImage;
        private BitmapImage _IconBitmap;
        private string _NavigationName;
        private string _ToolTips;
        private string _CategoryId;

        public SolidColorBrush BackGroundColor
        {
            get
            {
                return _BackGroundColor;
            }
            set
            {
                SetProperty(ref _BackGroundColor, value);
            }
        }

        public string GradientBackGroundEnd
        {
            get
            {
                return _GradientBackGroundEnd;
            }
            set
            {
                SetProperty(ref _GradientBackGroundEnd, value);
            }
        }

        public string GradientBackGroundStart
        {
            get
            {
                return _GradientBackGroundStart;
            }
            set
            {
                SetProperty(ref _GradientBackGroundStart, value);
            }
        }

        public Brush FontColor
        {
            get
            {
                return _FontColor;
            }
            set
            {
                SetProperty(ref _FontColor, value);
            }
        }

        public string IconImage
        {
            get
            {
                return _IconImage;
            }
            set
            {
                SetProperty(ref _IconImage, value);
            }
        }

        public BitmapImage IconBitmap
        {
            get
            {
                return _IconBitmap;
            }
            set
            {
                SetProperty(ref _IconBitmap, value);
            }
        }

        public string NavigationName
        {
            get
            {
                return _NavigationName;
            }
            set
            {
                SetProperty(ref _NavigationName, value);
            }
        }

        public string ToolTips
        {
            get
            {
                return _ToolTips;
            }
            set
            {
                SetProperty(ref _ToolTips, value);
            }
        }

        public string CategoryId
        {
            get
            {
                return _CategoryId;
            }
            set
            {
                SetProperty(ref _CategoryId, value);
            }
        }

        public SidePanelItem(string Name, string ImagePath)
        {//(SolidColorBrush)(new BrushConverter().ConvertFrom("##D3D3D3	"));
            BackGroundColor = new SolidColorBrush(Colors.Transparent);
            GradientBackGroundStart = Colors.Transparent.ToString();
            GradientBackGroundEnd = Colors.Transparent.ToString();
            FontColor = Brushes.Gray;
            IconImage = ImagePath;
            NavigationName = Name;
            ToolTips = Name;

            IconBitmap = new BitmapImage();
            IconBitmap.BeginInit();
            IconBitmap.UriSource = new Uri(IconImage, UriKind.RelativeOrAbsolute);
            //IconBitmap.UriSource = IconImage;
            IconBitmap.CacheOption = BitmapCacheOption.OnLoad;
            IconBitmap.EndInit();
        }
    }

    public class SidePanels
    {
        public SidePanels()
        {
            SidePanelItems = new List<SidePanelItem>();
        }

        public List<SidePanelItem> SidePanelItems { get; private set; }

        public void Add(SidePanelItem item)
        {
            SidePanelItems.Add(item);
        }
    }
}