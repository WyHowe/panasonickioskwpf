﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PanasonicKiosk.Core.Models
{
    public class ProductDetailsModel : BindableBase
    {
        public bool Editing { get; set; }
        public string Id { get; set; }
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string ParentCategoryName { get; set; }
        public string ParentCategoryId { get; set; }
        public string Name { get; set; }
        public string DescriptionFileName { get; set; }
        public string FeaturesFileName { get; set; }
        public string SpecificationsFileName { get; set; }
        public string OverviewFileName { get; set; }
        public string SpecificationImageFileName { get; set; }
        public string ImageUrl { get; set; }

        public string MemberPrice
        {
            get
            {
                return _MemberPrice;
            }
            set
            {
                SetProperty(ref _MemberPrice, value);
            }
        }

        public string SpecialPrice
        {
            get
            {
                return _SpecialPrice;
            }
            set
            {
                SetProperty(ref _SpecialPrice, value);
            }
        }

        public string DiscountPercentage
        {
            get
            {
                return _DiscountPercentage;
            }
            set
            {
                SetProperty(ref _DiscountPercentage, value);
            }
        }

        public bool ShowDiscountBlock
        {
            get
            {
                return _ShowDiscountBlock;
            }
            set
            {
                SetProperty(ref _ShowDiscountBlock, value);
            }
        }

        public bool ShowNonDiscountBlock
        {
            get
            {
                return _ShowNonDiscountBlock;
            }
            set
            {
                SetProperty(ref _ShowNonDiscountBlock, value);
            }
        }

        private string _MemberPrice;
        private string _SpecialPrice;
        private string _DiscountPercentage;
        private bool _ShowDiscountBlock;
        private bool _ShowNonDiscountBlock;
        public string YoutubeEmbed { get; set; }

        public ProductModelViewModel DefaultItem
        {
            get
            {
                return VariatyItems != null && VariatyItems.Count > 0 ? VariatyItems[0] : null;
            }
        }

        public List<ProductModelViewModel> VariatyItems { get; set; }

        public List<string> VarietyImages
        {
            get
            {
                return _VarietyImages;
            }
            set
            {
                SetProperty(ref _VarietyImages, value);
            }
        }

        private List<string> _VarietyImages;
        public List<AttributeViewModel> Attributes { get; set; }

        public ImageSource ImageBitmapSource
        {
            get
            {
                return BitmapFrame.Create(new Uri(ImageUrl, UriKind.Absolute));
            }
        }
    }

    public class ParentsCategory
    {
        public string Id { get; set; }
        public string ParentId { get; set; }
        public DateTime CreationDate { get; set; }
        public string CreationId { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string LastModifiedId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageFileName { get; set; }
        public int Position { get; set; }
        public bool IsActive { get; set; }
        public ParentsCategory ParentCategory { get; set; }
    }

    public class Category
    {
        public string Id { get; set; }
        public string ParentId { get; set; }
        public DateTime CreationDate { get; set; }
        public string CreationId { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string LastModifiedId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageFileName { get; set; }
        public int Position { get; set; }
        public bool IsActive { get; set; }
        public ParentsCategory ParentCategory { get; set; }

        //public string FullURLImageFileName
        //{
        //    get
        //    {
        //        string PanasonicURL = "https://crm.pm.panasonic.com.my/OMSContent/Images/Categories/Original/";
        //        return PanasonicURL + ImageFileName;
        //    }
        //}
        //public BitmapImage ImageBitmap
        //{
        //    get
        //    {
        //        BitmapImage bi3 = new BitmapImage();
        //        bi3.BeginInit();
        //        string PanasonicURL = "https://crm.pm.panasonic.com.my/OMSContent/Images/Categories/Original/";
        //        bi3.UriSource = new Uri(PanasonicURL + ImageFileName, UriKind.RelativeOrAbsolute);
        //        bi3.CacheOption = BitmapCacheOption.OnLoad;
        //        bi3.EndInit();
        //        return bi3;
        //    }
        //}
    }

    public class CategoryViewModel
    {
        public string Id { get; set; }
        public object Attributes { get; set; }
        public object MarketPlaceCategories { get; set; }
        public string ParentId { get; set; }
        public string ParentCategoryId { get; set; }
        public string ParentCategoryName { get; set; }
        public DateTime CreationDate { get; set; }
        public string CreationId { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string LastModifiedId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageFileName { get; set; }
        public int Position { get; set; }
    }

    public class Inventory
    {
        public string Sku { get; set; }
        public DateTime CreationDate { get; set; }
        public string CreationId { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string LastModifiedId { get; set; }
        public int AvailableQuantity { get; set; }
        public int AllocatedQuantity { get; set; }
    }

    public class ImageViewModel
    {
        public string Id { get; set; }
        public string ProductModelId { get; set; }
        public string Sku { get; set; }
        public string ImageFileName { get; set; }
        public string ImageFileUrl { get; set; }
    }

    public class Amounts
    {
        public string Id { get; set; }
        public string ProductModelId { get; set; }
        public string Sku { get; set; }
        public string Type { get; set; }
        public double Amount { get; set; }
    }

    public class ProductModelViewModel
    {
        public string Id { get; set; }
        public string Sku { get; set; }
        public bool IsOutOfStock { get; set; }
        public string UnitOfMeasurement { get; set; }
        public int AvailableQuantity { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public decimal Amount { get; set; }
        public string SpecialType { get; set; }
        public decimal SpecialAmount { get; set; }
        public DateTime? SpecialFrom { get; set; }
        public DateTime? SpecialTo { get; set; }
        public List<string> Images { get; set; }
    }

    public class ProductViewModel
    {
        public string Id { get; set; }
        public DateTime CreationDate { get; set; }
        public string CreationId { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string LastModifiedId { get; set; }
        public string Sku { get; set; }
        public string Name { get; set; }
        public string UnitOfMeasurement { get; set; }
        public string ProductMajor { get; set; }
        public string ProductMinor { get; set; }
        public string ProductType { get; set; }
        public double GrossWeight { get; set; }
        public double Length { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public double Volumetric { get; set; }
        public Inventory Inventory { get; set; }
        public List<ImageViewModel> Images { get; set; }
        public List<Amounts> Amounts { get; set; }
        public List<object> SpecialAmounts { get; set; }
        public object FlashSales { get; set; }
    }

    public class ItemViewModel
    {
        public string Id { get; set; }
        public string ProductId { get; set; }
        public string ProductModelId { get; set; }
        public string Sku { get; set; }
        public string Name { get; set; }
        public ProductModelViewModel ProductModel { get; set; }
    }

    public class AttributeViewModel
    {
        public string Id { get; set; }
        public string ProductId { get; set; }
        public string CategoryAttributeId { get; set; }
        public string AttributeId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class SubCategoryDetails
    {
        public string ParentCategoryId { get; set; }
        public string ParentCategoryName { get; set; }
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string CategoryImageFileUrl { get; set; }
        public string CategoryImageFileName { get; set; }
        public string CategoryImageFileAddress { get; set; }
    }

    public class ItemDetails
    {
        public bool OnSale { get; set; }
        public bool Editing { get; set; }
        public string Id { get; set; }
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string ParentCategoryId { get; set; }
        public string ParentCategoryName { get; set; }
        public string Name { get; set; }
        public string DescriptionFileName { get; set; }
        public string FeaturesFileName { get; set; }
        public string SpecificationsFileName { get; set; }
        public string OverviewFileName { get; set; }
        public string SpecificationImageFileName { get; set; }
        public bool IsActive { get; set; }
        public List<ProductModelViewModel> Items { get; set; }
        public List<AttributeViewModel> Attributes { get; set; }

        public ImageSource ImageBitmapSource
        {
            get
            {
                return BitmapFrame.Create(new Uri(ImageUrl, UriKind.Absolute));
            }
        }

        public List<ProductModelViewModel> InStockItem
        {
            get
            {
                return Items.Where(x => x.IsOutOfStock == false).ToList();
            }
        }

        public bool IsAllOutOfStock { get; set; }
        public bool ShowIsSoldOut { get; set; }
        public bool ShowAddToCart { get; set; }
        public string ImageUrl { get; set; }
        public string MemberPrice { get; set; }
        public string SpecialPrice { get; set; }
        public string DiscountPercentage { get; set; }
        public bool ShowDiscountBlock { get; set; }
        public bool ShowNonDiscountBlock { get; set; }
        public string YoutubeVideoEmbeds { get; set; }
        public List<string> YoutubeVideoEmbedsList { get; set; }

        public ItemDetails()
        {
            Editing = true;
            OnSale = true;
        }
    }
}