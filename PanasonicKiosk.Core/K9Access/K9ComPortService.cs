﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.IO.Ports;
//using System.Threading;
//using PanasonicKiosk.Core.Utility;

//namespace PanasonicKiosk.Core.K9Access
//{
//    public class K9ComPortService
//    {
//        //
//        // Error Code
//        //
//        public const int ERR_OK = (0);              //OK
//        public const int ERR_CON_ERROR = (-1);      //Connnection Error
//        public const int ERR_TIME_OUT = (-2);       //Time Out Error

//        public const int ERR_FRAME_ERROR1 = (-3);    //Framing Error - Frame Request Not Supported
//        public const int ERR_FRAME_ERROR2 = (-4);    //Framing Error - Incoming Frame Not Valid
//        public const int ERR_WRITE_ERROR = (-5);    //Write to comport error

//        private SerialPort port;
//        public int guardtimems = 30;//guard time in ms
//        public int flushtimeout = 5;//flush before read, time out > 0 enable
//        private string LastExcpetion = "";

//        public event ReceiveEventHandler Receive;

//        public delegate void ReceiveEventHandler(string msg);

//        public K9ComPortService()
//        {
//            port = new SerialPort();
//        }

//        public string Get_last_exception()
//        {
//            return this.LastExcpetion;
//        }

//        public void Connect(int portnumber, int baudrate)
//        {
//            if (port.IsOpen)
//            {
//                System.Diagnostics.Debug.WriteLine("Port is open. Closing now...");
//                port.Close();
//                System.Diagnostics.Debug.WriteLine("Closed");
//            }

//            port.PortName = "COM" + portnumber.ToString();
//            port.BaudRate = baudrate;
//            port.Parity = System.IO.Ports.Parity.None;
//            port.DataBits = 8;
//            port.StopBits = System.IO.Ports.StopBits.One;

//            System.Diagnostics.Debug.WriteLine("com-connect");
//            port.Open();
//            System.Diagnostics.Debug.WriteLine("OK");
//        }
//        public SerialPort GetPort()
//        {
//            return port;
//        }
//        public void Disconnnect()
//        {
//            if (!port.IsOpen)
//            {
//                System.Diagnostics.Debug.WriteLine("Port is NOT open");
//                return;
//            }

//            System.Diagnostics.Debug.WriteLine("Closing port...");

//            try
//            {
//                port.Close();
//            }
//            catch
//            {
//                System.Diagnostics.Debug.WriteLine("Closing port Exception Error");
//            }

//            //close port in new thread to avoid hang
//            //Thread ClosePort = new Thread(new ThreadStart(CloseSerialOnExit));
//            //ClosePort.Start();
//            //close port in new thread to avoid hang

//            //port.Dispose();
//            System.Diagnostics.Debug.WriteLine("OK");
//        }

//        private void CloseSerialOnExit()
//        {
//            try
//            {
//                port.Close();
//            }
//            catch
//            {
//            }
//        }

//        public void Writebytes(byte[] data, int dataoffset, int datalen)
//        {
//            //
//            // Flush FISH buffer before new command
//            //
//            if (!port.IsOpen)
//            {
//                throw new Exception("Unable to write data to comport. Port not opened");
//            }

//            if (flushtimeout > 0)
//            {
//                int iTotalFlush = 0;

//                try
//                {
//                    port.BaseStream.ReadTimeout = flushtimeout;

//                    while (port.BaseStream.ReadByte() >= 0)
//                    {
//                        iTotalFlush++;
//                    }
//                }
//                catch
//                {
//                    //do nothing
//                }

//                if (iTotalFlush > 0)
//                {
//                    System.Diagnostics.Debug.WriteLine("com-flusing " + iTotalFlush.ToString());
//                    //printlog("warning: com-flusing " + iTotalFlush.ToString());
//                }
//            }

//            //
//            //
//            //
//            System.Diagnostics.Debug.WriteLine("com>> " + K9Utility.ByteArrayToHexString(data, dataoffset, datalen, " "));
//            //printlog("com>> " + util.ByteArrayToHexString(data, dataoffset, datalen, " "));

//            port.BaseStream.Write(data, dataoffset, datalen);

//        }

//        public int SetReadTimeOut(int timeoutms)
//        {
//            if (!port.IsOpen)
//            {
//                throw new Exception("Unable to read data from comport. Port not opened");
//            }

//            port.BaseStream.ReadTimeout = timeoutms;

//            return ERR_OK;
//        }
//        private void serialPort_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
//        {
//            try
//            {
//                var IncomingData = "";
//                int Count = port.BytesToRead;
//                var Buffer = new byte[Count];
//                port.Read(Buffer, 0, Count);
//                for (int i = 0, loopTo = Buffer.GetUpperBound(0); i <= loopTo; i++)
//                    IncomingData += Buffer[i].ToString("X2");
//                Receive?.Invoke(IncomingData);
//            }
//            catch (TimeoutException t)
//            {
//                Receive?.Invoke(t.Message);
//            }
//            catch (Exception ex)
//            {
//                Receive?.Invoke(ex.Message);
//            }
//        }
//        public int Readbytes(byte[] dataout, int dataoutoffset, int timeoutms)
//        {
//            int iRespLen;
//            int iRespByte;
//            int reloadms = 50;

//            if (!port.IsOpen)
//            {
//                throw new Exception("Unable to read data from comport. Port not opened");
//            }

//            //port.BaseStream.ReadTimeout = timeoutms;
//            port.BaseStream.ReadTimeout = reloadms;

//            //System.Diagnostics.Debug.WriteLine("com-Reading...");
//            iRespLen = 0;
//            iRespByte = 0;
//            do
//            {
//                try
//                {
//                    iRespByte = port.BaseStream.ReadByte();

//                    if (iRespByte >= 0)
//                    {
//                        if (iRespLen == 0)//change timeout to shorter(guard time) after first byte recieved
//                            port.BaseStream.ReadTimeout = this.guardtimems;

//                        dataout[dataoutoffset + iRespLen++] = (byte)iRespByte;
//                    }

//                    //if (interchange.isStopping)
//                    //{
//                    //port.BaseStream.WriteByte(0x00);//send a null byte to stop
//                    //interchange.isStopping = false;
//                    //break;
//                    //}
//                    //throw new Exception ("Stop EMV Polling");

//                }
//                catch (Exception ex)
//                {
//                    if (iRespLen == 0)
//                    {
//                        timeoutms -= reloadms;
//                        if (timeoutms <= 0)
//                        {
//                            this.LastExcpetion = ex.Message;
//                            break;
//                        }
//                        //else continue
//                    }
//                    else
//                    {
//                        this.LastExcpetion = ex.Message;
//                        break;
//                    }

//                }

//            } while (iRespByte >= 0);

//            if (iRespLen == 0)
//            {
//                if (this.LastExcpetion.Trim() == "The operation has timed out.")
//                    throw new Exception("Device not responding in a specific time out. Please ensure the device is connected to this PC");
//                else
//                    throw new Exception(this.LastExcpetion);
//                //return ERR_TIME_OUT;
//            }

//            System.Diagnostics.Debug.WriteLine("com<< " + K9Utility.ByteArrayToHexString(dataout, dataoutoffset, iRespLen, " "));
//            //printlog("com<< " + K9Utility.ByteArrayToHexString(dataout, dataoutoffset, iRespLen, " "));

//            return (iRespLen + dataoutoffset);
//        }

//        public bool IsOpen()
//        {
//            return port.IsOpen;
//        }

//        public int Transmitbytes(byte[] data, int dataoffset, int datalen, byte[] resp, int respoffset, int timeoutms)
//        {
//            int iResp = 0;

//            Writebytes(data, dataoffset, datalen);

//            if (timeoutms < 0)
//            {
//                return 0;//timeout < 0 indicate write only; return read byte = 0
//            }

//            iResp = Readbytes(resp, respoffset, timeoutms);

//            return iResp;
//        }

//        //public void printlog(string message)
//        //{
//        //    System.Diagnostics.Debug.WriteLine("com:: " + message);
//        //    if (inter.interchange.logtextbox != null)
//        //        inter.interchange.logtextbox.AppendText("\r\n" + message);
//        //    //or forward this message to else where
//        //}

//    }
//}