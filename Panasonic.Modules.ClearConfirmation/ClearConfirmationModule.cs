﻿using Panasonic.Modules.ClearConfirmation.Views;
using PanasonicKiosk.Core;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace Panasonic.Modules.ClearConfirmation
{
    public class ClearConfirmationModule : IModule
    {
        private readonly IRegionManager _regionManager;
        public ClearConfirmationModule(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }
        public void OnInitialized(IContainerProvider containerProvider)
        {
            _regionManager.RegisterViewWithRegion(RegionNames.ConfirmationRegion, typeof(ClearConfirmationView));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<ClearConfirmationView>();
        }
    }
}