﻿using Microsoft.AspNet.SignalR.Client;
using PanasonicKiosk.Core.HubClients;
using PanasonicKiosk.Core.Models.SignalRModels;
using System;

namespace PanasonicKiosk.Clients
{
    namespace SignalRDataProvider.DataProvider
    {
        public class SignalRHubSync : ISignalRHubSync
        {
            private readonly PanasonicKioskHubClient _panasonicKioskHubClient;

            //public event Action<bool> ConnectionEvent;

            public SignalRHubSync()
            {
                _panasonicKioskHubClient = new PanasonicKioskHubClient();
                _panasonicKioskHubClient.ConnectionEvent += _myHubClient_ConnectionEvent;
                _panasonicKioskHubClient.ReceivedMessageEvent += _myHubClient_ReceivedMessageEvent;
                _panasonicKioskHubClient.ReceivedPingEvent += _myHubClient_ReceivedPingEvent;
            }

            private void _myHubClient_ReceivedMessageEvent(MyMessage obj)
            {
                if (ReceiveMessageEvent != null) ReceiveMessageEvent.Invoke(obj);
            }

            private void _myHubClient_ReceivedPingEvent(string obj)
            {
                if (ReceivePingEvent != null) ReceivePingEvent.Invoke(obj);
            }

            private void _myHubClient_ConnectionEvent(bool obj)
            {
                if (ConnectionEvent != null) ConnectionEvent.Invoke(obj);
            }

            public event Action<bool> ConnectionEvent;

            public event Action<MyMessage> ReceiveMessageEvent;

            public event Action<string> ReceivePingEvent;

            public void Connect()
            {
                _panasonicKioskHubClient.StartHub();

                //HubClientEvents.Log.Informational("Started the Hub");
            }

            public void Disconnect()
            {
                _panasonicKioskHubClient.CloseHub();
                //HubClientEvents.Log.Informational("Closed Hub");
            }

            //public void AddMessage(MyMessage message)
            //{
            //    if (_panasonicKioskHubClient.State == ConnectionState.Connected)
            //    {
            //        _panasonicKioskHubClient.AddMessage(message.Name, message.Message);
            //    }
            //    else
            //    {
            //        //HubClientEvents.Log.Warning("Can't send message, connectionState= " + _myHubClient.State);
            //    }
            //}
            public void Heartbeat()
            {
                if (_panasonicKioskHubClient.State == ConnectionState.Connected)
                {
                    _panasonicKioskHubClient.Heartbeat();
                }
                else
                {
                    //HubClientEvents.Log.Warning("Can't send message, connectionState= " + _myHubClient.State);
                }
            }

            public void PaymentTerminalHeartBeat(TerminalStatusModel TerminalStatus)
            {
                _panasonicKioskHubClient.PaymentTerminalHeartBeat(TerminalStatus);
            }

            public void SendHelloObject(MyMessage message)
            {
                if (_panasonicKioskHubClient.State == ConnectionState.Connected)
                {
                    _panasonicKioskHubClient.SendHelloObject(new HelloModel { Age = int.Parse(message.Name), Molly = message.Message });
                }
                else
                {
                    //HubClientEvents.Log.Warning("Can't send message, connectionState= " + _myHubClient.State);
                }
            }
        }
    }
}