﻿using PanasonicKiosk.Core;
using PanasonicKiosk.Modules.ProductListing.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace PanasonicKiosk.Modules.ProductListing
{
    public class ProductListingModule : IModule
    {
        private readonly IRegionManager _regionManager;

        public ProductListingModule(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }

        public void OnInitialized(IContainerProvider containerProvider)
        {
            _regionManager.RegisterViewWithRegion(RegionNames.ProductListingRegion, typeof(ProductListingView));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<ProductListingView>();
        }
    }
}