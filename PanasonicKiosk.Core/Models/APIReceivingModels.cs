﻿using System.Collections.Generic;

namespace PanasonicKiosk.Core.Models
{
    public class ItemReceivingModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public List<KSK_ProductViewModel> Result { get; set; }
    }

    public class KioskDetailReceivingModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public KioskDetailReponseModel Result { get; set; }
    }
    public class KioskStatesReceivingModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public List<KioskStateResponseModel> Result { get; set; }
    }

    public class CampaignReceivingModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public List<KSK_CampaignProductViewModel> Result { get; set; }
    }

    public class CategoriesReceivingModel
    {
        public string Message { get; set; }
        public List<CategoryViewModel> Result { get; set; }
    }

    public class KioskStateResponseModel
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class KSK_Kiosk
    {
        public string Id { get; set; }
        public string KioskName { get; set; }
        public string KioskMachineId { get; set; }
        public string Descriptions { get; set; }
        public string DealerCode { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public bool IsActive { get; set; }
        public int RowNumber { get; set; }
    }

    public class DOM_SystemUserAddress
    {
        public string Id { get; set; }
        public string SystemUserId { get; set; }
        public string DeliverySequence { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public bool IsDefaultBilling { get; set; }
        public bool IsActive { get; set; }
        public bool IsDefaultShipping { get; set; }
        public string CS_Contact { get; set; }
        public string CS_Email { get; set; }
        public string SMS_ShortName { get; set; }
        public string PIC_Email { get; set; }
    }

    public class KioskDetailReponseModel
    {
        public KSK_Kiosk Kiosk { get; set; }
        public DOM_SystemUserAddress MerchantContactDetails { get; set; }
    }
}