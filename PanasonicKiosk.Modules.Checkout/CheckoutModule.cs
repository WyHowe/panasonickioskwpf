﻿using PanasonicKiosk.Core;
using PanasonicKiosk.Modules.Checkout.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace PanasonicKiosk.Modules.Checkout
{
    public class CheckoutModule : IModule
    {
        private readonly IRegionManager _regionManager;

        public CheckoutModule(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }

        public void OnInitialized(IContainerProvider containerProvider)
        {
            _regionManager.RegisterViewWithRegion(RegionNames.TransactionPrimaryRegion, typeof(CheckoutView));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<CheckoutView>();
        }
    }
}