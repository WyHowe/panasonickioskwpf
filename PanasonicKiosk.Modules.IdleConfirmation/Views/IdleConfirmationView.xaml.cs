﻿using System.Windows.Controls;

namespace PanasonicKiosk.Modules.IdleConfirmation.Views
{
    /// <summary>
    /// Interaction logic for ViewA.xaml
    /// </summary>
    public partial class IdleConfirmationView : UserControl
    {
        public IdleConfirmationView()
        {
            InitializeComponent();
        }
    }
}