﻿using PanasonicKiosk.Core.Models.SignalRModels;
using PanasonicKiosk.ViewModels;

namespace PanasonicKiosk.Clients
{
    public interface IContext
    {
        void SendConnectionEvent(MainWindowViewModel mainViewModel, bool connected);

        void ReceiveMessageEvent(MainWindowViewModel mainViewModel, MyMessage myMessage);

        void ReceivePingEvent(MainWindowViewModel mainViewModel, string myMessage);
    }
}