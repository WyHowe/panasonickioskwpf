﻿using System;

namespace PanasonicKiosk.Core.Models
{
    public class ItemRequestModel
    {
        public string SearchKey { get; set; }
        public string CategoryId { get; set; }
        public string DealerCode { get; set; }
        public string CampaignId { get; set; }
        public ItemRequestModel()
        {
            DealerCode = "5000005522";
        }
    }
    public class KioskDetailRequestModel
    {
        public string KioskMachineId { get; set; }
    }

    public class SubCategorySearchModel
    {
        public Guid LastRequestId { get; set; }
        public string SearchKey { get; set; }
        public string CampaignId { get; set; }
        public string CampaignName { get; set; }
        public SubCategoryDetails SubCategoryDetails { get; set; }
    }
    public class CategoryRequestModel
    {
        public string ParentCategoryId { get; set; }
    }
}