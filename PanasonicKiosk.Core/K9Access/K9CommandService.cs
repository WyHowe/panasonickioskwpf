﻿using Microsoft.VisualBasic;
using System;
using System.Text;

namespace PanasonicKiosk.Core.K9Access
{
    public class K9CommandService : IK9CommandService
    {
        private byte SEQNO = 0;

        public string SendCmd(string cmd, string data)
        {
            try
            {
                // STX 1 byte
                // LEN 2 (SEQ to DATA)
                // SEQ 1
                // CMD 1
                // DATA *
                // BCC 1 STX to Data
                // ETX 1
                string InitCmd1 = "";
                string InitCmd2 = "";
                string Bcc = "";
                string Lenght = "0000";
                SEQNO = (byte)(SEQNO + 1);
                if (SEQNO > 255)
                    SEQNO = 0;
                string SQ = SEQNO.ToString("X").PadLeft(2, '0');
                Lenght = (data.Length / 2d + 2d).ToString().PadLeft(4, '0');
                InitCmd1 = "02" + Lenght + SQ + cmd + data;
                //0200080821000000003500
                Bcc = Calculate_BCC(HexStringToByteArray(InitCmd1)).ToString("X").PadLeft(2, '0');
                InitCmd2 = InitCmd1 + Bcc + "03";
                //02000808210000000035001603
                return InitCmd2;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string StringToHex(string text)
        {
            string hex = "";
            for (int i = 0, loopTo = text.Length - 1; i <= loopTo; i++)
                hex += Strings.Asc(text.Substring(i, 1)).ToString("X").ToUpper();
            return hex;
        }

        public byte[] HexStringToByteArray(string s)
        {
            s = s.Replace(" ", "");
            var buffer = new byte[(s.Length / 2)];
            for (int i = 0, loopTo = s.Length - 1; i <= loopTo; i += 2)
                buffer[i / 2] = Convert.ToByte(s.Substring(i, 2), 16);
            return buffer;
        }

        public byte Calculate_BCC(byte[] data)
        {
            int iCount;
            byte BCC = 0;
            BCC = data[0];
            var loopTo = data.Length - 1;
            for (iCount = 1; iCount <= loopTo; iCount++)
                BCC = (byte)(BCC ^ data[iCount]);
            return BCC;
        }

        public string Bytes_To_String(byte[] bytes_Input)
        {
            var strTemp = new StringBuilder(bytes_Input.Length * 2);
            foreach (byte b in bytes_Input)
                strTemp.Append(Conversion.Hex(b));
            return strTemp.ToString();
        }

        public string SetCurrentDate()
        {
            string YY = DateTime.Now.ToString("yy");
            string MM = DateTime.Now.ToString("MM");
            string DD = DateTime.Now.ToString("dd");
            string HH = DateTime.Now.ToString("HH");
            string MI = DateTime.Now.ToString("mm");
            string SS = DateTime.Now.ToString("ss");
            return YY + MM + DD + HH + MI + SS;
        }

        public string GetToday()
        {
            string YY = DateTime.Now.ToString("yy");
            string MM = DateTime.Now.ToString("MM");
            string DD = DateTime.Now.ToString("dd");
            return YY + MM + DD;
        }

        public string RespGetStatus(string msg)
        {
            string result = "";
            string status = "";
            if (msg.StartsWith("02"))
            {
                status = msg.Substring(8, 2);
                result = status;
            }

            return result;
        }

        public string RespGetConnectionTest(string msg)
        {
            string result = "";
            string ServerResp = "";
            if (msg.StartsWith("02"))
            {
                ServerResp = msg.Substring(10, 2);
                result = ServerResp;
            }

            return result;
        }

        public string RespGetSEQ(string msg)
        {
            string result = "";
            string len = msg.Substring(2, 4);
            string status = msg.Substring(8, 2);
            if (msg.StartsWith("02"))
            {
                result = msg.Substring(6, 2);
            }

            return result;
        }

        public string RespGetCard(string cmd, string data, ref string SALE_TID, ref string SALE_DT, ref string MASK_PAN, ref string HASH_PAN, ref string RRN, ref string STAN, ref string APPR_CODE, ref string ADD_DATA)
        {
            data = data.Trim();
            string respcode = data.Substring(8, 2);
            SALE_TID = HexToString(data.Substring(10, 16));
            SALE_DT = data.Substring(26, 12);
            SALE_DT = "20" + SALE_DT.Substring(0, 2) + "-" + SALE_DT.Substring(2, 2) + "-" + SALE_DT.Substring(4, 2) + " " + SALE_DT.Substring(6, 2) + ":" + SALE_DT.Substring(8, 2) + ":" + SALE_DT.Substring(10, 2);
            MASK_PAN = data.Substring(38, 20 - 4); // TnG = 20 'CC/DC = 16
            HASH_PAN = HexToString(data.Substring(58, 80));
            if (double.Parse(cmd) == 22d)
            {
                RRN = HexToString(data.Substring(138, 24));
                STAN = data.Substring(162, 6);
                APPR_CODE = HexToString(data.Substring(168, 12));
                string adddata = data.Substring(180);
                ADD_DATA = adddata.Substring(0, adddata.Length - 4);
            }
            else
            {
                RRN = "";
                STAN = "";
                APPR_CODE = "";
                ADD_DATA = "";
            }

            return respcode;
        }

        public string HexToString(string hex)
        {
            var text = new StringBuilder(hex.Length / 2);
            for (int i = 0, loopTo = hex.Length - 2; i <= loopTo; i += 2)
                text.Append((char)Convert.ToByte(hex.Substring(i, 2), 16));
            return text.ToString();
        }
    }
}