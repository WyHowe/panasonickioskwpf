﻿//using PanasonicKiosk.Core.Utility;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace PanasonicKiosk.Core.K9Access
//{
//    public class K9MTGPService : IK9MTGPService
//    {
//        //
//        // Error Code
//        //
//        public const int ERR_OK = 0;//OK
//        public const int ERR_FAILED = 01;//General Error
//        public const int ERR_INVALID_INPUT = (-5);//invalid input parameters

//        private string RESPCODE = "";
//        private string SALE_TID = "";
//        private string SALE_DT = "";
//        private string MASK_PAN = "";
//        private string HASH_PAN = "";
//        private string RRN = "";
//        private string STAN = "";
//        private string APPR_CODE = "";
//        private string ADD_DATA = "";
//        private string data = "";
//        private string Output = "";
//        private string received = "";
//        private int ComNo = 0;
//        private bool KioskMode = false;
//        private string txtFile = "";
//        private string Mid = "01";

//        //
//        // Command Code
//        //
//        const byte CMD_ENABLE = 0x01;
//        const byte CMD_DISABLE = 0x02;
//        const byte CMD_TRANSACT_SALE_ENTRY = 0x03;
//        const byte CMD_TRANSACT_SALE_EXIT = 0x04;
//        //const byte CMD_FARE_CALC = 0x05;
//        const byte CMD_UPDATE = 0x06;
//        const byte CMD_SETTLEMENT = 0x08;
//        const byte CMD_TRANSACT_PROC_SALE_ENTRY = 0x13;
//        const byte CMD_TRANSACT_PROC_SALE_EXIT = 0x14;
//        const byte CMD_TRANSACT_PROC_SALE_TUNNELING = 0x15;
//        const byte CMD_INIT_SINGLE_SALE = 0x21;
//        const byte CMD_PROCEED_SINGLE_SALE = 0x22;
//        const byte CMD_INIT_ENTRY_SINGLE_SALE = 0x23;
//        const byte CMD_PROCEED_ENTRY_SINGLE_SALE = 0x24;
//        const byte CMD_TRANSACT_PROC_EXIT_SALE = 0x26;
//        const byte CMD_VIEW_BATCH = 0x31;
//        const byte CMD_CLEAR_BATCH = 0x32;
//        const byte CMD_INJECT_NMX_KEY = 0x41;
//        const byte CMD_UPDATE_CONF_MID_TID = 0x42;
//        const byte CMD_UPDATE_CONF_MISC = 0x43;
//        const byte CMD_SET_DEVICE_IP = 0x44;
//        const byte CMD_GET_INI_INFO = 0x45;
//        const byte CMD_CONN_TEST = 0x4C;
//        const byte CMD_REBOOT = 0x4F;
//        const byte CMD_RECOVERY = 0xEC;
//        const byte CMD_SEND_ACKNOWLEDGE = 0x89;

//        //
//        // TimeOut
//        //
//        const int TO_STD = 3000;//ms
//        const int TO_LONG = 5000;
//        const int TO_SALE = 10000;//ms
//        const int TO_WRITE_ONLY = (-1);

//        //
//        // Status Code
//        //

//        //
//        // comport
//        //
//        private K9ComPortService ComPort;
//        public int com_number = 1;
//        public int com_baudrate = 115200;

//        //
//        // protocol
//        //
//        private const byte STX = 0x02;
//        private const byte ETX = 0x03;
//        private byte[] cmdbuf;
//        private byte[] respbuf;
//        private byte[] databuf;
//        private byte[] statuscode;
//        private byte SEQNO = 0;

//        private byte CMD_State;
//        public K9MTGPService()
//        {
//            ComPort = new K9ComPortService();

//            this.cmdbuf = new byte[1024];
//            this.respbuf = new byte[1024];
//            this.databuf = new byte[1024];
//            this.statuscode = new byte[2];
//        }

//        public void SetComPortNo(int Port)
//        {
//            com_number = Port;
//        }
//        public void SetBaudRate (int Baudrate)
//        {
//            com_baudrate = Baudrate;
//        }
//        private int Status_to_errorcode(byte statuscode)
//        {
//            if (statuscode == 0x00)
//                return ERR_OK;
//            else
//                return (0 - (200000 + statuscode));
//        }
//        public int Connect(int comportnumber, int baudrate)
//        {
//            this.com_number = comportnumber;
//            this.com_baudrate = baudrate;

//            try
//            {
//                ComPort.Connect(com_number, com_baudrate);
//            }
//            catch (Exception ex)
//            {
//                Log("connect error: " + ex.Message);
//                return ERR_FAILED;
//            }

//            return ERR_OK;
//        }
//        public int Connect()
//        {
//            //this.com_number = comportnumber;
//            //this.com_baudrate = baudrate;

//            try
//            {
//                ComPort.Connect(com_number, com_baudrate);
//            }
//            catch (Exception ex)
//            {
//                Log("connect error: " + ex.Message);
//                return ERR_FAILED;
//            }

//            return ERR_OK;
//        }

//        private void clsSerial_Receive(string msg)
//        {
//            msg = msg.Trim();

//            if (CMD_State == CMD_INIT_ENTRY_SINGLE_SALE)
//            {
//                // If (CmdState = 21) And (command.RespGetStatus(msg.Trim) = "00") Then

//                if (K9Utility.RespGetStatus(msg.Trim()) == "00")
//                {
//                    var RESPCODE = K9Utility.RespGetCard("21", msg.Trim(), ref SALE_TID, ref SALE_DT, ref MASK_PAN, ref HASH_PAN, ref RRN, ref STAN, ref APPR_CODE, ref ADD_DATA);

//                    if (ComPort.IsOpen())
//                    {
//                        CMD_State = CMD_PROCEED_SINGLE_SALE;
//                        var IResp = ProceedSingleSale(new byte[1024],0);
//                    }
//                }
//                else
//                {
//                    RESPCODE = K9Utility.RespGetStatus(msg.Trim());
//                }
//            }
//            else if (CMD_State == CMD_PROCEED_SINGLE_SALE)
//            {
//                if (K9Utility.RespGetStatus(msg.Trim()) == "00")
//                {
//                    RESPCODE = K9Utility.RespGetCard("22", msg.Trim(), ref SALE_TID, ref SALE_DT, ref MASK_PAN, ref HASH_PAN, ref RRN, ref STAN, ref APPR_CODE, ref ADD_DATA);

//                    // Send ACK
//                    if (ComPort.IsOpen())
//                    {
//                        CmdState = 89;
//                        string Com = command.SendCmd("89", "22");
//                        SetText("com>>" + Com);
//                        clsSerial.Send(Com);
//                    }
//                }
//                else
//                {
//                    RESPCODE = command.RespGetStatus(msg.Trim());
//                    Output = "CMD=22 (ProcSale)" + Constants.vbCrLf + "RESPCODE=" + RESPCODE + Constants.vbCrLf;
//                    SetCardText(Output);
//                }

//                CmdState = 0;
//            }

//        }
//        //private void Port_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
//        //{
//        //    var iResp = ProceedSingleSale(new byte[1024], 0);
//        //}

//        //return ok, or device not responding, or comport not found
//        public int Disconnect()
//        {
//            try
//            {
//                ComPort.Disconnnect();
//            }
//            catch (Exception ex)
//            {
//                Log("disconnect error: " + ex.Message);
//                return ERR_FAILED;
//            }

//            return ERR_OK;
//        }

//        //setup reader for EMV transaction
//        public int Initialize_emv_reader()
//        {
//            return ERR_OK;
//        }

//        //send ecpi
//        public int Poll_sendecpi(string strCmd, int timeOutMs, byte[] respdata, int respdataoffset)
//        {
//            int iResp, iDataLen = 0;
//            iDataLen = K9Utility.HexStringToByteArray(strCmd, this.databuf, 0);

//            if (iDataLen < 1)
//            {
//                return (-1);
//            }

//            iResp = Send(this.databuf[0], this.databuf, 1, iDataLen - 1, this.statuscode, 0, respdata, respdataoffset, timeOutMs);
//            if (iResp != ERR_OK)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;//return length of respdata
//        }

//        //
//        // Polling
//        // respdata[out] - buffer of respdata
//        // respdataoffset[in] - offset/pointer to respdata
//        // return: length of respdata, negative indicate error
//        //
//        public int Poll_reader(byte[] respdata, int respdataoffset)
//        {
//            int iResp, iDataLen = 0;
//            int iYear = System.DateTime.Now.Year - 2000;

//            string strDate = iYear.ToString("00") +
//                System.DateTime.Now.Month.ToString("00") +
//                System.DateTime.Now.Day.ToString("00") +
//                System.DateTime.Now.Hour.ToString("00") +
//                System.DateTime.Now.Minute.ToString("00") +
//                System.DateTime.Now.Second.ToString("00");

//            iDataLen = K9Utility.HexStringToByteArray(strDate, this.databuf, 0);

//            iResp = Send(CMD_ENABLE, this.databuf, 0, iDataLen, this.statuscode, 0, respdata, respdataoffset, TO_STD);
//            if (iResp != ERR_OK)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;//return length of respdata
//        }

//        //
//        // Reset
//        // respdata[out] - buffer of respdata
//        // respdataoffset[in] - offset/pointer to respdata
//        // return: length of respdata, negative indicate error
//        //
//        public int Reset_reader(byte[] respdata, int respdataoffset)
//        {
//            int iResp;
//            iResp = Send(CMD_DISABLE, this.databuf, 0, 0, this.statuscode, 0, respdata, respdataoffset, TO_STD);
//            if (iResp != ERR_OK)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;//return length of respdata
//        }

//        //
//        // Set Date Time
//        // To set reader date time
//        // newdateimte [in] - byte array contain datetime in BCD format, should be 7bytes, YYYYMMDDHHmmSS
//        // newdatetimeoffset [in] - offset/pointer to newdatetime
//        // newdatetimele[in] - length of datetime. should be 7 bytes
//        // return 0=succesful, negative indicate error
//        //
//        /*public int set_date_time(byte[] newdatetime, int newdatetimeoffset, int newdatetimelen)
//        {
//            int iResp;
//            iResp = send(CMD_SET_DATETIME, newdatetime, newdatetimeoffset, newdatetimelen, this.statuscode, 0, this.databuf, 0, TO_STD);
//            if (iResp != ERR_OK)
//                return iResp;

//            return status_to_errorcode(this.statuscode[0]);
//        }*/

//        //
//        // Transact Sales
//        // Perform EMV Purcashse transaction
//        // sale_amount [in] - integer with 2 decimal points. e.g. $1.00 = 100
//        // respdata [out] - byte array to store transaction
//        // respdataoffset [in] - the offset/pointer on respdata
//        // return: >=0 OK successful and indicate length of respdata. Negative indicate error
//        //
//        public int Transact_sale_entry(int sale_amount, byte[] respdata, int respdataoffset)
//        {
//            int iResp;
//            int iDataLen;

//            iDataLen = 0;

//            iResp = Send(CMD_TRANSACT_SALE_ENTRY, this.databuf, 0, iDataLen, this.statuscode, 0, respdata, respdataoffset, TO_SALE);
//            if (iResp < 0)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;
//        }
//        public int Transact_proc_sale_entry(int sale_amount, byte[] respdata, int respdataoffset)
//        {
//            int iResp;
//            int iDataLen;

//            iDataLen = 0;

//            iResp = Send(CMD_TRANSACT_PROC_SALE_ENTRY, this.databuf, 0, iDataLen, this.statuscode, 0, respdata, respdataoffset, TO_SALE);
//            if (iResp < 0)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;
//        }

//        public int Transact_sale_exit(int sale_amount, byte[] respdata, int respdataoffset)
//        {
//            int iResp;
//            int iDataLen;

//            iDataLen = 0;

//            iResp = Send(CMD_TRANSACT_SALE_EXIT, this.databuf, 0, iDataLen, this.statuscode, 0, respdata, respdataoffset, TO_SALE);
//            if (iResp != ERR_OK)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;
//        }

//        public int ProceedTunneling(int validCode, int sale_amount, byte[] respdata, int respdataoffset)
//        {
//            int iResp;
//            int iDataLen;

//            string strValidCode = validCode.ToString("00");
//            string strAmount = sale_amount.ToString("000000000000");
//            if (sale_amount < 0 || strAmount.Length != 12)
//            {
//                Log("Invalid sale amount: " + sale_amount.ToString());
//                return ERR_INVALID_INPUT;
//            }

//            iDataLen = K9Utility.HexStringToByteArray(strValidCode + strAmount, this.databuf, 0);

//            iResp = Send(CMD_TRANSACT_PROC_SALE_TUNNELING, this.databuf, 0, iDataLen, this.statuscode, 0, respdata, respdataoffset, TO_SALE);
//            if (iResp != ERR_OK)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;
//        }

//        public int Display_fare(int validCode, int sale_amount, byte[] respdata, int respdataoffset)
//        {
//            int iResp;
//            int iDataLen;

//            string strValidCode = validCode.ToString("00");
//            string strAmount = sale_amount.ToString("000000000000");
//            if (sale_amount < 0 || strAmount.Length != 12)
//            {
//                Log("Invalid sale amount: " + sale_amount.ToString());
//                return ERR_INVALID_INPUT;
//            }

//            iDataLen = K9Utility.HexStringToByteArray(strValidCode + strAmount, this.databuf, 0);

//            iResp = Send(CMD_TRANSACT_PROC_SALE_EXIT, this.databuf, 0, iDataLen, this.statuscode, 0, respdata, respdataoffset, TO_SALE);
//            if (iResp != ERR_OK)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;
//        }

//        public int InitSingleSale(int sale_amount, byte[] respdata, int respdataoffset)
//        {
//            int iResp;
//            int iDataLen;

//            string strAmount = sale_amount.ToString("000000000000");
//            if (sale_amount < 0 || strAmount.Length != 12)
//            {
//                Log("Invalid sale amount: " + sale_amount.ToString());
//                return ERR_INVALID_INPUT;
//            }

//            iDataLen = K9Utility.HexStringToByteArray(strAmount, this.databuf, 0);

//            CMD_State = CMD_INIT_ENTRY_SINGLE_SALE;
//            iResp = Send(CMD_INIT_SINGLE_SALE, this.databuf, 0, iDataLen, this.statuscode, 0, respdata, respdataoffset, TO_SALE);
//            if (iResp != ERR_OK)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;
//        }

//        public int ProceedSingleSale(byte[] respdata, int respdataoffset)
//        {
//            int iResp;
//            /*
//             * string strAmount = sale_amount.ToString("000000000000");
//            if (sale_amount < 0 || strAmount.Length != 12)
//            {
//                log("Invalid sale amount: " + sale_amount.ToString());
//                return ERR_INVALID_INPUT;
//            }

//            iDataLen = util.HexStringToByteArray(strAmount, this.databuf, 0);
//            */

//            CMD_State = CMD_PROCEED_SINGLE_SALE;
//            iResp = Send(CMD_PROCEED_SINGLE_SALE, this.databuf, 0, 0, this.statuscode, 0, respdata, respdataoffset, TO_SALE);
//            if (iResp != ERR_OK)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;
//        }

//        public int ClearBatch(byte[] respdata, int respdataoffset)
//        {
//            int iResp;

//            /*
//             * string strAmount = sale_amount.ToString("000000000000");
//            if (sale_amount < 0 || strAmount.Length != 12)
//            {
//                log("Invalid sale amount: " + sale_amount.ToString());
//                return ERR_INVALID_INPUT;
//            }

//            iDataLen = util.HexStringToByteArray(strAmount, this.databuf, 0);
//            */

//            iResp = Send(CMD_CLEAR_BATCH, this.databuf, 0, 0, this.statuscode, 0, respdata, respdataoffset, TO_SALE);
//            if (iResp != ERR_OK)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;
//        }

//        public int InjectNmxKey(string nmxKeys, byte[] respdata, int respdataoffset)
//        {
//            int iResp;
//            int iDataLen;

//            /*
//             * string strAmount = sale_amount.ToString("000000000000");
//            if (sale_amount < 0 || strAmount.Length != 12)
//            {
//                log("Invalid sale amount: " + sale_amount.ToString());
//                return ERR_INVALID_INPUT;
//            }

//            */
//            //iDataLen = util.HexStringToByteArray(nmxKeys, this.databuf, 0);
//            iDataLen = K9Utility.ASCIIToByteArray(nmxKeys, this.databuf, 0);

//            iResp = Send(CMD_INJECT_NMX_KEY, this.databuf, 0, iDataLen, this.statuscode, 0, respdata, respdataoffset, TO_SALE);
//            if (iResp != ERR_OK)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;
//        }

//        public int InjectMidTid(string mid, string tid, byte[] respdata, int respdataoffset)
//        {
//            int iResp;
//            int iDataLen;

//            mid = mid + "               ";
//            mid = mid.Substring(0, 15);//must be 15chars

//            //iDataLen = util.HexStringToByteArray(nmxKeys, this.databuf, 0);
//            Array.Clear(this.databuf, 0, this.databuf.Length);
//            iDataLen = 0;
//            K9Utility.ASCIIToByteArray(mid, this.databuf, iDataLen);
//            iDataLen += mid.Length;
//            K9Utility.ASCIIToByteArray(tid, this.databuf, iDataLen);
//            iDataLen += tid.Length;

//            iResp = Send(CMD_UPDATE_CONF_MID_TID, this.databuf, 0, iDataLen, this.statuscode, 0, respdata, respdataoffset, TO_SALE);
//            if (iResp != ERR_OK)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;
//        }

//        public int InjectMisc(string preauthAmt, string autoSettleTime, string fareServerIp, string fareServerPort, string bankServerIp, string bankServerPort, byte[] respdata, int respdataoffset)
//        {
//            int iResp;
//            int iDataLen;

//            //iDataLen = util.HexStringToByteArray(nmxKeys, this.databuf, 0);
//            Array.Clear(this.databuf, 0, this.databuf.Length);
//            iDataLen = 0;
//            K9Utility.ASCIIToByteArray(preauthAmt, this.databuf, iDataLen);
//            iDataLen += 30;
//            K9Utility.ASCIIToByteArray(autoSettleTime, this.databuf, iDataLen);
//            iDataLen += 30;
//            K9Utility.ASCIIToByteArray(fareServerIp, this.databuf, iDataLen);
//            iDataLen += 30;
//            K9Utility.ASCIIToByteArray(fareServerPort, this.databuf, iDataLen);
//            iDataLen += 30;
//            K9Utility.ASCIIToByteArray(bankServerIp, this.databuf, iDataLen);
//            iDataLen += 30;
//            K9Utility.ASCIIToByteArray(bankServerPort, this.databuf, iDataLen);
//            iDataLen += 30;

//            iResp = Send(CMD_UPDATE_CONF_MISC, this.databuf, 0, iDataLen, this.statuscode, 0, respdata, respdataoffset, TO_SALE);
//            if (iResp != ERR_OK)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;
//        }

//        public int SetDeviceIp(byte dhcp, string ipaddr, string netmask, string gateway, byte[] respdata, int respdataoffset)
//        {
//            int iResp;
//            int iDataLen;

//            //iDataLen = util.HexStringToByteArray(nmxKeys, this.databuf, 0);
//            Array.Clear(this.databuf, 0, this.databuf.Length);
//            iDataLen = 0;
//            this.databuf[iDataLen] = dhcp;
//            iDataLen += 1;
//            K9Utility.ASCIIToByteArray(ipaddr, this.databuf, iDataLen);
//            iDataLen += 30;
//            K9Utility.ASCIIToByteArray(netmask, this.databuf, iDataLen);
//            iDataLen += 30;
//            K9Utility.ASCIIToByteArray(gateway, this.databuf, iDataLen);
//            iDataLen += 30;

//            iResp = Send(CMD_SET_DEVICE_IP, this.databuf, 0, iDataLen, this.statuscode, 0, respdata, respdataoffset, TO_SALE);
//            if (iResp != ERR_OK)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;
//        }

//        public int Settlement(byte[] respdata, int respdataoffset)
//        {
//            int iResp;
//            int iDataLen;

//            iDataLen = 3;

//            iResp = Send(CMD_SETTLEMENT, this.databuf, 0, iDataLen, this.statuscode, 0, respdata, respdataoffset, TO_SALE);
//            if (iResp != ERR_OK)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;
//        }

//        public int Send_entry_ack(byte[] respdata, int respdataoffset)
//        {
//            int iResp, iDataLen;

//            string strAck = "01";
//            iDataLen = K9Utility.HexStringToByteArray(strAck, this.databuf, 0);

//            iResp = Send(CMD_TRANSACT_SALE_ENTRY, this.databuf, 0, iDataLen, this.statuscode, 0, respdata, respdataoffset, 0);
//            if (iResp != ERR_OK)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;//return length of respdata
//        }

//        public int Send_recovery(int seqNo, byte[] respdata, int respdataoffset)
//        {
//            int iResp;

//            this.databuf[0] = (byte)seqNo;
//            iResp = Send(CMD_RECOVERY, this.databuf, 0, 1, this.statuscode, 0, respdata, respdataoffset, 0);

//            if (iResp != ERR_OK)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;//return length of respdata
//        }

//        public int Getinfo(byte[] respdata, int respdataoffset)
//        {
//            int iResp;
//            iResp = Send(CMD_GET_INI_INFO, this.databuf, 0, 0, this.statuscode, 0, respdata, respdataoffset, TO_LONG);
//            if (iResp != ERR_OK)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;
//        }

//        public int Reboot(byte[] respdata, int respdataoffset)
//        {
//            int iResp;
//            iResp = Send(CMD_REBOOT, this.databuf, 0, 0, this.statuscode, 0, respdata, respdataoffset, TO_SALE);
//            if (iResp != ERR_OK)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;
//        }

//        public int Connection_test(int type, byte[] respdata, int respdataoffset)
//        {
//            int iResp;

//            this.databuf[0] = (byte)type;
//            iResp = Send(CMD_CONN_TEST, this.databuf, 0, 1, this.statuscode, 0, respdata, respdataoffset, TO_SALE);

//            if (iResp != ERR_OK)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;//return length of respdata
//        }
//        //
//        // Cancel
//        // to cancel a exiting active sale
//        // return 0; - the response code will be returned in sale response
//        //
//        public int Transact_cancel()
//        {
//            int iResp;

//            //com.setReadTimeOut(0);//set 0 to comport read timeout to stop waiting in sale

//            iResp = Send(CMD_DISABLE, this.databuf, 0, 0, this.statuscode, 0, this.databuf, 0, TO_WRITE_ONLY);
//            if (iResp != ERR_OK)
//                return iResp;

//            return ERR_OK;
//        }
//        private byte Calculate_bcc(byte[] data, int datalen)
//        {
//            int iCount;
//            byte BCC = 0;

//            BCC = data[0];
//            for (iCount = 1; iCount < datalen; iCount++)
//            {
//                BCC = (byte)(BCC ^ data[iCount]);
//            }
//            return BCC;
//        }
//        private int Form_command(byte command, byte[] data, int dataoffset, int datalen, byte[] destbuf, int destbufoffset)
//        {
//            int datalenpluscmd = datalen + 2;
//            int iOriDestBufOffset = destbufoffset;

//            //02 ... 3F 45 03
//            datalen.ToString("X");

//            //STX
//            destbuf[destbufoffset++] = STX;
//            //DATALEN
//            destbuf[destbufoffset++] = (byte)(datalenpluscmd / 0x100);
//            destbuf[destbufoffset++] = (byte)(datalenpluscmd % 0x100);// Convert.ToByte(datalenpluscmd.ToString(), 16);
//            //SEQNO
//            if (command == K9MTGPService.CMD_RECOVERY)
//            {
//                datalen = 0;
//                destbuf[destbufoffset++] = data[0];
//            }
//            else
//            {
//                destbuf[destbufoffset++] = SEQNO++;
//            }
//            //COMMAND
//            destbuf[destbufoffset++] = command;
//            //DATA
//            destbufoffset = K9Utility.arraycopy(data, dataoffset, destbuf, destbufoffset, datalen);
//            //CRC
//            int iCalcLen = destbufoffset - iOriDestBufOffset;
//            destbuf[destbufoffset++] = Calculate_bcc(destbuf, iCalcLen);
//            //ETX
//            destbuf[destbufoffset++] = ETX;

//            return (destbufoffset);
//        }

//        private static int Verify_response(byte[] data, int dataoffset, int datalen, byte[] respstatus, int respstatusoffset, byte[] respdata, int respdataoffset)
//        {
//            byte bStatus;
//            ushort len;

//            //02 --- 3F 45 03

//            //Check Minimun Length
//            if (datalen < 7)
//                return ERR_FAILED;

//            //STX & ETX
//            if (data[dataoffset] != STX)
//                return ERR_FAILED;
//            if (data[dataoffset + datalen - 1] != ETX)
//                return ERR_FAILED;
//            dataoffset += 1;
//            datalen -= 2;

//            //LEN
//            len = K9Utility.getushort(data, dataoffset);
//            dataoffset += 2;
//            datalen -= 2;

//            //SeqNo
//            dataoffset += 1;
//            datalen -= 1;

//            //Command
//            //dataoffset += 1;
//            //datalen -= 1;

//            //STATUS
//            bStatus = data[dataoffset];
//            respstatus[respdataoffset] = bStatus;
//            dataoffset += 1;
//            datalen -= 1;

//            //
//            //BCC (-1)
//            //
//            datalen -= 1;

//            //invalid return length
//            if (datalen < 0)
//                return datalen;

//            //DATA
//            respdataoffset = K9Utility.arraycopy(data, dataoffset, respdata, respdataoffset, datalen);

//            return respdataoffset;
//        }

//        public int Send(byte command, byte[] data, int dataoffset, int datalen, byte[] respstatus, int respstatusoffset, byte[] respdata, int respdataoffset, int Timeoutms)
//        {
//            int iCmdLen, iResp;

//            /*if (!com.isOpen())
//            {
//                log("com send error. Port not opened");
//                return ERR_FAILED;
//            }*/

//            iCmdLen = Form_command(command, data, dataoffset, datalen, this.cmdbuf, 0);
//            if (iCmdLen < 0)
//                return iCmdLen;// throw new Exception("Invalid data. Unable to form SCPI message");

//            try
//            {
//                iResp = ComPort.Transmitbytes(this.cmdbuf, 0, iCmdLen, this.respbuf, 0, Timeoutms);
//                if (iResp < 0)
//                    return ERR_FAILED;// throw new Exception("Serial Read Error " + iResp.ToString());
//                if (Timeoutms < 0)
//                    return iResp;
//            }
//            catch (Exception ex)
//            {
//                Log("com send error. " + ex.Message);
//                return ERR_FAILED;
//            }

//            iResp = Verify_response(this.respbuf, 0, iResp, respstatus, respstatusoffset, respdata, respdataoffset);
//            if (iResp < 0)
//                return iResp;// throw new Exception("Invalid Response. Unable to parse SCPI response");

//            return iResp;
//        }

//        private void Log(string message)
//        {
//            System.Diagnostics.Debug.Print("ECPI: " + message);
//        }

//        public int InitEntrySingleSale(int sale_amount, byte[] respdata, int respdataoffset)
//        {
//            int iResp;
//            int iDataLen;

//            string strAmount = sale_amount.ToString("000000000000");
//            if (sale_amount < 0 || strAmount.Length != 12)
//            {
//                Log("Invalid sale amount: " + sale_amount.ToString());
//                return ERR_INVALID_INPUT;
//            }

//            iDataLen = K9Utility.HexStringToByteArray(strAmount, this.databuf, 0);

//            iResp = Send(CMD_INIT_ENTRY_SINGLE_SALE, this.databuf, 0, iDataLen, this.statuscode, 0, respdata, respdataoffset, TO_SALE);
//            if (iResp != ERR_OK)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;
//        }

//        public int ProceedEntrySingleSale(byte[] respdata, int respdataoffset)
//        {
//            int iResp;

//            iResp = Send(CMD_PROCEED_ENTRY_SINGLE_SALE, this.databuf, 0, 0, this.statuscode, 0, respdata, respdataoffset, TO_SALE);
//            if (iResp != ERR_OK)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;
//        }

//        public int Transact_proc_exit_sale(int sale_amount, byte[] respdata, int respdataoffset)
//        {
//            int iResp;
//            int iDataLen;

//            iDataLen = 0;

//            iResp = Send(CMD_TRANSACT_PROC_EXIT_SALE, this.databuf, 0, iDataLen, this.statuscode, 0, respdata, respdataoffset, TO_SALE);
//            if (iResp < 0)
//                return iResp;

//            if (this.statuscode[0] != ERR_OK)
//                return Status_to_errorcode(this.statuscode[0]);

//            return iResp;
//        }
//    }
//}