﻿using Newtonsoft.Json;
using PanasonicKiosk.Core.Sunmi.Interface;
using PanasonicKiosk.Core.Sunmi.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanasonicKiosk.Core.Sunmi
{
    public class SummiSocketService : ISummiSocketService
    {
        private byte[] StringToByteArray(string hex)
        {

            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public string CreateSunmiHexString(string RequestJson)
        {
            // Create two different encodings.
            Encoding ASCII = Encoding.ASCII;
            Encoding Unicode = Encoding.Unicode;

            // Convert the string into a byte array.
            byte[] UnicodeBytes = Unicode.GetBytes(RequestJson);
            // Perform the conversion from one encoding to the other.
            byte[] ASCIIBytes = Encoding.Convert(Unicode, ASCII, UnicodeBytes);

            // Convert the new byte[] into a char[] and then into a string.
            char[] ASCIIChar = new char[ASCII.GetCharCount(ASCIIBytes, 0, ASCIIBytes.Length)];
            ASCII.GetChars(ASCIIBytes, 0, ASCIIBytes.Length, ASCIIChar, 0);
            string ASCIIString = new string(ASCIIChar);

            // Display the strings created before and after the conversion.
            Console.WriteLine("Original string: {0}", RequestJson);
            Console.WriteLine("Ascii converted string: {0}", ASCIIString);

            byte[] ba = Encoding.Default.GetBytes(ASCIIString);
            var HexString = BitConverter.ToString(ba);
            HexString = HexString.Replace("-", "");
            return AssembleData(HexString);
        }

        public SunmiP1ResponseViewModel ProcessSunmiResponse(string EventData)
        {
            Console.WriteLine($"Message: {EventData}");
            var MessageData = EventData.Substring(6, EventData.Length - 10);
            var bytes = StringToByteArray(MessageData);
            var asciiStr = System.Text.Encoding.ASCII.GetString(bytes);
            Console.WriteLine(asciiStr);
            return JsonConvert.DeserializeObject<SunmiP1ResponseViewModel>(asciiStr);
        }

        private string AssembleData(string value)
        {
            int length = value.Length;
            string len = (length / 2).ToString().PadLeft(4, '0');
            string calculateLRCStr = len + value + "03";
            string lrc = LRC(calculateLRCStr);
            return "02" + calculateLRCStr + lrc;
        }

        private string calculateLRC(string str)
        {
            byte[] dataOut = Encoding.Default.GetBytes(str);
            byte calLrc = 0X00;
            foreach (byte temp in dataOut)
            {
                calLrc ^= temp;
            }
            var debug = calLrc.ToString("X2");
            return debug;
        }

        private string LRC(string Data)
        {
            int checksum = 0;
            foreach (char c in GetStringFromHex(Data))
            {
                checksum ^= Convert.ToByte(c);
            }

            string hex = checksum.ToString("X2");

            Console.WriteLine("Calculated LRC = " + hex);

            return hex;
        }

        //Supporting Function used in LRC function
        private string GetStringFromHex(string s)
        {
            string result = "";
            string s2 = s.Replace(" ", "");
            for (int i = 0; i < s2.Length; i += 2)
            {
                result += Convert.ToChar(int.Parse(s2.Substring(i, 2), System.Globalization.NumberStyles.HexNumber));
            }
            return result;
        }
    }
}
