﻿using PanasonicKiosk.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PanasonicKiosk.Core.Service
{
    public interface IAPIAccessService
    {
        //List<CategoryViewModel> GetCategoriesByRoot();
        Task<List<ItemDetails>> GetProducts(ItemRequestModel rm);

        Task<OrderSubmitionReturnModel> SubmitOrder(OrderHeaderModel rm);

        Task<List<CategoryViewModel>> GetRootCategories();

        Task<List<CategoryViewModel>> GetSubCategory(string SubCategoryId);

        Task<List<CategoryViewModel>> GetSearchCategory(string Searchkey);
        Task<List<CategoryViewModel>> GetCampaignCategory(string CampaignId);
        Task<KioskDetailReponseModel> GetKioskDetailsAsync(string KioskMachineId);
        KioskDetailReponseModel GetKioskDetails(string KioskMachineId);
        List<KioskStateResponseModel> GetStates();
    }
}