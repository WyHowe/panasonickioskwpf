﻿using PanasonicKiosk.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PanasonicKiosk.Core.Service
{
    public interface ISidePanelService
    {
        Task<List<SidePanelItem>> GetSidePanels();
    }
}