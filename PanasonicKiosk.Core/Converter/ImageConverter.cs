﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace PanasonicKiosk.Core.Converter
{
    [ValueConversion(typeof(string), typeof(BitmapImage))]
    public class ImageConverter : IValueConverter
    {
        //public static string DefaultProductImageLocation = "ProductsImage";
        //private static string[] ValidImageExtensions = new[] { ".png", ".jpg", ".jpeg", ".bmp", ".gif" };
        //public static string ApplicatonDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
        //public string ProductImageDirectory = Path.Combine(ApplicatonDirectory, DefaultProductImageLocation);

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (value != null)
                {
                    
                    int RenderHeight = 0;
                    int RenderWidth = 0;
                    if (parameter != null)
                    {
                        string parameterString = parameter as string;
                        if (parameterString.Split('|').Length == 2)
                        {
                            RenderHeight = int.Parse(parameterString.Split('|')[0]);
                            RenderWidth = int.Parse(parameterString.Split('|')[1]);
                            if (RenderHeight == 80)
                            {

                            }
                        }
                        else
                        {
                            RenderHeight = int.Parse((string)parameter);
                            RenderWidth = int.Parse((string)parameter);
                            if (RenderHeight == 80)
                            {

                            }
                        }
                    }

                    //BitmapImage source = new BitmapImage();
                    //source.BeginInit();
                    //source.UriSource = uri;
                    //source.DecodePixelHeight = 10;
                    //source.DecodePixelWidth = 10;
                    //source.EndInit();

                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    //bi.UriSource = new Uri(value.ToString(), UriKind.Absolute);
                    bi.UriSource = Helper.ImageUriDownloadHelper.GetImageUri(value.ToString());

                    bi.CacheOption = BitmapCacheOption.OnLoad;
                    if (RenderHeight != 0)
                    {
                        bi.DecodePixelHeight = RenderHeight;
                        bi.DecodePixelWidth = RenderWidth;
                    }

                    bi.EndInit();
                    return bi;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}