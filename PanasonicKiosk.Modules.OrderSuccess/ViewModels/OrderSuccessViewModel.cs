﻿using PanasonicKiosk.Core;
using PanasonicKiosk.Core.Events;
using PanasonicKiosk.Core.Models;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Windows.Input;
using System.Windows.Threading;

namespace PanasonicKiosk.Modules.OrderSuccess.ViewModels
{
    public class OrderSuccessViewModel : BindableBase, INavigationAware, IRegionMemberLifetime
    {
        private IEventAggregator _eventAggregator;

        private readonly DispatcherTimer _countDownTimer;
        private IRegionManager _regionManager;
        private DelegateCommand _CloseCommand;
        private OrderSummaryViewModel _Summary;
        private string _message;
        private string _InfoImage;
        private string _PanasonicLogo;
        private int ElapseTime = 30;

        public string InfoImage
        {
            get { return _InfoImage; }
            set { SetProperty(ref _InfoImage, value); }
        }

        public string PanasonicLogo
        {
            get { return _PanasonicLogo; }
            set { SetProperty(ref _PanasonicLogo, value); }
        }

        public OrderSummaryViewModel Summary
        {
            get { return _Summary; }
            set { SetProperty(ref _Summary, value); }
        }

        public OrderSuccessViewModel(IEventAggregator eventAggregator, IRegionManager regionManager)
        {
            this._eventAggregator = eventAggregator;
            _regionManager = regionManager;

            _countDownTimer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(1), IsEnabled = true };
            _countDownTimer.Tick += OnInactivity;
            _CloseCommand = new DelegateCommand(closeSummary);
            InfoImage = "pack://application:,,,/Assets/Success.gif";
            PanasonicLogo = "pack://application:,,,/Assets/Panasonic_logo.png";
            //Message = "View A from your Prism Module";
        }

        public string Message
        {
            get
            {
                return _message;
            }
            set
            {
                SetProperty(ref _message, value);
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                return _CloseCommand;
            }
        }

        public bool KeepAlive
        {
            get
            {
                return false;
            }
        }

        private void closeSummary()
        {
            _countDownTimer.Stop();
            //_eventAggregator.GetEvent<KeepItemFromCartEvent>().Publish(Entry);
            _eventAggregator.GetEvent<HideSuccessOrderScreenEvent>().Publish();
            _eventAggregator.GetEvent<ProductListingShowEvent>().Publish();
            _eventAggregator.GetEvent<ResetViewListingEvent>().Publish();
            _regionManager.RequestNavigate(RegionNames.TransactionPrimaryRegion, "ProductListingView");
        }

        private void OnInactivity(object sender, EventArgs e)
        {
            ElapseTime--;
            Message = ElapseTime.ToString() + " seconds";
            if (ElapseTime <= -1)
            {
                //closeSummary();
            }

            // remember mouse position
            //_inactiveMousePosition = Mouse.GetPosition(MainGrid);

            // set UI on inactivity
            // rectangle.Visibility = Visibility.Hidden;
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            ElapseTime = 30;
            Message = ElapseTime + " seconds";
            _countDownTimer.Stop();
            _countDownTimer.Start();
            var SelectedEntry = navigationContext.Parameters["Entry"] as OrderSummaryViewModel;
            if (SelectedEntry != null)
            {
                //SelectedEntry.MerchantAddress1 = "LOT 115-1, JALAN ANGGUN CITY 3,";
                //SelectedEntry.MerchantAddress2 = "PUSAT KOMERSIAL ANGGUN CITY,";
                //SelectedEntry.MerchantAddress3 = "TAMAN ANGGUN, Rawang";
                //SelectedEntry.MerchantState = "SELANGOR";
                //SelectedEntry.MerchantContact = "03 - 60911733 / 601234567893";
                //SelectedEntry.MerchantCountry = "MALAYSIA";
                //SelectedEntry.MerchantEmail = "cheryl5898@yahoo.com";
                //SelectedEntry.MerchantName = "MTT SOLUTIONS SDN BHD";
                //SelectedEntry.MerchantPostCode = "48000";
                Summary = SelectedEntry;
            }
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
            //throw new NotImplementedException();
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            //throw new NotImplementedException();
        }
    }
}