﻿using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace PanasonicKiosk.Core.Models
{
    public class CarouselImageViewModel
    {
        public List<string> ImagesAddresses { get; set; }
        public List<string> CampaignIds { get; set; }
        public List<string> CampaignNames { get; set; }
        public Image[] CarouselImageControls { get; set; }
        public List<ImageSource> ImagesSources { get; set; }
        public int CurrentSourceIndex { get; set; }
        public int CurrentCtrlIndex { get; set; }
        public DispatcherTimer timerCarouselImageChange { get; set; }

        public CarouselImageViewModel()
        {
            ImagesAddresses = new List<string>();
            CampaignIds = new List<string>();
            CampaignNames = new List<string>();
            ImagesSources = new List<ImageSource>();
        }
    }
}