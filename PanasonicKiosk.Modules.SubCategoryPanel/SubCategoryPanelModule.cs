﻿using PanasonicKiosk.Core;
using PanasonicKiosk.Modules.SubCategoryPanel.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace PanasonicKiosk.Modules.SubCategoryPanel
{
    public class SubCategoryPanelModule : IModule
    {
        private readonly IRegionManager _regionManager;

        public SubCategoryPanelModule(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }

        public void OnInitialized(IContainerProvider containerProvider)
        {
            _regionManager.RegisterViewWithRegion(RegionNames.SubCategoryRegion, typeof(SubCategoryView));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<SubCategoryView>();
        }
    }
}