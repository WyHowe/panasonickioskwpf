﻿using Prism.Ioc;
using Prism.Modularity;

namespace PanasonicKiosk.Modules.CampaignBanner
{
    public class CampaignBannerModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
        }
    }
}