﻿using Newtonsoft.Json;
using PanasonicKiosk.Core.Helper;
using PanasonicKiosk.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace PanasonicKiosk.Core.Service
{
    public class APIAccessService : IAPIAccessService
    {
        private Uri TargetUrl { get; set; }
        private Uri TargetUrlTest { get; set; }

        public APIAccessService()
        {
            TargetUrl = new Uri(Properties.Settings.Default.TargetUrl);
            //TargetUrl = new Uri(Properties.Settings.Default.TargetUrl);
            //TargetUrlTest = new Uri(Properties.Settings.Default.TestTargetUrl);
            //TargetUrlTest = new Uri("https://localhost:44370/");
            //TargetUrl = new Uri("https://localhost:44370/");
        }


        public async Task<List<ItemDetails>> GetProducts(ItemRequestModel rm)
        {
            List<ItemDetails> fm = new List<ItemDetails>();
            //rm.DealerCode = "";
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = TargetUrl;
                    //client.DefaultRequestHeaders.Add("Authorization", App.AccessTokens.TokenType + " " + App.AccessTokens.AccessToken);
                    //client.DefaultRequestHeaders.Add("SaveSettings", Value);
                    //client.DefaultRequestHeaders.Add("SaveType", Type);
                    //client.DefaultRequestHeaders.Add("Type", "UploadInterval");
                    var content = new StringContent(JsonConvert.SerializeObject(rm), Encoding.UTF8, "application/json");
                    //client.DefaultRequestHeaders.Add("SearchKey", "");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                    var address = client.BaseAddress.AbsoluteUri + "" + "api/Product/Hierarchy/Get";
                    //var response = client.PostAsync(address, content).Result;


                    CoreSettingHelper.ToggleAllowUnsafeHeaderParsing(true);
                    var response = await client.PostAsync(address, content);

                    var jsontext = response.Content.ReadAsStringAsync();

                    CoreSettingHelper.ToggleAllowUnsafeHeaderParsing(false);
                    var responseModel = JsonConvert.DeserializeObject<ItemReceivingModel>(jsontext.Result.ToString());

                    //fm = responseModel.Result;
                    if (responseModel.Result != null && responseModel.Result.Count > 0)
                    {
                        foreach (var Item in responseModel.Result)
                        {
                            ItemDetails newItemsDetails = new ItemDetails()
                            {
                                Items = new List<ProductModelViewModel>(),
                                Id = Item.Id,
                                CategoryId = Item.CategoryId,
                                Name = Item.Name,
                                SpecificationImageFileName = Item.SpecificationImageFileName,
                                CategoryName = Item.CategoryName,
                                ParentCategoryId = Item.ParentCategoryId,
                                ParentCategoryName = Item.ParentCategoryName,

                                //MemberPrice = Item.Items.First().Amount,
                                //CreationDate
                                //CreationId
                                //LastModifiedDate
                                //LastModifiedId
                                //DescriptionFileName
                                //FeaturesFileName
                                //SpecificationsFileName = Item.SpecificationImageFileName,
                                //OverviewFileName
                                //IsActive
                                //List<ItemViewModel> Items
                                //List<AttributeViewModel> Attributes
                                //public ImageSource ImageBitmapSource
                                //        {
                                //            get
                                //            {
                                //                return BitmapFrame.Create(new Uri(ImageUrl, UriKind.Absolute));
                                //            }
                                //        }
                                //        ImageUrl
                                //        MemberPrice
                                //YoutubeVideoEmbeds
                                //public List<string> YoutubeVideoEmbedsList
                            };
                            foreach (var ItemDetails in Item.Items)
                            {
                                //if (ItemDetails.Quantity > 0)
                                //{
                                ProductModelViewModel ItemViewModel = new ProductModelViewModel()
                                {
                                    Id = ItemDetails.Id,
                                    Name = ItemDetails.Name,
                                    Amount = ItemDetails.Amount,
                                    Color = ItemDetails.Color,
                                    UnitOfMeasurement = ItemDetails.UnitOfMeasurement,
                                    AvailableQuantity = ItemDetails.Quantity,
                                    SpecialAmount = ItemDetails.SpecialAmount,
                                    Images = ItemDetails.Images.Select(x => x.ImageFileName).ToList(),
                                    SpecialFrom = ItemDetails.SpecialFrom,
                                    SpecialTo = ItemDetails.SpecialTo,
                                    SpecialType = ItemDetails.SpecialType,
                                    //ProductId = ItemDetails.p
                                    Sku = ItemDetails.Sku,
                                    IsOutOfStock = ItemDetails.Quantity == 0
                                };
                                //newItemsDetails.Items.Add(ItemViewModel);
                                // }
                                //else
                                //{
                                //    ProductModelViewModel ItemViewModel = new ProductModelViewModel()
                                //    {
                                //        Id = ItemDetails.Id,
                                //        Name = ItemDetails.Name,
                                //        Amount = ItemDetails.Amount,
                                //        Color = ItemDetails.Color,
                                //        UnitOfMeasurement = ItemDetails.UnitOfMeasurement,
                                //        AvailableQuantity = ItemDetails.Quantity,
                                //        SpecialAmount = ItemDetails.SpecialAmount,
                                //        Images = ItemDetails.Images.Select(x => x.ImageFileName).ToList(),
                                //        SpecialFrom = ItemDetails.SpecialFrom,
                                //        SpecialTo = ItemDetails.SpecialTo,
                                //        SpecialType = ItemDetails.SpecialType,
                                //        //ProductId = ItemDetails.p

                                //        Sku = ItemDetails.Sku,
                                //    };
                                newItemsDetails.Items.Add(ItemViewModel);
                                // }
                            }
                            if (newItemsDetails.Items.Count > 0)
                            {
                                if (newItemsDetails.Items.Where(x => x.IsOutOfStock == false).Count() > 0)
                                {
                                    newItemsDetails.IsAllOutOfStock = false;
                                    newItemsDetails.ShowIsSoldOut = false;
                                    newItemsDetails.ShowAddToCart = true;
                                }
                                else
                                {
                                    newItemsDetails.IsAllOutOfStock = true;
                                    newItemsDetails.ShowIsSoldOut = true;
                                    newItemsDetails.ShowAddToCart = false;
                                }
                                fm.Add(newItemsDetails);
                            }
                            //else
                            //{
                            //    fm.Add(newItemsDetails);
                            //}
                        }
                    }
                    return fm;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex);
                    return fm;
                    //return new ResultAppSettingViewModel();
                }
            }
        }
        public async Task<KioskDetailReponseModel> GetKioskDetailsAsync(string KioskMachineId)
        {
            KioskDetailRequestModel rm = new KioskDetailRequestModel();
            rm.KioskMachineId = KioskMachineId;
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = TargetUrl;
                    //client.DefaultRequestHeaders.Add("Authorization", App.AccessTokens.TokenType + " " + App.AccessTokens.AccessToken);
                    //client.DefaultRequestHeaders.Add("SaveSettings", Value);
                    //client.DefaultRequestHeaders.Add("SaveType", Type);
                    //client.DefaultRequestHeaders.Add("Type", "UploadInterval");
                    var content = new StringContent(JsonConvert.SerializeObject(rm), Encoding.UTF8, "application/json");
                    //client.DefaultRequestHeaders.Add("SearchKey", "");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                    var address = client.BaseAddress.AbsoluteUri + "" + "api/Kiosk/KioskDetail/Get";
                    //var response = client.PostAsync(address, content).Result;


                    CoreSettingHelper.ToggleAllowUnsafeHeaderParsing(true);
                    var response = await client.PostAsync(address, content);

                    var jsontext = response.Content.ReadAsStringAsync();


                    CoreSettingHelper.ToggleAllowUnsafeHeaderParsing(false);
                    var responseModel = JsonConvert.DeserializeObject<KioskDetailReceivingModel>(jsontext.Result.ToString());

                    //fm = responseModel.Result;
                    return responseModel.Result;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex);
                    return null;
                    //return new ResultAppSettingViewModel();
                }
            }
        }
        public KioskDetailReponseModel GetKioskDetails(string KioskMachineId)
        {
            KioskDetailRequestModel rm = new KioskDetailRequestModel();
            rm.KioskMachineId = KioskMachineId;
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = TargetUrl;
                    //client.DefaultRequestHeaders.Add("Authorization", App.AccessTokens.TokenType + " " + App.AccessTokens.AccessToken);
                    //client.DefaultRequestHeaders.Add("SaveSettings", Value);
                    //client.DefaultRequestHeaders.Add("SaveType", Type);
                    //client.DefaultRequestHeaders.Add("Type", "UploadInterval");
                    var content = new StringContent(JsonConvert.SerializeObject(rm), Encoding.UTF8, "application/json");
                    //client.DefaultRequestHeaders.Add("SearchKey", "");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                    var address = client.BaseAddress.AbsoluteUri + "" + "api/Kiosk/KioskDetail/Get";
                    //var response = client.PostAsync(address, content).Result;

                    CoreSettingHelper.ToggleAllowUnsafeHeaderParsing(true);
                    var response = client.PostAsync(address, content).Result;

                    var jsontext = response.Content.ReadAsStringAsync();


                    CoreSettingHelper.ToggleAllowUnsafeHeaderParsing(false);
                    var responseModel = JsonConvert.DeserializeObject<KioskDetailReceivingModel>(jsontext.Result.ToString());

                    //fm = responseModel.Result;
                    return responseModel.Result;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex);
                    return null;
                    //return new ResultAppSettingViewModel();
                }
            }
        }
        public List<KioskStateResponseModel> GetStates()
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = TargetUrl;
                    //client.DefaultRequestHeaders.Add("Authorization", App.AccessTokens.TokenType + " " + App.AccessTokens.AccessToken);
                    //client.DefaultRequestHeaders.Add("SaveSettings", Value);
                    //client.DefaultRequestHeaders.Add("SaveType", Type);
                    //client.DefaultRequestHeaders.Add("Type", "UploadInterval");
                    //client.DefaultRequestHeaders.Add("SearchKey", "");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                    var address = client.BaseAddress.AbsoluteUri + "" + "api/Order/States/Get";
                    //var response = client.PostAsync(address, content).Result;

                    CoreSettingHelper.ToggleAllowUnsafeHeaderParsing(true);
                    var response = client.PostAsync(address, null).Result;

                    var jsontext = response.Content.ReadAsStringAsync();

                    CoreSettingHelper.ToggleAllowUnsafeHeaderParsing(false);
                    var responseModel = JsonConvert.DeserializeObject<KioskStatesReceivingModel>(jsontext.Result.ToString());

                    //fm = responseModel.Result;
                    return responseModel.Result;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex);
                    return null;
                    //return new ResultAppSettingViewModel();
                }
            }
        }
        public async Task<OrderSubmitionReturnModel> SubmitOrder(OrderHeaderModel orm)
        {
            List<ItemDetails> fm = new List<ItemDetails>();
            using (var client = new HttpClient())
            {
                try
                {
                    //client.BaseAddress = new Uri("https://localhost:44370/");
                    client.BaseAddress = TargetUrl;
                    //client.DefaultRequestHeaders.Add("Authorization", App.AccessTokens.TokenType + " " + App.AccessTokens.AccessToken);
                    //client.DefaultRequestHeaders.Add("SaveSettings", Value);
                    //client.DefaultRequestHeaders.Add("SaveType", Type);
                    //client.DefaultRequestHeaders.Add("Type", "UploadInterval");
                    var content = new StringContent(JsonConvert.SerializeObject(orm), Encoding.UTF8, "application/json");
                    //client.DefaultRequestHeaders.Add("SearchKey", "");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                    var address = client.BaseAddress.AbsoluteUri + "" + "api/Order/Save";
                    //var response = client.PostAsync(address, content).Result;


                    CoreSettingHelper.ToggleAllowUnsafeHeaderParsing(true);
                    var response = await client.PostAsync(address, content);

                    var jsontext = response.Content.ReadAsStringAsync();


                    CoreSettingHelper.ToggleAllowUnsafeHeaderParsing(false);
                    var responseModel = JsonConvert.DeserializeObject<OrderSubmitionReturnModel>(jsontext.Result.ToString());
                    //return responseModel.Result;
                    return responseModel;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex);
                    return new OrderSubmitionReturnModel()
                    {
                        OrderId = "",
                        Result = "Transaction Failed",
                        IsSuccess = false
                    };
                    //return new ResultAppSettingViewModel();
                }
            }
        }

        public async Task<List<CategoryViewModel>> GetRootCategories()
        {
            List<CategoryViewModel> fm = new List<CategoryViewModel>();
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = TargetUrl;
                    CategoryRequestModel rm = new CategoryRequestModel();
                    rm.ParentCategoryId = "Random";
                    var content = new StringContent(JsonConvert.SerializeObject(rm), Encoding.UTF8, "application/json");
                    //client.DefaultRequestHeaders.Add("SearchKey", "");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                    var address = client.BaseAddress.AbsoluteUri + "" + "api/Product/Category/Root/Get";


                    CoreSettingHelper.ToggleAllowUnsafeHeaderParsing(true);
                    var response = await client.PostAsync(address, content);

                    var jsontext = response.Content.ReadAsStringAsync();


                    CoreSettingHelper.ToggleAllowUnsafeHeaderParsing(false);
                    var responseModel = JsonConvert.DeserializeObject<CategoriesReceivingModel>(jsontext.Result.ToString());
                    fm = responseModel.Result;
                    return fm;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex);
                    return fm;
                    //return new ResultAppSettingViewModel();
                }
            }
        }

        public async Task<List<CategoryViewModel>> GetSubCategory(string SubCategoryId)
        {
            List<CategoryViewModel> fm = new List<CategoryViewModel>();
            using (var client = new HttpClient())
            {
                //Too slow to get all subcategory items.
                try
                {
                    client.BaseAddress = TargetUrl;
                    CategoryRequestModel rm = new CategoryRequestModel();
                    rm.ParentCategoryId = SubCategoryId;
                    var content = new StringContent(JsonConvert.SerializeObject(rm), Encoding.UTF8, "application/json");
                    //client.DefaultRequestHeaders.Add("SearchKey", "");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                    var address = client.BaseAddress.AbsoluteUri + "" + "api/Product/Category/Sub/Get";


                    CoreSettingHelper.ToggleAllowUnsafeHeaderParsing(true);
                    var response = await client.PostAsync(address, content);

                    var jsontext = response.Content.ReadAsStringAsync();


                    CoreSettingHelper.ToggleAllowUnsafeHeaderParsing(false);
                    var responseModel = JsonConvert.DeserializeObject<CategoriesReceivingModel>(jsontext.Result.ToString());
                    fm = responseModel.Result;
                    return fm;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex);
                    return fm;
                    //return new ResultAppSettingViewModel();
                }
            }
        }

        public async Task<List<CategoryViewModel>> GetSearchCategory(string SearchKey)
        {
            List<CategoryViewModel> fm = new List<CategoryViewModel>();
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = TargetUrl;
                    ItemRequestModel rm = new ItemRequestModel();
                    rm.SearchKey = SearchKey;
                    var content = new StringContent(JsonConvert.SerializeObject(rm), Encoding.UTF8, "application/json");
                    //client.DefaultRequestHeaders.Add("SearchKey", "");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                    //var address = client.BaseAddress.AbsoluteUri + "" + "api/Panasonic/GetSearchCategories";
                    var address = client.BaseAddress.AbsoluteUri + "" + "api/Product/Category/Sub/Search";


                    CoreSettingHelper.ToggleAllowUnsafeHeaderParsing(true);
                    var response = await client.PostAsync(address, content);

                    var jsontext = response.Content.ReadAsStringAsync();


                    CoreSettingHelper.ToggleAllowUnsafeHeaderParsing(false);
                    var responseModel = JsonConvert.DeserializeObject<CategoriesReceivingModel>(jsontext.Result.ToString());
                    fm = responseModel.Result;
                    return fm;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex);
                    return fm;
                    //return new ResultAppSettingViewModel();
                }
            }
        }

        public async Task<List<CategoryViewModel>> GetCampaignCategory(string CampaignId)
        {
            List<CategoryViewModel> fm = new List<CategoryViewModel>();
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = TargetUrl;
                    ItemRequestModel rm = new ItemRequestModel();
                    rm.CampaignId = CampaignId;
                    var content = new StringContent(JsonConvert.SerializeObject(rm), Encoding.UTF8, "application/json");
                    //client.DefaultRequestHeaders.Add("SearchKey", "");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                    //var address = client.BaseAddress.AbsoluteUri + "" + "api/Panasonic/GetSearchCategories";
                    var address = client.BaseAddress.AbsoluteUri + "" + "api/Product/Category/Sub/Campaign";


                    CoreSettingHelper.ToggleAllowUnsafeHeaderParsing(true);
                    var response = await client.PostAsync(address, content);

                    var jsontext = response.Content.ReadAsStringAsync();


                    CoreSettingHelper.ToggleAllowUnsafeHeaderParsing(false);
                    var responseModel = JsonConvert.DeserializeObject<CategoriesReceivingModel>(jsontext.Result.ToString());
                    fm = responseModel.Result;
                    return fm;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex);
                    return fm;
                    //return new ResultAppSettingViewModel();
                }
            }
        }
        //public List<CategoryViewModel> GetCategoriesByRoot()
        //{
        //    List<CategoryViewModel> fm = new List<CategoryViewModel>();
        //    using (var client = new HttpClient())
        //    {
        //        try
        //        {
        //            client.BaseAddress = new Uri(Properties.Settings.Default.TargetUrl);
        //            CategoryRequestModel rm = new CategoryRequestModel();
        //            rm.ParentCategoryId = "Random";
        //            var content = new StringContent(JsonConvert.SerializeObject(rm), Encoding.UTF8, "application/json");
        //            //client.DefaultRequestHeaders.Add("SearchKey", "");
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
        //            var address = client.BaseAddress.AbsoluteUri + "" + "api/Panasonic/GetRootCategories";
        //            var response = client.PostAsync(address, content).Result;

        //            var jsontext = response.Content.ReadAsStringAsync();

        //            var responseModel = JsonConvert.DeserializeObject<RootCategoriesReceivingModel>(jsontext.Result.ToString());
        //            fm = responseModel.RootCategories;
        //            return fm;
        //        }
        //        catch (Exception ex)
        //        {
        //            System.Diagnostics.Debug.WriteLine(ex);
        //            return fm;
        //            //return new ResultAppSettingViewModel();
        //        }
        //    }
        //}
    }
}