﻿using PanasonicKiosk.Core.Events;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Windows.Input;
using System.Windows.Threading;

namespace PanasonicKiosk.Modules.IdleConfirmation.ViewModels
{
    public class IdleConfirmationViewModel : BindableBase, INavigationAware, IRegionMemberLifetime
    {
        private readonly DispatcherTimer _countDownTimer;
        private IEventAggregator _eventAggregator;
        private DelegateCommand _YesCommand;
        private DelegateCommand _NoCommand;
        private string _message;
        private string _ErrorImage;
        private int ElapseTime = 30;

        public string ErrorImage
        {
            get { return _ErrorImage; }
            set { SetProperty(ref _ErrorImage, value); }
        }

        public IdleConfirmationViewModel(IEventAggregator eventAggregator)
        {
            _countDownTimer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(1), IsEnabled = true };
            _countDownTimer.Tick += OnInactivity;

            this._eventAggregator = eventAggregator;
            _YesCommand = new DelegateCommand(loadHideConfirmation, canHideConfirmation);
            _NoCommand = new DelegateCommand(clearCartLoadSplashScreen);
            ErrorImage = "pack://application:,,,/Assets/exclamation.gif";

            //Message = "View A from your Prism Module";
        }

        public string Message
        {
            get
            {
                return _message;
            }
            set
            {
                SetProperty(ref _message, value);
            }
        }

        public ICommand YesCommand
        {
            get
            {
                return _YesCommand;
            }
        }

        public ICommand NoCommand
        {
            get
            {
                return _NoCommand;
            }
        }

        public bool KeepAlive
        {
            get
            {
                return false;
            }
        }

        private void clearCartLoadSplashScreen()
        {
            _countDownTimer.Stop();
            _eventAggregator.GetEvent<ClearCartEvent>().Publish();
            _eventAggregator.GetEvent<ClearAddressEvent>().Publish();
            _eventAggregator.GetEvent<ShowSplashScreenEvent>().Publish();
        }

        private bool canHideConfirmation()
        {
            return true;
        }

        private void loadHideConfirmation()
        {
            _countDownTimer.Stop();
            _eventAggregator.GetEvent<HideConfirmationScreenEvent>().Publish();
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            ElapseTime = 30;
            Message = ElapseTime + " seconds";
            _countDownTimer.Stop();
            _countDownTimer.Start();
            //throw new NotImplementedException();
        }

        private void OnInactivity(object sender, EventArgs e)
        {
            ElapseTime--;
            Message = ElapseTime.ToString() + " seconds";
            if (ElapseTime == -1)
            {
                clearCartLoadSplashScreen();
            }

            // remember mouse position
            //_inactiveMousePosition = Mouse.GetPosition(MainGrid);

            // set UI on inactivity
            // rectangle.Visibility = Visibility.Hidden;
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
            //throw new NotImplementedException();
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            //throw new NotImplementedException();
        }
    }
}