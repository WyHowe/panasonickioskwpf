﻿using PanasonicKiosk.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PanasonicKiosk.Core.Service
{
    public interface IProductService
    {
        Task<List<ItemDetails>> GetItem(ItemRequestModel rm);

        Task<List<CategoryViewModel>> GetSubCategoriesProducts(SidePanelItem CategoryDetails);

        Task<List<CategoryViewModel>> GetSearchCategories(string SearchKey);

        Task<OrderSubmitionReturnModel> SubmitOrder(OrderHeaderModel fm);
        Task<List<CategoryViewModel>> GetCampaignCategories(string CampaignId);
    }
}