﻿using Newtonsoft.Json;
using PanasonicKiosk.Core.Helper;
using PanasonicKiosk.Core.Models;
using PanasonicKiosk.Core.Service;
using PanasonicKiosk.Modules.Banner.Helper;
using PanasonicKiosk.Modules.Banner.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace PanasonicKiosk.Modules.Banner.Views
{
    /// <summary>
    /// Interaction logic for ViewA.xaml
    /// </summary>
    public partial class BannerView : UserControl
    {

        private string PanasonicOriginalURL = "https://crm.pm.panasonic.com.my/OMSContent/";
        public static string DefaultCampaignImageLocation = "CampaignImages";
        public static string ApplicatonDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
        public static string CampaignImageDirectory = Path.Combine(ApplicatonDirectory, DefaultCampaignImageLocation);

        public CarouselImageViewModel BannerCarousel = new CarouselImageViewModel()
        {
            ImagesAddresses = new List<string>(),
            ImagesSources = new List<ImageSource>()
        };

        private int EffectIndex = 0, IntervalTimer = 5;

        private static string[] ValidImageExtensions = new[] { ".png", ".jpg", ".jpeg", ".bmp", ".gif" };
        private static string[] TransitionEffects = new[] { "Fade" };
        private string TransitionType, strImagePath = "";

        //private int CurrentSourceIndex, CurrentCtrlIndex, EffectIndex = 0, IntervalTimer = 1;
        protected Point SwipeStart;

        protected Point SwipeEnd;
        protected bool AlreadySwipe;

        public BannerView()
        {
            InitializeComponent();
            BannerCarousel.ImagesAddresses = new List<string>();

            var TargetUrl = new Uri(CoreSettingHelper.GetTargetUrl());
            using (var client = new HttpClient())
            {
                try
                {
                    CoreSettingHelper.ToggleAllowUnsafeHeaderParsing(true);
                    client.BaseAddress = TargetUrl;
                    //var content = new StringContent(JsonConvert.SerializeObject(rm), Encoding.UTF8, "application/json");
                    //client.DefaultRequestHeaders.Add("SearchKey", "");
                    client.DefaultRequestHeaders
                        .Accept
                        .Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //ACCEPT header

                    var address = client.BaseAddress.AbsoluteUri + "" + "api/Product/Campaign/List/Get";
                    //var Request = new HttpRequestMessage(HttpMethod.Get, address)
                    //{
                    //    Version = HttpVersion.Version10
                    //};
                    //client.DefaultRequestHeaders.CacheControl = new CacheControlHeaderValue
                    //{
                    //    NoCache = true
                    //};
                    var response = client.GetAsync(address).Result;
                   // var response = client.SendAsync(Request).Result;
                    
                    var jsontext = response.Content.ReadAsStringAsync();

                    var responseModel = JsonConvert.DeserializeObject<CampaignReceivingModel>(jsontext.Result.ToString());
                    CoreSettingHelper.ToggleAllowUnsafeHeaderParsing(false);
                    //fm = responseModel.Result;
                    if (responseModel != null && responseModel.Result.Count > 0)
                    {
                        foreach (var Campaign in responseModel.Result.Where(x => x.BannerFileName != null).ToList())
                        {
                            if (Campaign.BannerFileName != null && !string.IsNullOrWhiteSpace(Campaign.BannerFileName))
                            {
                                if (Campaign.IsCampaign)
                                {
                                    Campaign.BannerFileName = PanasonicOriginalURL + Campaign.BannerFileName;
                                }
                                else
                                {
                                    Campaign.BannerFileName = PanasonicOriginalURL + Campaign.BannerFileName;
                                }


                                var NormalizePath = Campaign.BannerFileName.Replace('/', '\\');
                                var PathArray = NormalizePath.Split('\\');
                                var FileName = PathArray[PathArray.Length - 1];
                                var FilePath = CampaignImageDirectory;

                                var BannerDownload = ImageUriDownloadHelper.DownloadImageAndSave(Campaign.BannerFileName, FileName, FilePath);
                                if (BannerDownload)
                                {
                                    BannerCarousel.ImagesAddresses.Add(Path.Combine(FilePath, FileName));
                                    BannerCarousel.CampaignIds.Add(Campaign.Id);
                                    BannerCarousel.CampaignNames.Add(Campaign.Name);

                                    //BannerCarousel.ImagesAddresses.Add(Path.Combine(FilePath, FileName));
                                    //BannerCarousel.CampaignIds.Add("2c5a8c93-a6b5-4c4f-94e1-4be0812df741");
                                    //BannerCarousel.CampaignNames.Add("OctoberFest");
                                }
                                //Product.SpecificationImageFileName = "pack://application:,,,/Assets/RichContent/RichContent.jpg";
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                }
            }
            //var RequestUrl = TargetUrl + "api/Product/Campaign/List/Get";
            //using (WebClient client = new WebClient())
            //{
            //    client.Headers[HttpRequestHeader.Accept] = "text/plain";


            //    //if (appConfig.UseCredentialsForWebClient)
            //    //{
            //    //    client.UseDefaultCredentials = false;
            //    //    client.Credentials = new NetworkCredential(appConfig.SAPUsername, appConfig.SAPPassword);
            //    //}

            //    var apiResult = client.UploadString(RequestUrl, "GET");

            //    //Console.WriteLine(requiredTime);
            //}
            //var RequestUrl = TargetUrl + "api/Product/Campaign/List/Get";
            //var httpWebRequest = WebRequest.CreateHttp(RequestUrl);
            //httpWebRequest.ContentType = "application/json; charset=utf-8";
            //httpWebRequest.Method = "GET";
            //httpWebRequest.Accept = "text/html, application/xhtml+xml, */*";
            //HttpRequestCachePolicy noCachePolicy = new HttpRequestCachePolicy(HttpRequestCacheLevel.NoCacheNoStore);
            //httpWebRequest.CachePolicy = noCachePolicy;
            ////using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            ////{
            ////    streamWriter.Write(jsonString);
            ////    streamWriter.Flush();
            ////}
            //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            //using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            //{
            //    var jsontext = streamReader.ReadToEnd();

            //    //var jsontext = response.Content.ReadAsStringAsync();

            //    var responseModel = JsonConvert.DeserializeObject<CampaignReceivingModel>(jsontext);

            //    //fm = responseModel.Result;
            //    if (responseModel != null && responseModel.Result.Count > 0)
            //    {
            //        foreach (var Campaign in responseModel.Result.Where(x => x.BannerFileName != null).ToList())
            //            if (Campaign.BannerFileName != null && !string.IsNullOrWhiteSpace(Campaign.BannerFileName))
            //            {
            //                if (Campaign.IsCampaign)
            //                {
            //                    Campaign.BannerFileName = PanasonicOriginalURL + Campaign.BannerFileName;
            //                }
            //                else
            //                {
            //                    Campaign.BannerFileName = PanasonicOriginalURL + Campaign.BannerFileName;
            //                }


            //                var NormalizePath = Campaign.BannerFileName.Replace('/', '\\');
            //                var PathArray = NormalizePath.Split('\\');
            //                var FileName = PathArray[PathArray.Length - 1];
            //                var FilePath = CampaignImageDirectory;

            //                var BannerDownload = ImageUriDownloadHelper.DownloadImageAndSave(Campaign.BannerFileName, FileName, FilePath);
            //                if (BannerDownload)
            //                {
            //                    BannerCarousel.ImagesAddresses.Add(Path.Combine(FilePath, FileName));
            //                    BannerCarousel.CampaignIds.Add(Campaign.Id);
            //                    BannerCarousel.CampaignNames.Add(Campaign.Name);

            //                    //BannerCarousel.ImagesAddresses.Add(Path.Combine(FilePath, FileName));
            //                    //BannerCarousel.CampaignIds.Add("2c5a8c93-a6b5-4c4f-94e1-4be0812df741");
            //                    //BannerCarousel.CampaignNames.Add("OctoberFest");
            //                }
            //                //Product.SpecificationImageFileName = "pack://application:,,,/Assets/RichContent/RichContent.jpg";
            //            }

            //    }
            //}

            //BannerCarousel.ImagesAddresses = new List<string>()
            //{
            //    //"Assets/PanasonicCampaignBanner/nanoe.png",
            //    //"Assets/PanasonicCampaignBanner/cooking.jpg",
            //    //"Assets/PanasonicCampaignBanner/desktop_banner_4.jpg",
            //    //"Assets/PanasonicCampaignBanner/nanoe_HD_PMEC_Desktop_W1440px_x_H450px.png",
            //};



            //IntervalTimer = Convert.ToInt32(ConfigurationManager.AppSettings["IntervalTime"]);
            BannerCarousel.CarouselImageControls = new[] { SliderImage1, SliderImage2 };

            LoadImageFolder(strImagePath, BannerCarousel);

            BannerCarousel.timerCarouselImageChange = new DispatcherTimer();
            BannerCarousel.timerCarouselImageChange.Interval = new TimeSpan(0, 0, IntervalTimer);
            BannerCarousel.timerCarouselImageChange.Tick += new EventHandler(timerBannerImageChange_Tick);

            //end internal

            Loaded += Window_Loaded;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            PlaySlideShow(BannerCarousel);
            BannerCarousel.timerCarouselImageChange.IsEnabled = true;
            //viewModel.Product = new ObservableCollection<ItemDetails>(GetItem(viewModel));
        }

        private void LoadImageFolder(string folder, CarouselImageViewModel ImageCarousel)
        {
            //ErrorText.Visibility = Visibility.Collapsed;
            var sw = System.Diagnostics.Stopwatch.StartNew();
            //if (!System.IO.Path.IsPathRooted(folder))
            //    folder = System.IO.Path.Combine(Environment.CurrentDirectory, folder);
            //if (!System.IO.Directory.Exists(folder))
            //{
            //    //ErrorText.Text = "The specified folder does not exist: " + Environment.NewLine + folder;
            //    //ErrorText.Visibility = Visibility.Visible;
            //    return;
            //}
            Random r = new Random();

            //var sources = from filepath in ImageCarousel.ImagesAddresses
            //              select ImageHelper.CreateImageSource("pack://application:,,,/" + filepath, true);
            var sources = from filepath in ImageCarousel.ImagesAddresses
                          select new BitmapImage(new Uri(filepath));
            
            //var sources = from file in new System.IO.DirectoryInfo(folder).GetFiles().AsParallel()
            //              where ValidImageExtensions.Contains(file.Extension, StringComparer.InvariantCultureIgnoreCase)
            //              orderby r.Next()
            //              select CreateImageSource(file.FullName, true);


            ImageCarousel.ImagesSources.Clear();
            ImageCarousel.ImagesSources.AddRange(sources);
            sw.Stop();
            Console.WriteLine("Total time to load {0} images: {1}ms", ImageCarousel.ImagesSources.Count, sw.ElapsedMilliseconds);
        }

        private void timerBannerImageChange_Tick(object sender, EventArgs e)
        {
            PlaySlideShow(BannerCarousel);
        }

        private void PlaySlideShow(CarouselImageViewModel ImageCarousel)
        {
            try
            {
                if (ImageCarousel.ImagesSources.Count == 0)
                    return;
                var oldCtrlIndex = ImageCarousel.CurrentCtrlIndex;
                ImageCarousel.CurrentCtrlIndex = (ImageCarousel.CurrentCtrlIndex + 1) % 2;
                ImageCarousel.CurrentSourceIndex = (ImageCarousel.CurrentSourceIndex + 1) % ImageCarousel.ImagesSources.Count;

                Image imgFadeOut = ImageCarousel.CarouselImageControls[oldCtrlIndex];
                Image imgFadeIn = ImageCarousel.CarouselImageControls[ImageCarousel.CurrentCtrlIndex];
                ImageSource newSource = ImageCarousel.ImagesSources[ImageCarousel.CurrentSourceIndex];
                imgFadeIn.Source = newSource;

                TransitionType = TransitionEffects[EffectIndex].ToString();

                Storyboard StboardFadeOut = (Resources[string.Format("{0}Out", TransitionType.ToString())] as Storyboard).Clone();
                StboardFadeOut.Begin(imgFadeOut);
                Storyboard StboardFadeIn = Resources[string.Format("{0}In", TransitionType.ToString())] as Storyboard;
                StboardFadeIn.Begin(imgFadeIn);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        public void SlideShowNext(CarouselImageViewModel ImageCarousel)
        {
            ImageCarousel.timerCarouselImageChange.Stop();
            PlaySlideShow(ImageCarousel);
            ImageCarousel.timerCarouselImageChange.Start();
        }

        public void SlideShowPrevious(CarouselImageViewModel ImageCarousel)
        {
            ImageCarousel.timerCarouselImageChange.Stop();
            if (ImageCarousel.CurrentSourceIndex - 2 < 0 && ImageCarousel.CurrentSourceIndex != 1)
            {
                ImageCarousel.CurrentSourceIndex = ImageCarousel.ImagesSources.Count - 2;
            }
            else if (ImageCarousel.CurrentSourceIndex == 1)
            {
                ImageCarousel.CurrentSourceIndex = ImageCarousel.ImagesSources.Count - 1;
            }
            else
            {
                ImageCarousel.CurrentSourceIndex = ImageCarousel.CurrentSourceIndex - 2;
            }
            PlaySlideShow(ImageCarousel);
            ImageCarousel.timerCarouselImageChange.Start();
        }

        private void SliderLeft_Click(object sender, RoutedEventArgs e)
        {
            SlideShowPrevious(BannerCarousel);
        }

        private void SliderRight_Click(object sender, RoutedEventArgs e)
        {
            SlideShowNext(BannerCarousel);
        }

        private void Slider_MouseDown(object sender, MouseButtonEventArgs e)
        {
            SwipeStart = e.GetPosition(this);
        }

        //private void HideLanding(object sender, MouseButtonEventArgs e)
        //{
        //    try
        //    {
        //        ((Storyboard)FindResource("animate")).Begin((Grid)sender);
        //        Listing.Visibility = Visibility.Visible;
        //        LandingCarousel.timerCarouselImageChange.Stop();
        //    }
        //    catch (Exception ex)
        //    {
        //    }

        //}

        private void Slider_MouseUp(object sender, MouseButtonEventArgs e)
        {
            SwipeEnd = e.GetPosition(this);
            AlreadySwipe = false;

            //if( < 10 )
            var Deviation = SwipeStart.X - SwipeEnd.X;
            if (-10 <= Deviation && Deviation <= 10)
            {
                
                var CampaignId = BannerCarousel.CampaignIds[BannerCarousel.CurrentCtrlIndex];
                if (!string.IsNullOrEmpty(CampaignId.Trim()))
                {
                    CampaignDetailViewModel rm = new CampaignDetailViewModel()
                    {
                        CampaignId = CampaignId,
                        CampaignName = BannerCarousel.CampaignNames[BannerCarousel.CurrentCtrlIndex]
                    }; 

                    var viewModel = (BannerViewModel)DataContext;
                    if (viewModel.LoadProductListingCommand.CanExecute(rm))
                        viewModel.LoadProductListingCommand.Execute(rm);
                }
            }
        }

        private void Slider_MouseMove(object sender, MouseEventArgs e)
        {
            if (!AlreadySwipe)
            {
                if (SwipeStart != null && SwipeEnd.X > (SwipeStart.X + 200))
                {
                    SlideShowNext(BannerCarousel);
                    AlreadySwipe = true;
                }

                //Swipe Right
                if (SwipeStart != null && SwipeEnd.X < (SwipeStart.X - 200))
                {
                    SlideShowPrevious(BannerCarousel);
                    AlreadySwipe = true;
                }
            }
            e.Handled = true;
        }
    }
}