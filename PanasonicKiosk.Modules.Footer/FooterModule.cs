﻿using PanasonicKiosk.Core;
using PanasonicKiosk.Modules.Footer.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace PanasonicKiosk.Modules.Footer
{
    public class FooterModule : IModule
    {
        private readonly IRegionManager _regionManager;

        public FooterModule(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }

        public void OnInitialized(IContainerProvider containerProvider)
        {
            _regionManager.RegisterViewWithRegion(RegionNames.FooterRegion, typeof(FooterView));
            //S_regionManager.RegisterViewWithRegion(RegionNames.DetailSearchBarRegion, typeof(FooterView));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
        }
    }
}