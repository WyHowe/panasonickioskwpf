﻿using System.Windows.Controls;

namespace PanasonicKiosk.Modules.ClearConfirmation.Views
{
    /// <summary>
    /// Interaction logic for ViewA.xaml
    /// </summary>
    public partial class ClearConfirmationView : UserControl
    {
        public ClearConfirmationView()
        {
            InitializeComponent();
        }
    }
}