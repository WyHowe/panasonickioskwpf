﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanasonicKiosk.Core.Sunmi.ViewModel
{
    public class SunmiP1RequestViewModel
    {
        public string appId;
        public string appType;
        public string transType;
        public long amount;
        public string payCode;
        public string orderId;
        public string oriOrderId;
    }
    public class SunmiP1SalePreAuthRequestViewModel
    {
        public string appId;
        public string appType;
        public string transType;
        public long amount;
        public string orderId;
    }
    public class SunmiP1RefundRequestViewModel
    {
        public string appId;
        public string appType;
        public string transType;
        public long amount;
        public string orderId;
        public string oriOrderId;
    }
    public class SunmiP1ResponseViewModel
    {
        public string appId;
        public string appType;
        public string transType;
        public string misId;
        public string orderId;
        public string platformId;
        public string businessId;
        public long amount;
        public string oriOrderId;
        public string resultMsg;
        public string resultCode;
        public string model;
        public string version;
        public string transDate;
        public string transTime;
        public string operatorId;
        public string terminalId;
        public string merchantId;
        public string merchantName;
        public string cardNum;
        public string cardType;
        public string issuer;
        public string acquirer;
        public string accountType;
        public string authNum;
        public string batchNum;
        public string voucherNum;
        public string referenceNum;
        public string platform;
        public int transactionType;
        public int transactionPlatform;
    }
}
