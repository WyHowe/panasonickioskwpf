﻿using Prism.Ioc;
using Prism.Modularity;

namespace PanasonicKiosk.Modules.Header
{
    public class HeaderModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
        }
    }
}