﻿using PanasonicKiosk.Core;
using PanasonicKiosk.Modules.OrderSuccess.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace PanasonicKiosk.Modules.OrderSuccess
{
    public class OrderSuccessModule : IModule
    {
        private readonly IRegionManager _regionManager;

        public OrderSuccessModule(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }

        public void OnInitialized(IContainerProvider containerProvider)
        {
            _regionManager.RegisterViewWithRegion(RegionNames.SuccessOrderRegion, typeof(OrderSuccessView));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
        }
    }
}