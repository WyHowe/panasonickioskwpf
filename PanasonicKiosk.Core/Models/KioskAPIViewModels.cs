﻿using System;
using System.Collections.Generic;

namespace PanasonicKiosk.Core.Models
{
    public class KSK_ProductViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string ParentCategoryId { get; set; }
        public string ParentCategoryName { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string Specification { get; set; }
        public string SpecificationImageFileName { get; set; }
        public IList<KSK_ProductItemViewModel> Items { get; set; }
    }

    public class KSK_ProductItemViewModel
    {
        public string Id { get; set; }
        public string Sku { get; set; }
        public string UnitOfMeasurement { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public string Color { get; set; }
        public decimal Amount { get; set; }
        public string SpecialType { get; set; }
        public decimal SpecialAmount { get; set; }
        public DateTime? SpecialFrom { get; set; }
        public DateTime? SpecialTo { get; set; }
        public IList<KSK_ProductItemImageViewModel> Images { get; set; }
    }

    public class KSK_ProductItemImageViewModel
    {
        public string ImageFileName { get; set; }
    }

    public class KSK_CampaignProductViewModel
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool IsCampaign { get; set; }
        public string BannerFileName { get; set; }
        public IList<KSK_ProductViewModel> Products { get; set; }
    }
}