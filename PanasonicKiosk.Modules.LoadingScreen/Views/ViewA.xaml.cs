﻿using System.Windows.Controls;

namespace PanasonicKiosk.Modules.LoadingScreen.Views
{
    /// <summary>
    /// Interaction logic for ViewA.xaml
    /// </summary>
    public partial class ViewA : UserControl
    {
        public ViewA()
        {
            InitializeComponent();
        }
    }
}