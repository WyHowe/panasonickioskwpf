﻿using PanasonicKiosk.Core.Models.SignalRModels;

namespace PanasonicKiosk.Core.IHubSync.Client
{
    public interface ISendHubSync
    {
        //void AddMessage(string name, string message);

        void Heartbeat();

        //void Heartbeat2();
        void PaymentTerminalHeartBeat(TerminalStatusModel TerminalStatus);

        void SendHelloObject(HelloModel hello);
    }
}