﻿using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PanasonicKiosk.Modules.Banner.Helper
{
    public class ImageHelper
    {
        public static ImageSource CreateImageSource(string file, bool forcePreLoad)
        {
            if (forcePreLoad)
            {
                //new Uri(BannerAddress[CurrentImagePosition], UriKind.Relative)
                var src = new BitmapImage();
                src.BeginInit();
                src.UriSource = new Uri(file, UriKind.Absolute);
                src.CacheOption = BitmapCacheOption.OnLoad;
                src.EndInit();
                src.Freeze();
                return src;
            }
            else
            {
                var src = new BitmapImage(new Uri(file, UriKind.Absolute));
                src.Freeze();
                return src;
            }
        }
    }
}