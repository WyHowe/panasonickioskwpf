﻿//namespace PanasonicKiosk.Core.K9Access
//{
//    public interface IK9MTGPService
//    {
//        int ClearBatch(byte[] respdata, int respdataoffset);
//        int Connect(int comportnumber, int baudrate);
//        int Connect();
//        int Connection_test(int type, byte[] respdata, int respdataoffset);
//        int Disconnect();
//        int Display_fare(int validCode, int sale_amount, byte[] respdata, int respdataoffset);
//        int Getinfo(byte[] respdata, int respdataoffset);
//        int InitEntrySingleSale(int sale_amount, byte[] respdata, int respdataoffset);
//        int Initialize_emv_reader();
//        int InitSingleSale(int sale_amount, byte[] respdata, int respdataoffset);
//        int InjectMidTid(string mid, string tid, byte[] respdata, int respdataoffset);
//        int InjectMisc(string preauthAmt, string autoSettleTime, string fareServerIp, string fareServerPort, string bankServerIp, string bankServerPort, byte[] respdata, int respdataoffset);
//        int InjectNmxKey(string nmxKeys, byte[] respdata, int respdataoffset);
//        int Poll_reader(byte[] respdata, int respdataoffset);
//        int Poll_sendecpi(string strCmd, int timeOutMs, byte[] respdata, int respdataoffset);
//        int ProceedEntrySingleSale(byte[] respdata, int respdataoffset);
//        int ProceedSingleSale(byte[] respdata, int respdataoffset);
//        int ProceedTunneling(int validCode, int sale_amount, byte[] respdata, int respdataoffset);
//        int Reboot(byte[] respdata, int respdataoffset);
//        int Reset_reader(byte[] respdata, int respdataoffset);
//        int Send(byte command, byte[] data, int dataoffset, int datalen, byte[] respstatus, int respstatusoffset, byte[] respdata, int respdataoffset, int Timeoutms);
//        int Send_entry_ack(byte[] respdata, int respdataoffset);
//        void SetBaudRate(int Baudrate);
//        void SetComPortNo(int Port);
//        int Send_recovery(int seqNo, byte[] respdata, int respdataoffset);
//        int SetDeviceIp(byte dhcp, string ipaddr, string netmask, string gateway, byte[] respdata, int respdataoffset);
//        int Settlement(byte[] respdata, int respdataoffset);
//        int Transact_cancel();
//        int Transact_proc_exit_sale(int sale_amount, byte[] respdata, int respdataoffset);
//        int Transact_proc_sale_entry(int sale_amount, byte[] respdata, int respdataoffset);
//        int Transact_sale_entry(int sale_amount, byte[] respdata, int respdataoffset);
//        int Transact_sale_exit(int sale_amount, byte[] respdata, int respdataoffset);
//    }
//}