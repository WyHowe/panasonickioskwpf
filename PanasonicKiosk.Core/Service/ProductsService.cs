﻿using PanasonicKiosk.Core.Helper;
using PanasonicKiosk.Core.Models;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;

namespace PanasonicKiosk.Core.Service
{
    public class ProductService : IProductService
    {
        private IAPIAccessService APIDataSource;
        private IEventAggregator eventAggregator;
        private string PanasonicURL = "https://crm.pm.panasonic.com.my/OMSContent/Images/Products/Actual_D/";
        private string PanasonicOriginalURL = "https://crm.pm.panasonic.com.my/OMSContent/Images/Products/Original/";
        public static string DefaultProductImageLocation = "ProductsImage";
        public static string DefaultProductSpecificationImageLocation = "ProductsSpecificationImage";
        public static string ApplicatonDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
        public static string ProductImageDirectory = Path.Combine(ApplicatonDirectory, DefaultProductImageLocation);
        public static string ProductSpecificationImageDirectory = Path.Combine(ApplicatonDirectory, DefaultProductSpecificationImageLocation);
        public int CurrentCategoryId { get; set; }

        public ProductService(IAPIAccessService datasource, IEventAggregator eventAggregator)
        {
            this.APIDataSource = datasource;
            this.eventAggregator = eventAggregator;
        }

        public async Task<List<ItemDetails>> GetItem(ItemRequestModel rm)
        {
            List<ItemDetails> products = new List<ItemDetails>();
            List<ItemDetails> RawProduct = new List<ItemDetails>();
            try
            {
                //ItemRequestModel rm = new ItemRequestModel();
                RawProduct = await APIDataSource.GetProducts(rm);
                // products = products.Where(x.ToList();
                //if (!string.IsNullOrEmpty(SearchBox))
                //{
                //    products = products.Where(x => x.Name.ToLower().Contains(SearchBox.ToLower())).ToList();
                //}
                if (RawProduct.Count > 0)
                {
                    //products = products.Where(x => x.Items.Any(a => a.ProductModel.Amounts.Any())).ToList();
                    foreach (var Product in RawProduct)
                    {
                        if (Product.SpecificationImageFileName != null)
                        {
                            List<Task<bool>> DownloadImageTask = new List<Task<bool>>();

                            Product.SpecificationImageFileName = PanasonicOriginalURL + Product.SpecificationImageFileName;

                            var NormalizePath = Product.SpecificationImageFileName.Replace('/', '\\');
                            var PathArray = NormalizePath.Split('\\');
                            var FileName = PathArray[PathArray.Length - 1];
                            var FilePath = ProductImageDirectory;

                            DownloadImageTask.Add(ImageUriDownloadHelper.DownloadImageAndSaveAsync(Product.SpecificationImageFileName, FileName, FilePath));
                            var result = await Task.WhenAll(DownloadImageTask);
                            //Product.SpecificationImageFileName = "pack://application:,,,/Assets/RichContent/RichContent.jpg";
                        }
                        //if (Product.Category != null)
                        //{
                        if (Product.Items != null && Product.Items.Count > 0)
                        {
                            var LowestSpecialPriceItem = new ProductModelViewModel();
                            if (Product.IsAllOutOfStock)
                            {
                                LowestSpecialPriceItem = Product.Items.OrderBy(x => x.SpecialAmount).ThenBy(x => x.Amount).FirstOrDefault();
                                var LowestSpecialPrice = LowestSpecialPriceItem.SpecialAmount;
                                var LowestPrice = LowestSpecialPriceItem.Amount;
                                Product.MemberPrice = "RM " + LowestPrice.ToString("N2");

                                //public string SpecialPrice { get; set; }
                                //public string ShowDiscountBlock { get; set; }
                                //public string ShowNonDiscountBlock { get; set; }

                                if (LowestPrice > LowestSpecialPrice && LowestSpecialPrice != 0)
                                {
                                 
                                    Product.SpecialPrice = "RM " + LowestSpecialPrice.ToString("N2");
                                    int DiscountPercentage = Convert.ToInt32(100 - (LowestPrice / LowestSpecialPrice * 100));
                                    Product.DiscountPercentage = "" + DiscountPercentage + "%";
                                    Product.ShowNonDiscountBlock = false;
                                    Product.ShowDiscountBlock = true;
                                    Product.OnSale = DiscountPercentage > 5;
                                }
                                else
                                {
                                    Product.SpecialPrice = "RM " + LowestSpecialPrice.ToString("N2");
                                    int DiscountPercentage = 100;
                                    Product.DiscountPercentage = "" + DiscountPercentage + "%";
                                    Product.OnSale = false;
                                    Product.ShowDiscountBlock = false;
                                    Product.ShowNonDiscountBlock = true;
                                }
                                //Product.OnSale = false;
                                Product.Items = Product.Items.OrderBy(x => x.SpecialAmount).ThenBy(x => x.Amount).ToList();
                            }
                            else
                            {
                                LowestSpecialPriceItem = Product.Items.Where(x => x.IsOutOfStock == false).OrderBy(x => x.SpecialAmount).ThenBy(x => x.Amount).FirstOrDefault();
                                var LowestSpecialPrice = LowestSpecialPriceItem.SpecialAmount;
                                var LowestPrice = LowestSpecialPriceItem.Amount;
                                Product.MemberPrice = "RM " + LowestPrice.ToString("N2");

                                //public string SpecialPrice { get; set; }
                                //public string ShowDiscountBlock { get; set; }
                                //public string ShowNonDiscountBlock { get; set; }

                                if (LowestPrice > LowestSpecialPrice && LowestSpecialPrice != 0)
                                {
                                    
                                    Product.SpecialPrice = "RM " + LowestSpecialPrice.ToString("N2");
                                    //int DiscountPercentage = Convert.ToInt32(100 - (LowestPrice / LowestSpecialPrice * 100));
                                    int DiscountPercentage = Convert.ToInt32((100/LowestPrice) * (LowestPrice -LowestSpecialPrice));
                                    Product.DiscountPercentage = "" + DiscountPercentage + "%";
                                    Product.ShowNonDiscountBlock = false;
                                    Product.ShowDiscountBlock = true;
                                    Product.OnSale = DiscountPercentage > 5;
                                }
                                else
                                {
                                    Product.SpecialPrice = "RM " + LowestSpecialPrice.ToString("N2");
                                    int DiscountPercentage = 100;
                                    Product.DiscountPercentage = "" + DiscountPercentage + "%";
                                    Product.OnSale = false;
                                    Product.ShowDiscountBlock = false;
                                    Product.ShowNonDiscountBlock = true;
                                }
                                //Product.OnSale = false;
                                Product.Items = Product.Items.OrderBy(x => x.IsOutOfStock).ThenBy(x => x.SpecialAmount).ThenBy(x => x.Amount).ToList();
                            }

                            List<Task<bool>> DownloadImageTask = new List<Task<bool>>();
                            foreach (var ItemSku in Product.Items)
                            {
                                foreach (var Image in ItemSku.Images)
                                {
                                    var DownloadImageUrl = PanasonicURL + Image;
                                    DownloadImageUrl = PanasonicURL + HttpUtility.UrlEncode(Image);
                                    var NormalizePath = DownloadImageUrl.Replace('/', '\\');
                                    var PathArray = NormalizePath.Split('\\');
                                    var FileName = PathArray[PathArray.Length - 1];
                                    var FilePath = ProductImageDirectory;

                                    DownloadImageTask.Add(ImageUriDownloadHelper.DownloadImageAndSaveAsync(DownloadImageUrl, FileName, FilePath));
                                }
                            }
                            var result = await Task.WhenAll(DownloadImageTask);
                            if (LowestSpecialPriceItem.Images.Count > 0)
                            {
                                Product.ImageUrl = LowestSpecialPriceItem.Images.Select(s => s).First();
                                Product.ImageUrl = PanasonicURL + Product.ImageUrl;
                            }
                            else
                            {
                                if(Product.Items.Count( w=> w.Images.Count > 0) > 0)
                                {
                                    var ProductItemWithImages = Product.Items.Where(w => w.Images.Count > 0).First();
                                    Product.ImageUrl = ProductItemWithImages.Images.Select(s => s).First();
                                    Product.ImageUrl = PanasonicURL + Product.ImageUrl;
                                }
                                else
                                {
                                    string ProductImage = "pack://application:,,,/Assets/no_image_available.png";
                                    Product.ImageUrl = ProductImage;
                                }
                            }
                            products.Add(Product);
                            //}
                        }
                        else
                        {
                            //string ProductImage = "pack://application:,,,/Assets/no_image_available.png";
                            //Product.ImageUrl = ProductImage;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            return products;
        }

        public async Task<List<CategoryViewModel>> GetSubCategoriesProducts(SidePanelItem CategoryDetails)
        {
            var SubCategories = await APIDataSource.GetSubCategory(CategoryDetails.CategoryId);
            return SubCategories;
        }

        public async Task<OrderSubmitionReturnModel> SubmitOrder(OrderHeaderModel newOrder)
        {
            var result = await APIDataSource.SubmitOrder(newOrder);
            return result;
        }

        public async Task<List<CategoryViewModel>> GetSearchCategories(string Searchkey)
        {
            var SubCategories = await APIDataSource.GetSearchCategory(Searchkey);
            return SubCategories;
        }
        public async Task<List<CategoryViewModel>> GetCampaignCategories(string CampaignId)
        {
            var SubCategories = await APIDataSource.GetCampaignCategory(CampaignId);
            return SubCategories;
        }
    }
}