﻿using PanasonicKiosk.Core.Sunmi.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanasonicKiosk.Core.Sunmi.Interface
{
    public interface ISummiSocketService
    {
        string CreateSunmiHexString(string RequestJson);
        SunmiP1ResponseViewModel ProcessSunmiResponse(string EventData);
    }
}
