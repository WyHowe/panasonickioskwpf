﻿using PanasonicKiosk.Modules.Checkout.ViewModels;
using System.Windows.Controls;

namespace PanasonicKiosk.Modules.Checkout.Views
{
    /// <summary>
    /// Interaction logic for ViewA.xaml
    /// </summary>
    public partial class CheckoutView : UserControl
    {
        public CheckoutView()
        {
            InitializeComponent();
        }
    }
}