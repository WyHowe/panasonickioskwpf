﻿using PanasonicKiosk.Core.Helper;
using PanasonicKiosk.Core.Models;
using Prism.Events;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace PanasonicKiosk.Core.Service
{
    public class SidePanelService : ISidePanelService
    {
        private IAPIAccessService APIDataSource;
        private IEventAggregator eventAggregator;
        public int CurrentEmployeeId { get; set; }
        private string _CategoryImageURL = "https://crm.pm.panasonic.com.my/OMSContent/Images/Categories/Original/";
        public static string DefaultSubCategoryImageLocation = "CategoryImages";
        public static string ApplicatonDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
        public static string SubCategoryImageDirectory = Path.Combine(ApplicatonDirectory, DefaultSubCategoryImageLocation);
        public SidePanelService(IAPIAccessService datasource, IEventAggregator eventAggregator)
        {
            this.APIDataSource = datasource;
            this.eventAggregator = eventAggregator;
        }

        public async Task<List<SidePanelItem>> GetSidePanels()
        {
            var PanaCategories = await APIDataSource.GetRootCategories();

            //CategoryImageFileAddress = Path.Combine(SubCategoryImageDirectory, x.ImageFileName),

            List<string> IconLocalPath = new List<string>()
            {
                "pack://application:,,,/Assets/PanasonicCategoryIcons/TH-65HZ1500_00.png",
                "pack://application:,,,/Assets/PanasonicCategoryIcons/HC_01-01.png",
                "pack://application:,,,/Assets/PanasonicCategoryIcons/HA_1-01.png",
                "pack://application:,,,/Assets/PanasonicCategoryIcons/KA_1-01.png",
                "pack://application:,,,/Assets/PanasonicCategoryIcons/BIA_01.png",
                "pack://application:,,,/Assets/PanasonicCategoryIcons/BH_1.png",
                "pack://application:,,,/Assets/PanasonicCategoryIcons/health_R.png",
                "pack://application:,,,/Assets/PanasonicCategoryIcons/K-KJ18MCC4_02-04.png",
                "pack://application:,,,/Assets/PanasonicCategoryIcons/Light_00.png",
                //"pack://application:,,,/Assets/PanasonicCategoryIcons/accessories_parts.png",
                "pack://application:,,,/Assets/PanasonicCategoryIcons/accessories_parts.png",
            };

            var SidePanels = new List<SidePanelItem>();
            var i = 0;
            foreach (var RootCategory in PanaCategories.OrderBy(o => o.Position))
            {
                var CategoryDownload = await ImageUriDownloadHelper.DownloadImageAndSaveAsync(
                    _CategoryImageURL + RootCategory.ImageFileName, 
                    RootCategory.ImageFileName, 
                    SubCategoryImageDirectory);

                var SidePanel = new SidePanelItem(RootCategory.Name, Path.Combine(SubCategoryImageDirectory, RootCategory.ImageFileName));
                SidePanel.CategoryId = RootCategory.Id;
                SidePanels.Add(SidePanel);
                i++;
            }
            return SidePanels;
        }
    }
}