﻿namespace PanasonicKiosk.Core.K9Access
{
    public interface IK9CommandService
    {
        string Bytes_To_String(byte[] bytes_Input);

        byte Calculate_BCC(byte[] data);

        string GetToday();

        byte[] HexStringToByteArray(string s);

        string HexToString(string hex);

        string RespGetCard(string cmd, string data, ref string SALE_TID, ref string SALE_DT, ref string MASK_PAN, ref string HASH_PAN, ref string RRN, ref string STAN, ref string APPR_CODE, ref string ADD_DATA);

        string RespGetConnectionTest(string msg);

        string RespGetSEQ(string msg);

        string RespGetStatus(string msg);

        string SendCmd(string cmd, string data);

        string SetCurrentDate();

        string StringToHex(string text);
    }
}