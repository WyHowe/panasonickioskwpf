﻿using PanasonicKiosk.Core.Events;
using PanasonicKiosk.Core.Models;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using System.Windows.Input;

namespace PanasonicKiosk.Modules.ClearConfirmation.ViewModels
{
    public class ClearConfirmationViewModel : BindableBase, INavigationAware, IRegionMemberLifetime
    {
        private IEventAggregator _eventAggregator;
        private DelegateCommand _YesCommand;
        private DelegateCommand _NoCommand;
        private string _message;
        private string _InfoImage;
        private ShoppingCartEntryViewModel _Entry;

        public string InfoImage
        {
            get { return _InfoImage; }
            set { SetProperty(ref _InfoImage, value); }
        }

        public ShoppingCartEntryViewModel Entry
        {
            get
            {
                return _Entry;
            }
            set
            {
                SetProperty(ref _Entry, value);
            }
        }

        public ClearConfirmationViewModel(IEventAggregator eventAggregator)
        {
            this._eventAggregator = eventAggregator;
            _YesCommand = new DelegateCommand(loadDeleteItem, canHideConfirmation);
            _NoCommand = new DelegateCommand(loadKeepItem);
            InfoImage = "pack://application:,,,/Assets/exclamation.gif";

            //Message = "View A from your Prism Module";
        }

        public string Message
        {
            get
            {
                return _message;
            }
            set
            {
                SetProperty(ref _message, value);
            }
        }

        public ICommand YesCommand
        {
            get
            {
                return _YesCommand;
            }
        }

        public ICommand NoCommand
        {
            get
            {
                return _NoCommand;
            }
        }

        public bool KeepAlive
        {
            get
            {
                return false;
            }
        }

        private void loadKeepItem()
        {
            _eventAggregator.GetEvent<KeepItemFromCartEvent>().Publish(Entry);
            _eventAggregator.GetEvent<HideClearConfirmationScreenEvent>().Publish();
        }

        private bool canHideConfirmation()
        {
            return true;
        }

        private void loadDeleteItem()
        {
            _eventAggregator.GetEvent<RemoveItemFromCartEvent>().Publish(Entry);
            _eventAggregator.GetEvent<HideClearConfirmationScreenEvent>().Publish();
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            var SelectedEntry = navigationContext.Parameters["Entry"] as ShoppingCartEntryViewModel;
            if (SelectedEntry != null)
            {
                Entry = SelectedEntry;
                Message = "Remove " + SelectedEntry.ProductModel.Sku + " from cart?";
            }
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
            //throw new NotImplementedException();
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            //throw new NotImplementedException();
        }
    }
}