﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanasonicKiosk.Core.Helper
{

    public static class StringHelper
    {
        public static string Left(this string text, int length)
        {
            return text.Substring(0, Math.Min(text.Length, length));
        }

        public static string Right(this string text, int length)
        {
            return text.Substring(text.Length - Math.Min(text.Length, length));
        }

        public static string AddOrdinal(int num)
        {
            if (num <= 0) return num.ToString();

            switch (num % 100)
            {
                case 11:
                case 12:
                case 13:
                    return num + "th";
            }

            switch (num % 10)
            {
                case 1:
                    return num + "st";
                case 2:
                    return num + "nd";
                case 3:
                    return num + "rd";
                default:
                    return num + "th";
            }
        }

        public static string ToPhoneNumberFormat(this string value, bool hasPlus = true)
        {
            if (!string.IsNullOrEmpty(value))
            {
                value = value.Replace(" ", "");

                value = new string(value.Where(c => char.IsDigit(c)).ToArray());

                if (hasPlus)
                {
                    if (value.StartsWith("0"))
                    {
                        value = "+6" + value;
                    }
                    else if (value.StartsWith("60"))
                    {
                        value = "+" + value;
                    }
                }
                else
                {
                    if (value.StartsWith("0"))
                    {
                        value = "6" + value;
                    }
                }

                return value;
            }
            else
            {
                return "";
            }
        }
        public static string LastDayInLeapYearForLastYearYYYYMMDD(string dateTo)
        {
            if (Convert.ToInt32(dateTo.Substring(3, 2)) == 2 && Convert.ToInt32(dateTo.Substring(0, 2)) == 29)
            {
                return "28/02/" + (Convert.ToInt32(dateTo.Right(4)) - 1).ToString();

            }
            else
            {
                return (Convert.ToInt32(dateTo.Left(4)) - 1).ToString() + dateTo.Right(4);
            }
        }
        public static string LastDayOfMonth(string date)
        {
            int year = Convert.ToInt16(date.Substring(2, 4));
            int month = Convert.ToInt16(date.Substring(0, 2));
            string Day = DateTime.DaysInMonth(year, month).ToString();
            return Day;
        }

        public static string ReplaceBarAndTab(this string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return "";
            }

            return text.Replace("\t", " ")
                       .Replace('\u0009'.ToString(), " ")
                       .Replace("|", " ");
        }
    }
}
