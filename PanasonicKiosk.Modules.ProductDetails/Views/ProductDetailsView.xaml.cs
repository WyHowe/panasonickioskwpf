﻿using PanasonicKiosk.Modules.ProductDetails.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PanasonicKiosk.Modules.ProductDetails.Views
{
    /// <summary>
    /// Interaction logic for ProductDetailsView.xaml
    /// </summary>
    public partial class ProductDetailsView : UserControl
    {
        public ProductDetailsView()
        {
            InitializeComponent();
            //DataContext = vm;
        }

        private void animatedScrollViewer_ManipulationBoundaryFeedback(object sender, System.Windows.Input.ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void browser_IsBrowserInitializedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            string Random = "\u003ciframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/nkWoSWnfA8s\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen\u003e\u003c/iframe\u003e";
            ///browser.LoadHtml("<html><head></head><body>"+Random+"</body></html>", "http://www.example.com/");
        }
    }
}