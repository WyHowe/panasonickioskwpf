﻿using PanasonicKiosk.Core;
using PanasonicKiosk.Modules.Banner.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace PanasonicKiosk.Modules.Banner
{
    public class BannerModule : IModule
    {
        private readonly IRegionManager _regionManager;

        public BannerModule(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }

        public void OnInitialized(IContainerProvider containerProvider)
        {
            _regionManager.RegisterViewWithRegion(RegionNames.BannerRegion, typeof(BannerView));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
        }
    }
}