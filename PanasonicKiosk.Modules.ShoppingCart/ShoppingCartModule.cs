﻿using PanasonicKiosk.Core;
using PanasonicKiosk.Modules.ShoppingCart.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace PanasonicKiosk.Modules.ShoppingCart
{
    public class ShoppingCartModule : IModule
    {
        private readonly IRegionManager _regionManager;

        public ShoppingCartModule(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }

        public void OnInitialized(IContainerProvider containerProvider)
        {
            _regionManager.RegisterViewWithRegion(RegionNames.TransactionPrimaryRegion, typeof(ShoppingCartView));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<ShoppingCartView>();
        }
    }
}