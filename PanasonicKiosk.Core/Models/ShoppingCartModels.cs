﻿using Prism.Mvvm;
using System.Collections.Generic;

namespace PanasonicKiosk.Core.Models
{
    public class ShoppingCartEntryViewModel : BindableBase
    {
        private int _Quantity;
        private decimal _Price;
        private string _PriceString;
        private string _TotalPriceString;
        private decimal _ShippingCost;
        private int _ItemTaxPercent;
        private int _ShoppingTaxPercentage;

        public ProductModelViewModel ProductModel { get; set; }
        public ItemDetails Product { get; set; }

        public decimal ShippingCost
        {
            get
            {
                return _ShippingCost;
            }
            set
            {
                SetProperty(ref _ShippingCost, value);
            }
        }

        public bool _IsSingleProductModel;

        public bool IsSingleProductModel
        {
            get
            {
                return _IsSingleProductModel;
            }
            set
            {
                SetProperty(ref _IsSingleProductModel, value);
            }
        }

        public bool _IsMultiProductModel;

        public bool IsMultiProductModel
        {
            get
            {
                return _IsMultiProductModel;
            }
            set
            {
                SetProperty(ref _IsMultiProductModel, value);
            }
        }

        private string _SelectedProductModel;

        public string SelectedProductModel
        {
            get
            {
                return _SelectedProductModel;
            }
            set
            {
                SetProperty(ref _SelectedProductModel, value);
            }
        }

        private string _SingleProductName;

        public string SingleProductName
        {
            get
            {
                return _SingleProductName;
            }
            set
            {
                SetProperty(ref _SingleProductName, value);
            }
        }

        public int ItemTaxPercent
        {
            get
            {
                return _ItemTaxPercent;
            }
            set
            {
                SetProperty(ref _ItemTaxPercent, value);
            }
        }

        public int ShoppingTaxPercentage
        {
            get
            {
                return _ShoppingTaxPercentage;
            }
            set
            {
                SetProperty(ref _ShoppingTaxPercentage, value);
            }
        }

        public int Quantity
        {
            get
            {
                return _Quantity;
            }
            set
            {
                SetProperty(ref _Quantity, value);
            }
        }

        public decimal Price
        {
            get
            {
                return _Price;
            }
            set
            {
                SetProperty(ref _Price, value);
            }
        }

        public string PriceString
        {
            get
            {
                return _PriceString;
            }
            set
            {
                SetProperty(ref _PriceString, value);
            }
        }

        public string TotalPriceString
        {
            get
            {
                return _TotalPriceString;
            }
            set
            {
                SetProperty(ref _TotalPriceString, value);
            }
        }
    }

    public class ShoppingCartModel : BindableBase
    {
        private List<ShoppingCartEntryViewModel> _ShoppingEntries;
        private AddressViewModel _DeliveryAddress;
        private ShoppingCartCostsSummary _Summary;

        public List<ShoppingCartEntryViewModel> ShoppingEntries
        {
            get
            {
                return _ShoppingEntries;
            }
            set
            {
                SetProperty(ref _ShoppingEntries, value);
            }
        }

        public AddressViewModel DeliveryAddress
        {
            get
            {
                return _DeliveryAddress;
            }
            set
            {
                SetProperty(ref _DeliveryAddress, value);
            }
        }

        public ShoppingCartCostsSummary Summary
        {
            get
            {
                return _Summary;
            }
            set
            {
                SetProperty(ref _Summary, value);
            }
        }

        public ShoppingCartModel()
        {
            ShoppingEntries = new List<ShoppingCartEntryViewModel>();
            DeliveryAddress = new AddressViewModel();
            Summary = new ShoppingCartCostsSummary();
        }
    }

    public class ShoppingCartCostsSummary
    {
        public decimal ItemsSubtotal { get; set; }
        public decimal Shipping { get; set; }
        public decimal ItemsTax { get; set; }
        public decimal ShippingTax { get; set; }
        public decimal TotalTax { get; set; }
        public decimal Total { get; set; }
    }

    public class AddressViewModel
    {
        public string RecipientName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string PostCode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
    }
}