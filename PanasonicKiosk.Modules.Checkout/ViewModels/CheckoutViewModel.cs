﻿using Newtonsoft.Json;
using PanasonicKiosk.Core;
using PanasonicKiosk.Core.Events;
using PanasonicKiosk.Core.Helper;
using PanasonicKiosk.Core.K9Access;
using PanasonicKiosk.Core.Models;
using PanasonicKiosk.Core.Service;
using PanasonicKiosk.Core.Sunmi.Helper;
using PanasonicKiosk.Core.Sunmi.Interface;
using PanasonicKiosk.Core.Sunmi.ViewModel;
using Prism.Commands;
using Prism.Events;
using Prism.Modularity;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using WebSocketSharp;

namespace PanasonicKiosk.Modules.Checkout.ViewModels
{
    public class CheckoutViewModel : BindableBase, INavigationAware
    {
        private IModuleManager _moduleManager;
        private IEventAggregator _eventAggregator;
        private IRegionManager _regionManager;
        
        private IProductService _productService;
        private IKioskConnectorService _kioskConnectorService;
        private IK9CommandService _k9CommandService;
        private ISummiSocketService _sunmiService;
        //public string RESPCODE = "";
        //private int CmdState = 0;
        //private string SALE_TID = "";
        //private string SALE_DT = "";
        //private string MASK_PAN = "";
        //private string HASH_PAN = "";
        //private string RRN = "";
        //private string STAN = "";
        //private string APPR_CODE = "";
        //private string ADD_DATA = "";
        private bool KioskMode = true;

        int millisecondsDelay = 0;
        private string _ShippingAndTax;
        private string _SubTotal;
        private string _GrandTotal;
        private string _MerchantContactInfo;
        private string _MerchantAddress;
        private CancellationTokenSource OuterCancellationToken;
        private CancellationTokenSource InnerCancellationToken;
        private ObservableCollection<ShoppingCartEntryViewModel> _ShoppingEntries;
        public DelegateCommand<KeyEventArgs> KeyUpEventCommand { get; private set; }
        private DelegateCommand<ItemDetails> _loadProductDetailCommand;

        public static string DefaultSunnmiPaymentLogLocation = "SunmiPaymentLog";
        public static string ApplicatonDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
        public static string SunmiPaymentLogDirectory = Path.Combine(ApplicatonDirectory, DefaultSunnmiPaymentLogLocation);
        private string FullPath;
        private DOM_SystemUserAddress KioskAddress;
        private WebSocket SunmiWebSocket;

        public CheckoutViewModel(IEventAggregator eventAggregator, IRegionManager regionManager,
            IModuleManager moduleManager, IK9CommandService k9CommandService, IProductService productService, IKioskConnectorService kioskConnectorService, ISummiSocketService sunmiService)
        {
            _moduleManager = moduleManager;
            _regionManager = regionManager;
            _eventAggregator = eventAggregator;
            _k9CommandService = k9CommandService;
            _productService = productService;
            _sunmiService = sunmiService;
            _kioskConnectorService = kioskConnectorService; 
            _MakePaymentCommand = new DelegateCommand(MakePayment, canMakePayment);
            KeyUpEventCommand = new DelegateCommand<KeyEventArgs>(KeyUpEventHandler);
            //clsSerial = new K9SerialPortService();
            _eventAggregator.GetEvent<CheckOutEvent>().Subscribe(PopulateShoppingCart);
            _loadProductDetailCommand = new DelegateCommand<ItemDetails>(loadProductDetail);
            Country = "Malaysia";

            _eventAggregator.GetEvent<ClearAddressEvent>().Subscribe(ClearAddress);
            //Oncheckout need to get Address from kiosk Info
            var KioskInfo = _kioskConnectorService.GetKioskDetail(FingerPrint.Value());
            var ValidState = _kioskConnectorService.GetStates();

            StateList = ValidState.Select(x => x.Name).ToList();

            if(KioskInfo != null)
            {
                if(KioskInfo.Kiosk != null && KioskInfo.MerchantContactDetails != null )
                {
                    KioskAddress = KioskInfo.MerchantContactDetails;
                    MerchantAddress = KioskAddress.Address1.Trim() + " " +
                     KioskAddress.Address2.Trim() + " " +
                     KioskAddress.Address3.Trim() + " " +
                     KioskAddress.ZipCode.Trim() + " " +
                     KioskAddress.City.Trim() + " " +
                     KioskAddress.State.Trim() + " " +
                     KioskAddress.Country.Trim();
                    MerchantContactInfo = KioskAddress.FirstName.Trim() + " " + KioskAddress.LastName.Trim();
                    MerchantContactInfo += string.IsNullOrEmpty(KioskAddress.CS_Contact) ? "" : "/" + KioskAddress.CS_Contact.Trim();
                    MerchantContactInfo += string.IsNullOrEmpty(KioskAddress.CS_Email) ? "" : "/" + KioskAddress.CS_Email.Trim();
                }
            }
        }

        private void PopulateShoppingCart(List<ShoppingCartEntryViewModel> obj)
        {
            //Product.MemberPrice = "MYR " + Product.Items.First().ProductModel.Amounts.Where(w => w.Type == "MEMBER")-- >
            ShoppingEntries = new ObservableCollection<ShoppingCartEntryViewModel>(obj);
            ReCalculateTotal(ShoppingEntries);
        }

        public ICommand LoadProductDetailCommand
        {
            get
            {
                return _loadProductDetailCommand;
            }
        }

        private void loadProductDetail(ItemDetails itemDetails)
        {
            //_eventAggregator.GetEvent<ProductDetailShowEvent>().Publish(itemDetails);

            _moduleManager.LoadModule(ModuleNames.ProductDetailsModule);
            var parameters = new NavigationParameters();
            parameters.Add("ItemDetails", itemDetails);

            if (itemDetails != null)
                _regionManager.RequestNavigate(RegionNames.TransactionPrimaryRegion, "ProductDetailsView", parameters);
        }

        //private K9SerialPortService _clsSerial;

        //private K9SerialPortService clsSerial
        //{
        //    [MethodImpl(MethodImplOptions.Synchronized)]
        //    get
        //    {
        //        return _clsSerial;
        //    }

        //    [MethodImpl(MethodImplOptions.Synchronized)]
        //    set
        //    {
        //        if (_clsSerial != null)
        //        {
        //            _clsSerial.Receive -= clsSerial_Receive;
        //        }

        //        _clsSerial = value;
        //        if (_clsSerial != null)
        //        {
        //            _clsSerial.Receive += clsSerial_Receive;
        //        }
        //    }
        //}

        public ShoppingCartCostsSummary Summary { get; set; }

        public ObservableCollection<ShoppingCartEntryViewModel> ShoppingEntries
        {
            get
            {
                return _ShoppingEntries;
            }
            set
            {
                SetProperty(ref _ShoppingEntries, value);
            }
        }

        private void ReCalculateTotal(ObservableCollection<ShoppingCartEntryViewModel> entries)
        {
            Summary = new ShoppingCartCostsSummary();
            foreach (ShoppingCartEntryViewModel Item in entries)
            {
                decimal MemberAmount = Item.Price;
                Item.PriceString = "RM " + Item.Price.ToString("N2");
                Item.TotalPriceString = "RM " + (Item.Price * Item.Quantity).ToString("N2");

                decimal Amount = MemberAmount;
                decimal ItemSubTotal = Item.Quantity * Amount;
                decimal ItemTaxTotal = ItemSubTotal * Item.ItemTaxPercent / 100;
                decimal ItemShippingTotal = Item.ShippingCost;
                decimal ItemShippingTax = ItemShippingTotal * (Item.ShoppingTaxPercentage / 100);

                Summary.ItemsSubtotal += ItemSubTotal;
                Summary.ItemsTax += ItemTaxTotal;
                Summary.Shipping += ItemShippingTotal;
                Summary.ShippingTax += ItemShippingTax;
                Summary.Total += ItemSubTotal + ItemShippingTotal;
                Summary.TotalTax += ItemShippingTax + ItemTaxTotal;
            }

            GrandTotal = "RM " + (Summary.Total + Summary.TotalTax).ToString("N2");
            SubTotal = "RM " + Summary.ItemsSubtotal.ToString("N2");
            ShippingAndTax = "RM " + (Summary.Shipping + Summary.ShippingTax + Summary.TotalTax).ToString("N2");
        }

        public void KeyUpEventHandler(KeyEventArgs args)
        {
            _MakePaymentCommand.RaiseCanExecuteChanged();
        }

        public string MerchantContactInfo
        {
            get { return _MerchantContactInfo; }

            set
            {
                SetProperty(ref _MerchantContactInfo, value);
                RaisePropertyChanged("MerchantContactInfo");
            }
        }
        public string MerchantAddress
        {
            get { return _MerchantAddress; }

            set
            {
                SetProperty(ref _MerchantAddress, value);
                RaisePropertyChanged("MerchantAddress");
            }
        }

        public string ShippingAndTax
        {
            get { return _ShippingAndTax; }

            set
            {
                SetProperty(ref _ShippingAndTax, value);
            }
        }

        public string SubTotal
        {
            get { return _SubTotal; }

            set
            {
                SetProperty(ref _SubTotal, value);
            }
        }

        public string GrandTotal
        {
            get { return _GrandTotal; }

            set
            {
                SetProperty(ref _GrandTotal, value);
            }
        }

        private string _MobileNo;

        public string MobileNo
        {
            get { return _MobileNo; }

            set
            {
                SetProperty(ref _MobileNo, value);
                RaisePropertyChanged("MobileNo");
                _MakePaymentCommand.RaiseCanExecuteChanged();
            }
        }

        private string _Name;

        public string Name
        {
            get { return _Name; }

            set
            {
                SetProperty(ref _Name, value);
                RaisePropertyChanged("Name");
                _MakePaymentCommand.RaiseCanExecuteChanged();
            }
        }

        private string _FirstName;

        public string FirstName
        {
            get { return _FirstName; }

            set
            {
                SetProperty(ref _FirstName, value);
            }
        }

        private string _LastName;

        public string LastName
        {
            get { return _LastName; }

            set
            {
                SetProperty(ref _LastName, value);
            }
        }

        private string _AddressOne;

        public string AddressOne
        {
            get { return _AddressOne; }

            set
            {
                SetProperty(ref _AddressOne, value);
                RaisePropertyChanged("AddressOne");
                _MakePaymentCommand.RaiseCanExecuteChanged();
            }
        }

        private string _AddressTwo;

        public string AddressTwo
        {
            get { return _AddressTwo; }

            set
            {
                SetProperty(ref _AddressTwo, value);
                RaisePropertyChanged("AddressTwo");
                _MakePaymentCommand.RaiseCanExecuteChanged();
            }
        }

        private string _AddressThree;

        public string AddressThree
        {
            get { return _AddressThree; }

            set
            {
                SetProperty(ref _AddressThree, value);
                RaisePropertyChanged("AddressThree");
                _MakePaymentCommand.RaiseCanExecuteChanged();
            }
        }

        private string _City;

        public string City
        {
            get { return _City; }

            set
            {
                SetProperty(ref _City, value);
                RaisePropertyChanged("City");
                _MakePaymentCommand.RaiseCanExecuteChanged();
            }
        }

        private string _PostCode;

        public string PostCode
        {
            get { return _PostCode; }

            set
            {
                SetProperty(ref _PostCode, value);
                RaisePropertyChanged("PostCode");
                _MakePaymentCommand.RaiseCanExecuteChanged();
            }
        }


        private List<string> _StateList;

        public List<string> StateList
        {
            get { return _StateList; }

            set
            {
                SetProperty(ref _StateList, value);
                RaisePropertyChanged("StateList");
            }
        }

        private string _State;

        public string State
        {
            get { return _State; }

            set
            {
                SetProperty(ref _State, value);
                RaisePropertyChanged("State");
                _MakePaymentCommand.RaiseCanExecuteChanged();
            }
        }

        private string _Country;

        public string Country
        {
            get { return _Country; }

            set
            {
                SetProperty(ref _Country, value);
                RaisePropertyChanged("Country");
                _MakePaymentCommand.RaiseCanExecuteChanged();
            }
        }

        private string _Email;

        public string Email
        {
            get { return _Email; }

            set
            {
                SetProperty(ref _Email, value);
                RaisePropertyChanged("Email");
                _MakePaymentCommand.RaiseCanExecuteChanged();
            }
        }

        private DelegateCommand _MakePaymentCommand;

        public ICommand MakePaymentCommand
        {
            get
            {
                return _MakePaymentCommand;
            }
        }

        public void ClearAddress()
        {
            MobileNo = "";
            Name = "";
            FirstName = "";
            LastName = "";
            AddressOne = "";
            AddressTwo = "";
            AddressThree = "";
            City = "";
            PostCode = "";
            State = "";
            //Country = "";
            Email = "";
        }
        public void MakePayment()
        {
            ToggleViewModel ps = new ToggleViewModel()
            {
                IsShow = true,
                Header = "Payment",
                Description = "Proceed to Make Payment",
            };
            _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(ps);

            if (!string.IsNullOrEmpty(CoreSettingHelper.GetTerminalUrl()))
            {
                var TerminalUrl = CoreSettingHelper.GetTerminalUrl();

                string KioskAppId = FingerPrint.Value();
                string OrderId = Guid.NewGuid().ToString();


                if (!Directory.Exists(SunmiPaymentLogDirectory))
                {
                    Directory.CreateDirectory(SunmiPaymentLogDirectory);
                }
                FullPath = Path.Combine(SunmiPaymentLogDirectory, OrderId +"_"+ CoreSettingHelper.GetTimestamp(DateTime.Now) + ".txt");
                //if (File.Exists(Path.Combine(SunmiPaymentLogDirectory, OrderId +DateTime.Now.ToString("yyy")+ "txt")))
                //{
                //    File.Delete(fileName);
                //}

                // Create a new file     
                using (FileStream fs = File.Create(FullPath))
                {
                    // Add some text to file    
                    byte[] title = new UTF8Encoding(true).GetBytes("Payment_" + OrderId);
                    fs.Write(title, 0, title.Length);
                    byte[] newline = Encoding.ASCII.GetBytes(Environment.NewLine);
                    fs.Write(newline, 0, newline.Length);
                    byte[] author = new UTF8Encoding(true).GetBytes(FingerPrint.Value());
                    fs.Write(author, 0, author.Length);
                    fs.Write(newline, 0, newline.Length);
                    byte[] commonByte = new UTF8Encoding(true).GetBytes(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                    fs.Write(commonByte, 0, commonByte.Length);
                    fs.Write(newline, 0, newline.Length);

                }
                // Open the stream and read it back.                   

                var Total = Summary.Total + Summary.TotalTax;
                int TotalInCents = (int)(Total * 100);
                var strAmount = Total.ToString("N2").Replace(".", "").Replace(",", "");
                SunmiP1SalePreAuthRequestViewModel newRequest = new SunmiP1SalePreAuthRequestViewModel()
                {
                    amount = TotalInCents,
                    appId = KioskAppId,
                    appType = SunmiAppType.CardPayment,
                    orderId = OrderId,
                    transType = SunmiTransType.Sale
                };

                //SunmiP1RefundRequestViewModel newRefundRequest = new SunmiP1RefundRequestViewModel()
                //{
                //    amount = 100,
                //    appId = "com.flash.test",
                //    appType = "00",
                //    oriOrderId = "PTR-000000001",
                //    orderId = "RRR-000000001",
                //    transType = SunmiTransType.Refund
                //};
                string RequestString = JsonConvert.SerializeObject(newRequest);
                var HexString = _sunmiService.CreateSunmiHexString(RequestString);
                
                File.AppendAllText(FullPath, RequestString);
                File.AppendAllText(FullPath, HexString);

       
                SunmiWebSocket = new WebSocket(TerminalUrl);
                SunmiWebSocket.OnMessage += Ws_OnMessage;
                SunmiWebSocket.OnError += (sender, e) =>
                {
                    _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(new ToggleViewModel());

                    ErrorScreenToggle("Payment Terminal Is Not Connected", "Sorry for the inconvenience, please contact administrator.");
                };
                //debug send order straight
                ToggleViewModel ConnectingVm = new ToggleViewModel()
                {
                    IsShow = true,
                    Header = "Payment",
                    Description = "Connecting To Payment Terminal",
                };

                _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(ConnectingVm);
                OuterCancellationToken = new CancellationTokenSource();
                CancellationToken cs = OuterCancellationToken.Token;

                millisecondsDelay = 60000;
                Task.Run(async () =>
                {
                    await Task.Delay(millisecondsDelay);

                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        if (!cs.IsCancellationRequested)
                        {
                            _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(new ToggleViewModel());
                        }
                        //show warning if can
                    });
                }, cs);

                SunmiWebSocket.Connect();
                try
                {
                    if (SunmiWebSocket.IsAlive)
                    {
                        ToggleViewModel vm = new ToggleViewModel()
                        {
                            IsShow = true,
                            Header = "Payment",
                            Description = "Awaiting Payment",
                        };

                        _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(vm);
                        SunmiWebSocket.Send(HexString);
                    }
                    else
                    {

                        OuterCancellationToken.Cancel();
                        _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(new ToggleViewModel());
                        ErrorScreenToggle("Payment Terminal Is Not Connected", "Sorry for the inconvenience, please contact administrator.");

                        //OuterCancellationToken = new CancellationTokenSource();
                        //CancellationToken cs = OuterCancellationToken.Token;

                        //millisecondsDelay = 60000;
                        //Task.Run(async () =>
                        //{
                        //    await Task.Delay(millisecondsDelay);

                        //    Application.Current.Dispatcher.Invoke(() =>
                        //    {
                        //        if (!cs.IsCancellationRequested)
                        //        {
                        //            _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(new ToggleViewModel());
                        //        }
                        //        //show warning if can
                        //    });
                        //}, cs);
                    }
                }
                catch (Exception ex)
                {
                    OuterCancellationToken.Cancel();
                    _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(new ToggleViewModel());
                    ErrorScreenToggle("Payment Terminal Is Not Connected", "Sorry for the inconvenience, please contact administrator.");

                }

                //debug send order straight
                //var ResponseModel = new SunmiP1ResponseViewModel()
                //{
                //    orderId = OrderId,
                //    referenceNum = "RandomNoForTesting"
                //};
                //SendOrderToSAP(ResponseModel);
            }
        
            //auto close after 1 min if no payment is done.
        }

        private void ErrorScreenToggle(string Header,string Description)
        {
            _eventAggregator.GetEvent<ToggleErrorScreenEvent>().Publish(new ToggleViewModel
            {
                IsShow = true,
                Description = Description, //"Sorry for the inconvenience, please contact administrator.",
                Header = Header// "Payment Terminal Is Not Connected"
            });
            int millisecondsDelay = 8000;
            Task.Run(async () =>
            {
                await Task.Delay(millisecondsDelay);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    _eventAggregator.GetEvent<ToggleErrorScreenEvent>().Publish(new ToggleViewModel());
                });
            });
        }

        private bool RequestSend()
        {
            ToggleViewModel sc = new ToggleViewModel()
            {
                IsShow = true,
                Header = "Processing!",
                Description = "Awaiting Bank Acknowledgement!",
            };
            _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(sc);

            OuterCancellationToken.Cancel();

            int millisecondsDelay = 60000;
            InnerCancellationToken = new CancellationTokenSource();
            CancellationToken cs = InnerCancellationToken.Token;

            Task.Run(async () =>
            {
                await Task.Delay(millisecondsDelay);

                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (!cs.IsCancellationRequested)
                    {
                        _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(new ToggleViewModel());
                    }
                                    //show warning if can
                                });
            }, cs);
            return true;

        }
        public void Ws_OnMessage(object sender, MessageEventArgs e)
        {
            var ResponseModel = _sunmiService.ProcessSunmiResponse(e.Data);
            //received = Messages.Trim();
            if (KioskMode)
            {
                string ErrorHeader = "";
                string ErrorMessages = "";
                switch (ResponseModel.resultCode)
                {
                    case "T00":
                        OuterCancellationToken.Cancel();
                        ToggleViewModel vm = new ToggleViewModel()
                        {
                            IsShow = true,
                            Header = "Payment",
                            Description = "Payment Received! Processing Order!",
                        };
                        _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(vm);

                        OuterCancellationToken = new CancellationTokenSource();
                        CancellationToken cs = OuterCancellationToken.Token;

                        millisecondsDelay = 60000;
                        Task.Run(async () =>
                        {
                            await Task.Delay(millisecondsDelay);

                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                if (!cs.IsCancellationRequested)
                                {
                                    _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(new ToggleViewModel());
                                }
                                //show warning if can
                            });
                        }, cs);

                        //transaction Successful
                        SendOrderToSAP(ResponseModel);
                        break;
                    case "D03":
                        ErrorHeader = "The original transaction has been void";
                        //The original transaction has been void
                        break;
                    case "D11":
                        ErrorHeader = "Duplicate Order No";
                        //Duplicate Order No
                        break;
                    case "E19":
                        ErrorHeader = "The original transaction not found";
                        //The original transaction not found
                        break;
                    case "400":
                        ErrorHeader = "Unknown";
                        //Unkown
                        break;
                    case "401":
                        ErrorHeader = "Transaction Failed";
                        //Transaction Failed
                        break;
                    case "402":
                        ErrorHeader = "Invalid App Type";
                        //Invalid App Type
                        break;
                    case "405":
                        ErrorHeader = "invalid original reference number";
                        //invalid original reference number    
                        break;
                    case "406":
                        ErrorHeader = "invalid original voucher number";
                        //invalid original voucher number
                        break;
                    case "407":
                        ErrorHeader = "invalid trans type";
                        //invalid trans type
                        break;
                    case "409":
                        ErrorHeader = "invalid amount";
                        //invalid amount
                        break;
                    case "412":
                        ErrorHeader = "Logon Failed";
                        //Logon Failed
                        break;
                    case "413":
                        ErrorHeader = "Transaction Time Out";
                        //Transaction Time Out
                        break;
                    case "414":
                        ErrorHeader = "SDK initialized Failed";
                        //SDK initialized Failed
                        break;
                    case "416":
                        ErrorHeader = "invalid original voucher number";
                        //invalid original voucher number
                        break;
                    case "417":
                        ErrorHeader = "Invalid Order No";
                        //invalid Order No
                        break;
                    case "418":
                        ErrorHeader = "Transaction Failed";
                        //Transaction Cancel
                        break;
                    case "421":
                        ErrorHeader = "logoff failed";
                        //logoff failed
                        break;
                    case "425":
                        ErrorHeader = "Transaction No Limit";
                        //Transaction No Limit
                        break;
                    case "426":
                        ErrorHeader = "transaction function close";
                        //transaction function close
                        break;
                    case "427":
                        ErrorHeader = "Transaction Repeated call";
                        //Transaction Repeated call
                        break;

                }

                File.AppendAllText(FullPath, e.Data);
                File.AppendAllText(FullPath, ResponseModel.resultCode);
                File.AppendAllText(FullPath, ResponseModel.resultMsg);

                ErrorMessages = ResponseModel.resultMsg;
                
                if (ResponseModel.resultCode != "T00")
                {
                    _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(new ToggleViewModel());
                    OuterCancellationToken.Cancel();
                    ErrorScreenToggle(ErrorHeader, ErrorMessages);
                }
                //RESPCODE = _k9CommandService.RespGetCard("22", Messages.Trim(), ref SALE_TID, ref SALE_DT, ref MASK_PAN, ref HASH_PAN, ref RRN, ref STAN, ref APPR_CODE, ref ADD_DATA);
                //success
                //TerminalTransaction SuccessTransaction = new TerminalTransaction();

                //SuccessTransaction = StoreOutPut();
                SunmiWebSocket.Close();
            }
        }

        private void SendOrderToSAP(SunmiP1ResponseViewModel ResponseModel)
        {
            OrderHeaderModel OrderHeader = new OrderHeaderModel()
            {
                PaymentTransactionId = ResponseModel.orderId,
                CustomerFirstName = Name,
                CustomerLastName = "",
                CustomerEmail = Email,
                CustomerContact = MobileNo.ToPhoneNumberFormat(false),
                CustomerBillingFirstName = Name,
                CustomerBillingLastName = "",
                CustomerBillingAddress1 = AddressOne,
                CustomerBillingAddress2 = AddressTwo,
                CustomerBillingAddress3 = AddressThree,
                CustomerBillingCity = City,
                CustomerBillingZipCode = PostCode,
                CustomerBillingState = State,
                CustomerBillingCountry = Country,
                CustomerDeliveryFirstName = Name,
                CustomerDeliveryLastName = "",
                CustomerDeliveryAddress1 = AddressOne,
                CustomerDeliveryAddress2 = AddressTwo,
                CustomerDeliveryAddress3 = AddressThree,
                CustomerDeliveryCity = City,
                CustomerDeliveryZipCode = PostCode,
                CustomerDeliveryState = State,
                CustomerDeliveryCountry = Country,
                CustomerDeliveryContact = MobileNo.ToPhoneNumberFormat(false),
                PaymentTerminalReferenceId = ResponseModel.referenceNum,
                //GrandTotal = GrandTotal,
                Source = "KIOSK",
                KioskMachineId = FingerPrint.Value()
            };
            //Model
            foreach (var Product in ShoppingEntries.Select((value, i) => new { i, value }))
            {
                Item OrderDetail = new Item()
                {
                    Sku = Product.value.ProductModel.Sku,
                    DiscountAmount = 0,
                    ShippingAmount = 0,
                    FinalAmount = Product.value.Price * Product.value.Quantity,
                    ShippingVAT = 0,
                    CampaignDiscount = 0,
                    Position = Product.i+1,
                    Quantity = Product.value.Quantity,
                    UnitOfMeasurement = Product.value.ProductModel.UnitOfMeasurement,
                };
                OrderHeader.Items.Add(OrderDetail);
            }

            OrderHeader.GrandTotal = OrderHeader.Items.Sum(x => x.FinalAmount);
            OrderHeader.SubTotal = OrderHeader.Items.Sum(x => x.FinalAmount);
            Task.Run(async () =>
            {
                var result = await _productService.SubmitOrder(OrderHeader);
                
                _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(new ToggleViewModel());
                OuterCancellationToken.Cancel();
                //InnerCancellationToken.Cancel();
                if (result.IsSuccess)
                {
                    OrderSummaryViewModel SuccessModel = new OrderSummaryViewModel()
                    {
                        MerchantAddress1 = KioskAddress.Address1,
                        MerchantAddress2 = KioskAddress.Address2,
                        MerchantAddress3 = KioskAddress.Address3,
                        MerchantContact  = KioskAddress.Contact,
                        MerchantPostCode = KioskAddress.ZipCode,
                        MerchantState = KioskAddress.State,
                        MerchantCountry = KioskAddress.Country,
                        MerchantName = KioskAddress.FirstName.Trim() + " " + KioskAddress.LastName.Trim(),
                        MerchantEmail = KioskAddress.CS_Email,

                        CustomerDeliveryAddress1 = OrderHeader.CustomerDeliveryAddress1,
                        CustomerDeliveryAddress2 = OrderHeader.CustomerDeliveryAddress2,
                        CustomerDeliveryAddress3 = OrderHeader.CustomerDeliveryAddress3,
                        CustomerDeliveryCity = OrderHeader.CustomerDeliveryCity,
                        CustomerDeliveryPostCode = OrderHeader.CustomerBillingZipCode,
                        CustomerEmail = OrderHeader.CustomerEmail,
                        CustomerDeliveryContact = OrderHeader.CustomerDeliveryContact,
                        CustomerDeliveryCountry = OrderHeader.CustomerDeliveryCountry,
                        CustomerDeliveryFirstName = OrderHeader.CustomerDeliveryFirstName,
                        CustomerDeliveryLastName = OrderHeader.CustomerDeliveryLastName,
                        CustomerDeliveryState = OrderHeader.CustomerDeliveryState,
                        TotalAmount = "RM " + OrderHeader.GrandTotal.ToString("N2"),
                        CustomerDeliveryZipCode = OrderHeader.CustomerDeliveryZipCode,
                        OrderDate = OrderHeader.KioskOrderDate.ToString("dd/MM/yyyy"),
                        OrderNo = result.ResultObject.Id,
                        PaymentReferenceNo = OrderHeader.PaymentTerminalReferenceId
                    };

                    _eventAggregator.GetEvent<PaymentSuccessfulScreen>().Publish(SuccessModel);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        _moduleManager.LoadModule(ModuleNames.OrderSuccessModule);
                        //ShowSuccessOrderScreenScreen();
                        //_eventAggregator.GetEvent<ShowSuccessOrderScreenEvent>().Publish();
                        var parameters = new NavigationParameters();
                        parameters.Add("Entry", SuccessModel);
                        _regionManager.RequestNavigate(RegionNames.SuccessOrderRegion, "OrderSuccessView", parameters);
                    });

                    //removeFromShoppingCart(obj);

                    //int millisecondsDelay = 10000;
                    ClearAddress();
                    _eventAggregator.GetEvent<ClearCartEvent>().Publish();
                }
                else
                {
                    ErrorScreenToggle("Transaction Failed", "Order failed to submit to server. Please try again later.");
                }
            });
        }

        public bool canMakePayment()
        {
            if (string.IsNullOrWhiteSpace(Name)
                || string.IsNullOrWhiteSpace(AddressOne)
                || string.IsNullOrWhiteSpace(AddressTwo)
                || string.IsNullOrWhiteSpace(PostCode)
                || string.IsNullOrWhiteSpace(MobileNo)
                || string.IsNullOrWhiteSpace(Country)
                || string.IsNullOrWhiteSpace(State)
                || string.IsNullOrWhiteSpace(Email)
                )
            {
                return false;
            }
            return true;
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            var Carts = navigationContext.Parameters["Cart"] as List<ShoppingCartEntryViewModel>;
            if (Carts != null)
                //List<ShoppingCartEntryViewModel> obj
                PopulateShoppingCart(Carts);
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            var itemDetails = navigationContext.Parameters["Cart"] as List<ShoppingCartEntryViewModel>;
            if (itemDetails != null)
                return true;
            else
                return false;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            ///throw new NotImplementedException();
        }

        #region  Gobiz Make Payment
        //public void MakePayment()
        //{
        //    ToggleViewModel ps = new ToggleViewModel()
        //    {
        //        IsShow = true,
        //        Header = "Payment",
        //        Description = "Proceed to Make Payment",
        //    };
        //    _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(ps);

        //    if (ConnectToAvailablePorts())
        //    {
        //        InitializeSales();

        //        OuterCancellationToken = new CancellationTokenSource();
        //        CancellationToken cs = OuterCancellationToken.Token;

        //        int millisecondsDelay = 60000;
        //        Task.Run(async () =>
        //        {
        //            await Task.Delay(millisecondsDelay);

        //            Application.Current.Dispatcher.Invoke(() =>
        //            {
        //                if (!cs.IsCancellationRequested)
        //                {
        //                    _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(new ToggleViewModel());
        //                }
        //                //show warning if can
        //            });
        //        }, cs);

        //        ToggleViewModel vm = new ToggleViewModel()
        //        {
        //            IsShow = true,
        //            Header = "Payment",
        //            Description = "Awaiting Payment",
        //        };

        //        _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(vm);
        //    }
        //    else
        //    {
        //        _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(new ToggleViewModel());

        //        _eventAggregator.GetEvent<ToggleErrorScreenEvent>().Publish(new ToggleViewModel
        //        {
        //            IsShow = true,
        //            Description = "Sorry for the inconvenience, please contact administrator.",
        //            Header = "Payment Terminal Is Not Connected"
        //        });
        //        int millisecondsDelay = 8000;
        //        Task.Run(async () =>
        //        {
        //            await Task.Delay(millisecondsDelay);
        //            Application.Current.Dispatcher.Invoke(() =>
        //            {
        //                _eventAggregator.GetEvent<ToggleErrorScreenEvent>().Publish(new ToggleViewModel());
        //            });
        //        });
        //    }

        //    //auto close after 1 min if no payment is done.
        //}
        //public delegate void SetTextCallback(string strMessage);

        //private string SendData;

        //public bool ConnectToAvailablePorts()
        //{
        //    if (clsSerial.IsOpen)
        //    {
        //        return true;
        //    }
        //    try
        //    {
        //        string result = "";
        //        clsSerial.BaudRate = 115200;
        //        clsSerial.DataBits = 8;
        //        clsSerial.Parity = (int)System.IO.Ports.Parity.None;
        //        clsSerial.StopBits = 1;
        //        if (_clsSerial.GetPorts().Count > 0)
        //        {
        //            foreach (var Port in _clsSerial.GetPorts())
        //            {
        //                clsSerial.PortNumber = int.Parse(Port.Substring(3));
        //                if (clsSerial.Connect(ref result))
        //                {
        //                    //ConnectionTest();
        //                    if (SendData == "")
        //                    {
        //                        break;
        //                    }
        //                    //if (KioskMode == true)
        //                    //{
        //                    //    try
        //                    //    {
        //                    //        if (this.BackgroundWorker1.IsBusy != true)
        //                    //        {
        //                    //            this.BackgroundWorker1.RunWorkerAsync();
        //                    //        }
        //                    //    }
        //                    //    catch (Exception ex)
        //                    //    {
        //                    //    }
        //                    //}
        //                    break;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            return false;
        //        }

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine(ex.Message);
        //        return false;
        //        //this.lblPortStat.Text = ex.Message;
        //    }
        //}

        //public void DisconnectPort()
        //{
        //    clsSerial.Disconnect();
        //}

        //public TerminalTransaction StoreOutPut()
        //{
        //    //var Output = "CMD=" + CmdState + " (InitSale)" + " " + "RESPCODE=" + RESPCODE + " " + "SALE_TID=" + SALE_TID + " " + "SALE_DT=" + SALE_DT + " " + "MASK_PAN=" + MASK_PAN + " " + "HASH_PAN=" + HASH_PAN + " " + "RRN=" + RRN + " " + "STAN=" + STAN + " " + "APPR_CODE=" + APPR_CODE + " " + "ADD_DATA=" + ADD_DATA;
        //    return new TerminalTransaction()
        //    {
        //        CommandState = CmdState,
        //        ResponseCode = RESPCODE,
        //        SaleTransactionid = SALE_TID,
        //        SalesTransactionDateString = SALE_DT,
        //        SalesTransactionDate = DateTime.ParseExact(SALE_DT, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture),//	  "yyyy-MM-dd HH:mm:ss"
        //        MaskPan = MASK_PAN,
        //        HashPan = HASH_PAN,
        //        RRN = RRN,
        //        STAN = STAN,
        //        AdditionalData = ADD_DATA,
        //        ApprovalCode = APPR_CODE,
        //    };
        //}

        //private void clsSerial_Receive(string Messages)
        //{
        //    Messages = Messages.Trim();
        //    //received = Messages.Trim();
        //    if (KioskMode)
        //    {
        //        if (CmdState == 21)
        //        {
        //            // If (CmdState = 21) And (_k9CommandService.RespGetStatus(msg.Trim) = "00") Then

        //            if (_k9CommandService.RespGetStatus(Messages.Trim()) == "00")
        //            {
        //                ToggleViewModel sc = new ToggleViewModel()
        //                {
        //                    IsShow = true,
        //                    Header = "Processing!",
        //                    Description = "Awaiting Bank Acknowledgement!",
        //                };
        //                _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(sc);

        //                OuterCancellationToken.Cancel();

        //                int millisecondsDelay = 60000;
        //                InnerCancellationToken = new CancellationTokenSource();
        //                CancellationToken cs = InnerCancellationToken.Token;

        //                Task.Run(async () =>
        //                {
        //                    await Task.Delay(millisecondsDelay);

        //                    Application.Current.Dispatcher.Invoke(() =>
        //                    {
        //                        if (!cs.IsCancellationRequested)
        //                        {
        //                            _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(new ToggleViewModel());
        //                        }
        //                        //show warning if can
        //                    });
        //                }, cs);

        //                RESPCODE = _k9CommandService.RespGetCard("21", Messages.Trim(), ref SALE_TID, ref SALE_DT, ref MASK_PAN, ref HASH_PAN, ref RRN, ref STAN, ref APPR_CODE, ref ADD_DATA);
        //                StoreOutPut();
        //                //Output = "CMD=21 (InitSale)" + Constants.vbCrLf + "RESPCODE=" + RESPCODE + Constants.vbCrLf + "SALE_TID=" + SALE_TID + Constants.vbCrLf + "SALE_DT=" + SALE_DT + Constants.vbCrLf + "MASK_PAN=" + MASK_PAN + Constants.vbCrLf + "HASH_PAN=" + HASH_PAN + Constants.vbCrLf + "RRN=" + RRN + Constants.vbCrLf + "STAN=" + STAN + Constants.vbCrLf + "APPR_CODE=" + APPR_CODE + Constants.vbCrLf + "ADD_DATA=" + ADD_DATA + Constants.vbCrLf;
        //                //SetCardText(Output);
        //                if (clsSerial.IsOpen)
        //                {
        //                    CmdState = 22;
        //                    string Com = _k9CommandService.SendCmd("22", "00");
        //                    clsSerial.Send(Com);
        //                }
        //            }
        //            else
        //            {
        //                _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(new ToggleViewModel());
        //                RESPCODE = _k9CommandService.RespGetStatus(Messages.Trim());
        //                OuterCancellationToken.Cancel();
        //            }
        //        }
        //        else if (CmdState == 22)
        //        {
        //            if (_k9CommandService.RespGetStatus(Messages.Trim()) == "00")
        //            {
        //                RESPCODE = _k9CommandService.RespGetCard("22", Messages.Trim(), ref SALE_TID, ref SALE_DT, ref MASK_PAN, ref HASH_PAN, ref RRN, ref STAN, ref APPR_CODE, ref ADD_DATA);
        //                //success
        //                TerminalTransaction SuccessTransaction = new TerminalTransaction();

        //                SuccessTransaction = StoreOutPut();

        //                OrderHeaderModel OrderHeader = new OrderHeaderModel()
        //                {
        //                    PaymentTransactionId = SALE_TID,
        //                    CustomerFirstName = Name,
        //                    CustomerLastName = "",
        //                    CustomerEmail = Email,
        //                    CustomerContact = MobileNo,
        //                    CustomerBillingFirstName = Name,
        //                    CustomerBillingLastName = "",
        //                    CustomerBillingAddress1 = AddressOne,
        //                    CustomerBillingAddress2 = AddressTwo,
        //                    CustomerBillingAddress3 = AddressThree,
        //                    CustomerBillingCity = City,
        //                    CustomerBillingZipCode = PostCode,
        //                    CustomerBillingState = State,
        //                    CustomerBillingCountry = Country,
        //                    CustomerDeliveryFirstName = Name,
        //                    CustomerDeliveryLastName = "",
        //                    CustomerDeliveryAddress1 = AddressOne,
        //                    CustomerDeliveryAddress2 = AddressTwo,
        //                    CustomerDeliveryAddress3 = AddressThree,
        //                    CustomerDeliveryCity = City,
        //                    CustomerDeliveryZipCode = PostCode,
        //                    CustomerDeliveryState = State,
        //                    CustomerDeliveryCountry = Country,
        //                    CustomerDeliveryContact = MobileNo,
        //                    //GrandTotal = GrandTotal,
        //                    Source = "KIOSK",
        //                };
        //                //Model
        //                foreach (var Product in ShoppingEntries.Select((value, i) => new { i, value }))
        //                {
        //                    Item OrderDetail = new Item()
        //                    {
        //                        Sku = Product.value.ProductModel.Sku,
        //                        DiscountAmount = 0,
        //                        ShippingAmount = 0,
        //                        FinalAmount = (double)(Product.value.Price * Product.value.Quantity),
        //                        ShippingVAT = 0,
        //                        CampaignDiscount = 0,
        //                        Position = Product.i,
        //                        Quantity = Product.value.Quantity,
        //                        UnitOfMeasurement = Product.value.ProductModel.UnitOfMeasurement,
        //                    };
        //                    OrderHeader.Items.Add(OrderDetail);
        //                }

        //                OrderHeader.GrandTotal = OrderHeader.Items.Sum(x => x.FinalAmount);
        //                Task.Run(async () =>
        //                {
        //                    var result = await _productService.SubmitOrder(OrderHeader);

        //                    _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(new ToggleViewModel());
        //                    InnerCancellationToken.Cancel();

        //                    OrderSummaryViewModel SuccessModel = new OrderSummaryViewModel()
        //                    {
        //                        CustomerDeliveryAddress1 = OrderHeader.CustomerDeliveryAddress1,
        //                        CustomerDeliveryAddress2 = OrderHeader.CustomerDeliveryAddress2,
        //                        CustomerDeliveryAddress3 = OrderHeader.CustomerDeliveryAddress3,
        //                        CustomerDeliveryCity = OrderHeader.CustomerDeliveryCity,
        //                        CustomerDeliveryPostCode = OrderHeader.CustomerBillingZipCode,
        //                        CustomerEmail = OrderHeader.CustomerEmail,
        //                        CustomerDeliveryContact = OrderHeader.CustomerDeliveryContact,
        //                        CustomerDeliveryCountry = OrderHeader.CustomerDeliveryCountry,
        //                        CustomerDeliveryFirstName = OrderHeader.CustomerDeliveryFirstName,
        //                        CustomerDeliveryLastName = OrderHeader.CustomerDeliveryLastName,
        //                        CustomerDeliveryState = OrderHeader.CustomerDeliveryState,
        //                        TotalAmount = OrderHeader.GrandTotal.ToString("N2"),
        //                        CustomerDeliveryZipCode = OrderHeader.CustomerDeliveryZipCode,
        //                        OrderDate = OrderHeader.KioskOrderDate.ToString("dd/MM/yyyy"),
        //                        OrderNo = result.OrderId,
        //                        PaymentReferenceNo = OrderHeader.PaymentTransactionId
        //                    };

        //                    _eventAggregator.GetEvent<PaymentSuccessfulScreen>().Publish(SuccessModel);
        //                    Application.Current.Dispatcher.Invoke(() =>
        //                    {
        //                        _moduleManager.LoadModule(ModuleNames.OrderSuccessModule);
        //                        //ShowSuccessOrderScreenScreen();
        //                        //_eventAggregator.GetEvent<ShowSuccessOrderScreenEvent>().Publish();
        //                        var parameters = new NavigationParameters();
        //                        parameters.Add("Entry", SuccessModel);
        //                        _regionManager.RequestNavigate(RegionNames.SuccessOrderRegion, "OrderSuccessView", parameters);
        //                    });

        //                    //removeFromShoppingCart(obj);

        //                    //int millisecondsDelay = 10000;
        //                    ClearAddress();
        //                    _eventAggregator.GetEvent<ClearCartEvent>().Publish();
        //                });

        //                // Send ACK
        //                if (clsSerial.IsOpen)
        //                {
        //                    CmdState = 89;
        //                    string Com = _k9CommandService.SendCmd("89", "22");

        //                    clsSerial.Send(Com);
        //                }
        //                //sales Successfull

        //                //ToggleViewModel sc = new ToggleViewModel()
        //                //{
        //                //    IsShow = true,
        //                //    Header = "Success!",
        //                //    Description = "An Receipt Has Been Emailed To You Email!",
        //                //};

        //                //Task.Run(async () =>
        //                //{
        //                //    await Task.Delay(millisecondsDelay);

        //                //    // it only works in WPF
        //                //    Application.Current.Dispatcher.Invoke(() =>
        //                //    {
        //                //        _eventAggregator.GetEvent<ResetViewListingEvent>().Publish();
        //                //        //_eventAggregator.GetEvent<PaymentSuccessfulScreen>().Publish(new ToggleViewModel());

        //                //        _regionManager.RequestNavigate(RegionNames.TransactionPrimaryRegion, "ProductListingView");
        //                //    });
        //                //});
        //            }
        //            else
        //            {
        //                _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(new ToggleViewModel());
        //                RESPCODE = _k9CommandService.RespGetStatus(Messages.Trim());
        //                OuterCancellationToken.Cancel();
        //            }

        //            CmdState = 0;
        //        }
        //    }
        //    else if (CmdState == 21)
        //    {
        //        if (_k9CommandService.RespGetStatus(Messages.Trim()) == "00")
        //        {
        //            RESPCODE = _k9CommandService.RespGetCard("21", Messages.Trim(), ref SALE_TID, ref SALE_DT, ref MASK_PAN, ref HASH_PAN, ref RRN, ref STAN, ref APPR_CODE, ref ADD_DATA);
        //            StoreOutPut();
        //        }
        //        else
        //        {
        //            RESPCODE = _k9CommandService.RespGetStatus(Messages.Trim());
        //            OuterCancellationToken.Cancel();
        //            StoreOutPut();
        //        }
        //    }
        //    // ElseIf (CmdState = 4) Then

        //    // If (_k9CommandService.RespGetStatus(msg.Trim) = "00") Then
        //    // RESPCODE = _k9CommandService.RespGetStatus(msg.Trim)
        //    // Output = "CMD=4C (Connection Test)" & vbCrLf & "RESPCODE=" & RESPCODE & _k9CommandService.RespGetConnectionTest(msg.Trim) & vbCrLf
        //    // SetCardText(Output)
        //    // End If
        //    else if (CmdState == 22)
        //    {
        //        if (_k9CommandService.RespGetStatus(Messages.Trim()) == "00")
        //        {
        //            RESPCODE = _k9CommandService.RespGetCard("22", Messages.Trim(), ref SALE_TID, ref SALE_DT, ref MASK_PAN, ref HASH_PAN, ref RRN, ref STAN, ref APPR_CODE, ref ADD_DATA);
        //            StoreOutPut();

        //            // Send ACK
        //            if (clsSerial.IsOpen)
        //            {
        //                CmdState = 89;
        //                string Com = _k9CommandService.SendCmd("89", "22");

        //                clsSerial.Send(Com);
        //            }
        //        }
        //        else
        //        {
        //            RESPCODE = _k9CommandService.RespGetStatus(Messages.Trim());
        //            OuterCancellationToken.Cancel();
        //            StoreOutPut();
        //        }

        //        // CmdState = 0
        //    }
        //    else
        //    {
        //        RESPCODE = _k9CommandService.RespGetStatus(Messages.Trim());
        //        OuterCancellationToken.Cancel();
        //        StoreOutPut();
        //    }
        //}

        //public void ExecuteCommandState(string Amount = "0")
        //{
        //    switch (CmdState)
        //    {
        //        case 1: // Enable
        //            {
        //                SendData = _k9CommandService.SendCmd("01", _k9CommandService.SetCurrentDate());
        //                break;
        //            }

        //        case 4: // Connection Test
        //            {
        //                SendData = _k9CommandService.SendCmd("4C", "02");
        //                break;
        //            }

        //        case 5: // Reboot Reader
        //            {
        //                SendData = _k9CommandService.SendCmd("4F", "");
        //                break;
        //            }

        //        case 8: // Settlement
        //            {
        //                SendData = _k9CommandService.SendCmd("08", _k9CommandService.GetToday());
        //                break;
        //            }

        //        case 21: // InitSale
        //            {
        //                // Mid = "01"
        //                // Multi MID
        //                // Dim Amount As String = TextBox2.Text.ToString.PadLeft(12, "0") & "DF310101DF3201" & Mid
        //                //02000804210000000035001A03
        //                var Total = Summary.Total + Summary.TotalTax;
        //                var strAmount = Total.ToString("N2").Replace(".", "").Replace(",", "").PadLeft(12, '0');
        //                //000000003500
        //                //000000003500
        //                SendData = _k9CommandService.SendCmd("21", strAmount);
        //                break;
        //            }

        //        case 22: // ProcSale
        //            {
        //                SendData = _k9CommandService.SendCmd("22", "00");
        //                break;
        //            }

        //        case 89: // ACK
        //            {
        //                SendData = _k9CommandService.SendCmd("89", "22");
        //                break;
        //            }

        //        default:
        //            {
        //                SendData = _k9CommandService.SendCmd("01", _k9CommandService.SetCurrentDate());
        //                break;
        //            }
        //    }

        //    if (clsSerial.IsOpen)
        //    {
        //        var random = clsSerial.Send(SendData);
        //    }
        //    else
        //    {
        //        //SetText("COM Port closed");
        //    }
        //}

        //public void EnableK9()
        //{
        //    // If clsSerial.IsOpen Then
        //    // CmdState = 1
        //    // Dim Com As String = _k9CommandService.SendCmd("01", _k9CommandService.SetCurrentDate)
        //    // SetText(Com)
        //    // clsSerial.Send(Com)
        //    // End If
        //    try
        //    {
        //        CmdState = 1;
        //        ExecuteCommandState();
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine(ex.Message);
        //    }
        //}

        //public void InitializeSales()
        //{
        //    CmdState = 21;
        //    try
        //    {
        //        ExecuteCommandState();
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine(ex.Message);
        //    }
        //}

        //public void ProceedSales()
        //{
        //    CmdState = 22;
        //    try
        //    {
        //        ExecuteCommandState();
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine(ex.Message);
        //    }
        //}

        //public void ConnectionTest()
        //{
        //    CmdState = 4; // 0x4C
        //    try
        //    {
        //        ExecuteCommandState();
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine(ex.Message);
        //    }
        //}

        //public void RebootK9()
        //{
        //    CmdState = 5; // 0x4F
        //    try
        //    {
        //        ExecuteCommandState();
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine(ex.Message);
        //    }
        //}

        //public void Settlement()
        //{
        //    CmdState = 8; // 0x08
        //    try
        //    {
        //        ExecuteCommandState();
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine(ex.Message);
        //    }
        //}
        #endregion
    }
}