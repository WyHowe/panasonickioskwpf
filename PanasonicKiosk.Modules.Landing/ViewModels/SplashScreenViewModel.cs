﻿using PanasonicKiosk.Core.Events;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System.Windows.Input;

namespace PanasonicKiosk.Modules.Landing.ViewModels
{
    public class SplashScreenViewModel : BindableBase
    {
        private IEventAggregator _eventAggregator;
        private DelegateCommand loadProductCommand;
        private string _LandingMediaSrc;

        public SplashScreenViewModel(IEventAggregator eventAggregator)
        {
            this._eventAggregator = eventAggregator;
            loadProductCommand = new DelegateCommand(loadProductListing, canLoadProductScreen);
            //Panasonic Kiosk 2
            //LandingMediaSrc = "pack://application:,,,/Assets/PanasonicAdvert/Panasonic-Kiosk-2-2.gif";
            LandingMediaSrc = "Assets/Videos/PanasonicKiosk2.mp4";
        }

        public ICommand LoadProductListingCommand
        {
            get
            {
                return loadProductCommand;
            }
        }

        private bool canLoadProductScreen()
        {
            return true;
        }

        private void loadProductListing()
        {
            _eventAggregator.GetEvent<HideSplashScreenEvent>().Publish();
        }

        public string LandingMediaSrc
        {
            get { return _LandingMediaSrc; }
            set { SetProperty(ref _LandingMediaSrc, value); }
        }
    }
}