﻿using PanasonicKiosk.Core.Events;
using PanasonicKiosk.Core.Models;
using PanasonicKiosk.Core.Service;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Input;
using System.Windows.Media;

namespace PanasonicKiosk.Modules.SidePanel.ViewModels
{
    public class SidePanelBarViewModel : BindableBase
    {
        private ISidePanelService _sidePanelService;
        private IEventAggregator _eventAggregator;
        private List<SidePanelItem> _SidePanels;
        private DelegateCommand<SidePanelItem> _SelectParentCategoryCommand;
        
        public SidePanelBarViewModel(ISidePanelService sidePanelService, IEventAggregator eventAggregator)
        {
            this._eventAggregator = eventAggregator;
            this._sidePanelService = sidePanelService;
            _SelectParentCategoryCommand = new DelegateCommand<SidePanelItem>(SelectSidePanel, canSelectSidePanel);
            _eventAggregator.GetEvent<UpdateSidePanelEvent>().Subscribe(ResetSidePanel);
            _eventAggregator.GetEvent<LoadSidePanelEvent>().Subscribe(SetSidePanel);
            _eventAggregator.GetEvent<ResetViewListingEvent>().Subscribe(ResetViewListing);
        }

        public ICommand SelectParentCategoryCommand
        {
            get
            {
                return _SelectParentCategoryCommand;
            }
        }

        public List<SidePanelItem> SidePanels
        {
            get
            {
                return _SidePanels;
            }
            set
            {
                SetProperty(ref _SidePanels, value);
            }
        }

        private async void SetSidePanel()
        {
            _SidePanels = await _sidePanelService.GetSidePanels();
            RaisePropertyChanged("SidePanels");
            if (_SidePanels.Count > 0)
            {
                SelectSidePanel(_SidePanels[0]);
            }
        }

        private void ResetViewListing()
        {
            if (SidePanels.Count > 0)
            {
                SelectSidePanel(SidePanels[0]);
            }
        }

        private void ResetSidePanel()
        {
            foreach (var Panel in SidePanels)
            {
                Panel.BackGroundColor = new SolidColorBrush(Colors.Transparent);
                Panel.FontColor = Brushes.Gray;
            }
        }

        private void SelectSidePanel(SidePanelItem SidePanel)
        {
            foreach (var Panel in SidePanels.Where(x => x != SidePanel).ToList())
            {
                Panel.BackGroundColor = new SolidColorBrush(Colors.Transparent);
                Panel.GradientBackGroundStart = Colors.Transparent.ToString();
                Panel.GradientBackGroundEnd = Colors.Transparent.ToString();
                Panel.FontColor = Brushes.Gray;
            }

            SidePanel.BackGroundColor = (SolidColorBrush)(new BrushConverter().ConvertFrom("#D3D3D3"));
            SidePanel.GradientBackGroundStart = "#1b3087";
            SidePanel.GradientBackGroundEnd = "#082166";
            SidePanel.FontColor = Brushes.White;
            ToggleViewModel ps = new ToggleViewModel()
            {
                IsShow = true,
                Header = "Loading",
                Description = SidePanel.NavigationName,
            };

            _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(ps);
            _eventAggregator.GetEvent<SidePanelBarEvents>().Publish(SidePanel);

            //_eventAggregator.GetEvent<ProductListingShowEvent>().Publish();
            _eventAggregator.GetEvent<ClearSearchBoxEvent>().Publish();
        }

        public bool canSelectSidePanel(SidePanelItem SidePanel)
        {
            return true;
        }
    }
}