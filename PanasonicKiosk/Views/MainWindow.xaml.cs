﻿using PanasonicKiosk.Dispatchers;
using System.Windows;

namespace PanasonicKiosk.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            Execute.InitializeWithDispatcher();
            InitializeComponent();
        }

        //private void InitializeSignalR(string SystemHId)
        //{
        //    try
        //    {
        //        var Url = Core.Helper.CoreSettingHelper.GetTargetUrl() + "KioskNotificationHub";
        //        Url = Core.Helper.CoreSettingHelper.GetTargetUrl();
        //        var hubConnection = new HubConnection(Url);

        //        var prestoHubProxy = hubConnection.CreateHubProxy("KioskNotificationHub");
        //        prestoHubProxy.On<string>("GetPinged", (message) =>
        //        {
        //            MessageBox.Show(message);
        //        });

        //        hubConnection.Start().Wait();
        //        prestoHubProxy.Invoke("Heartbeat").ContinueWith(task =>
        //        {
        //            if (task.IsFaulted)
        //            {
        //                //HubClientEvents.Log.Error("There was an error opening the connection:" + task.Exception.GetBaseException());
        //            }

        //        }).Wait();

        //    }
        //    catch (System.Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}