﻿using PanasonicKiosk.Core.Helper;
using PanasonicKiosk.Core.Models;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace PanasonicKiosk.Core.Service
{
    public class KioskConnectorService : IKioskConnectorService
    {
        private IAPIAccessService APIDataSource;
        private IEventAggregator eventAggregator;

        public KioskConnectorService(IAPIAccessService datasource, IEventAggregator eventAggregator)
        {
            this.APIDataSource = datasource;
            this.eventAggregator = eventAggregator;
        }
        public async Task<KioskDetailReponseModel> GetKioskDetailAsync(string KioskMachineId)
        {

            return await APIDataSource.GetKioskDetailsAsync(KioskMachineId);
        }
        public KioskDetailReponseModel GetKioskDetail(string KioskMachineId)
        {

            return APIDataSource.GetKioskDetails(KioskMachineId);
        }
        public List<KioskStateResponseModel> GetStates()
        {

            return APIDataSource.GetStates();
        }
    }
}