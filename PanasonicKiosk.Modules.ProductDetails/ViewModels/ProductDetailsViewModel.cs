﻿using PanasonicKiosk.Core.Events;
using PanasonicKiosk.Core.Models;
using PanasonicKiosk.Core.Service;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PanasonicKiosk.Modules.ProductDetails.ViewModels
{
    public class ProductDetailsViewModel : BindableBase, INavigationAware, IRegionMemberLifetime
    {
        private IProductService _productService;
        private IEventAggregator _eventAggregator;
        private ProductDetailsModel _Product;
        private ItemDetails _itemDetails;
        private string _DisplayImage;
        private DelegateCommand _AddToCartCommand;
        private DelegateCommand<string> _ShowSelectedImageCommand;

        public ProductDetailsViewModel(IProductService productService, IEventAggregator eventAggregator)
        {
            this._eventAggregator = eventAggregator;
            this._productService = productService;

            _AddToCartCommand = new DelegateCommand(addToCart, canAddToCart);
            _ShowSelectedImageCommand = new DelegateCommand<string>(addShowImage, canShowImage);
            //eventAggregator.GetEvent<ProductDetailPopulateEvent>().Subscribe(RefreshProductDetail);

            _DiscountImageUrl = "pack://application:,,,/Assets/sales_bookmark.png";
        }

        private string _DiscountImageUrl;

        public string DiscountImageUrl
        {
            get { return _DiscountImageUrl; }
            set
            {
                SetProperty(ref _DiscountImageUrl, value);
            }
        }

        private bool _OnSale;

        public bool OnSale
        {
            get { return _OnSale; }
            set
            {
                SetProperty(ref _OnSale, value);
            }
        }

        private string _BreadCrumb;

        public string BreadCrumb
        {
            get
            {
                return _BreadCrumb;
            }
            set
            {
                SetProperty(ref _BreadCrumb, value);
            }
        }

        private bool _ShowIsSoldOut;
        private bool _ShowAddToCart;

        public bool ShowIsSoldOut
        {
            get { return _ShowIsSoldOut; }
            set
            {
                SetProperty(ref _ShowIsSoldOut, value);
            }
        }

        public bool ShowAddToCart
        {
            get { return _ShowAddToCart; }
            set
            {
                SetProperty(ref _ShowAddToCart, value);
            }
        }

        public ICommand ShowSelectedImageCommand
        {
            get
            {
                return _ShowSelectedImageCommand;
            }
        }

        public ICommand AddToCartCommand
        {
            get
            {
                return _AddToCartCommand;
            }
        }

        private bool canShowImage(string ImageFileUrl)
        {
            return true;
        }

        private void addShowImage(string ImageFileUrl)
        {
            if (!string.IsNullOrEmpty(ImageFileUrl))
            {
                DisplayImage = Product.VarietyImages.Where(x => x == ImageFileUrl).FirstOrDefault();
                RaisePropertyChanged("DisplayImage");
            }
        }

        private bool canAddToCart()
        {
            return true;
        }

        private void addToCart()
        {
            ProductModelViewModel selectedItem = new ProductModelViewModel();
            // var Random = SelectedProductModel;
            if (Product.VariatyItems.Count > 0)
            {
                selectedItem = Product.VariatyItems.Where(x => x.Id == SelectedProductModel).FirstOrDefault();
                if (selectedItem == null)
                {
                    selectedItem = Product.VariatyItems[0];
                }
            }
            if (selectedItem.AvailableQuantity > 0)
            {
                ShoppingCartEntryViewModel sc = new ShoppingCartEntryViewModel();
                sc.ProductModel = selectedItem;
                sc.Product = ItemDetails;
                sc.Quantity = 1;

                if (selectedItem != null)
                {
                    if (selectedItem.SpecialAmount != 0 && selectedItem.Amount > selectedItem.SpecialAmount)
                    {
                        sc.Price = selectedItem.SpecialAmount;
                    }
                    else
                    {
                        sc.Price = selectedItem.Amount;
                    }
                    _eventAggregator.GetEvent<AddToCartEvent>().Publish(sc);
                }
            }
        }

        public ItemDetails ItemDetails
        {
            get
            {
                return _itemDetails;
            }
            set
            {
                SetProperty(ref _itemDetails, value);
            }
        }

        public ProductDetailsModel Product
        {
            get
            {
                return _Product;
            }
            set
            {
                SetProperty(ref _Product, value);
            }
        }

        public string DisplayImage
        {
            get
            {
                return _DisplayImage;
            }
            set
            {
                SetProperty(ref _DisplayImage, value);
            }
        }

        public bool KeepAlive
        {
            get
            {
                return false;
            }
        }

        public bool _IsSingleProductModel;

        public bool IsSingleProductModel
        {
            get
            {
                return _IsSingleProductModel;
            }
            set
            {
                SetProperty(ref _IsSingleProductModel, value);
            }
        }

        public bool _IsMultiProductModel;

        public bool IsMultiProductModel
        {
            get
            {
                return _IsMultiProductModel;
            }
            set
            {
                SetProperty(ref _IsMultiProductModel, value);
            }
        }

        private string _SelectedProductModel;

        public string SelectedProductModel
        {
            get
            {
                return _SelectedProductModel;
            }
            set
            {
                SetProperty(ref _SelectedProductModel, value);
                var SelectedProduct = ItemDetails.Items.Where(x => x.Id == _SelectedProductModel).FirstOrDefault();
                if (SelectedProduct != null)
                {
                    Product.VarietyImages = SelectedProduct?.Images;

                    Product.MemberPrice = "RM " + SelectedProduct.Amount.ToString("N2");
                    Product.SpecialPrice = "RM " + SelectedProduct.SpecialAmount.ToString("N2");
                    if (SelectedProduct.SpecialAmount != 0 && SelectedProduct.Amount > SelectedProduct.SpecialAmount)
                    {
                        Product.ShowDiscountBlock = true;
                        Product.ShowNonDiscountBlock = false;
                        int DiscountPercentage = Convert.ToInt32(100 - (SelectedProduct.Amount / SelectedProduct.SpecialAmount * 100));
                        Product.DiscountPercentage = "" + DiscountPercentage + "%";
                    }
                    else
                    {
                        Product.ShowDiscountBlock = false;
                        Product.ShowNonDiscountBlock = true;
                    }

                    if (Product.VarietyImages != null && Product.VarietyImages.Count > 0)
                    {
                        DisplayImage = Product.VarietyImages[0];
                    }

                    if (SelectedProduct.IsOutOfStock)
                    {
                        ShowAddToCart = false;
                        ShowIsSoldOut = true;
                    }
                    else
                    {
                        ShowAddToCart = true;
                        ShowIsSoldOut = false;
                    }
                }
            }
        }

        private string _SingleProductName;

        public string SingleProductName
        {
            get
            {
                return _SingleProductName;
            }
            set
            {
                SetProperty(ref _SingleProductName, value);
            }
        }

        private void RefreshProductDetail(ItemDetails obj)
        {
            var DefaultItem = obj.Items.Where(x => x.Images.Count> 0).FirstOrDefault();
            if(DefaultItem == null)
            {
                DefaultItem = obj.Items.First();
            }
            BreadCrumb = obj.ParentCategoryName + " \\ " + obj.CategoryName;
            ItemDetails = obj;
            Product = new ProductDetailsModel()
            {
                Id = obj.Id,
                CategoryId = obj.CategoryId,
                Name = obj.Name,
                DescriptionFileName = obj.DescriptionFileName,
                FeaturesFileName = obj.FeaturesFileName,
                SpecificationsFileName = obj.SpecificationImageFileName,
                OverviewFileName = obj.OverviewFileName,
                SpecificationImageFileName = obj.SpecificationImageFileName,
                VariatyItems = obj.Items,
                Attributes = obj.Attributes,
                ImageUrl = obj.ImageUrl,
                MemberPrice = obj.MemberPrice,
                SpecialPrice = obj.SpecialPrice,
                ShowDiscountBlock = obj.ShowDiscountBlock,
                ShowNonDiscountBlock = obj.ShowNonDiscountBlock,
                DiscountPercentage = obj.DiscountPercentage,
                VarietyImages = DefaultItem.Images.ToList()
            };
            if (DefaultItem.SpecialAmount != 0 && DefaultItem.Amount >= DefaultItem.SpecialAmount)
            {
                Product.SpecialPrice = "RM " + DefaultItem.SpecialAmount.ToString("N2");
            }
            OnSale = obj.OnSale;
            ShowAddToCart = obj.ShowAddToCart;
            ShowIsSoldOut = obj.ShowIsSoldOut;
            IsSingleProductModel = false;
            IsMultiProductModel = true;

            SelectedProductModel = DefaultItem.Id;

            if (Product.VarietyImages.Count > 0)
            {
                DisplayImage = Product.VarietyImages[0];
            }
            else
            {
                DisplayImage = "pack://application:,,,/Assets/no_image_available.png";
            }

            if (Product.VariatyItems.Count == 1)
            {
                IsSingleProductModel = true;
                IsMultiProductModel = false;
                SingleProductName = Product.VariatyItems[0].Name + " - " + Product.VariatyItems[0].Sku;
                SelectedProductModel = Product.VariatyItems[0].Id;
            }
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            var itemDetails = navigationContext.Parameters["ItemDetails"] as ItemDetails;
            if (itemDetails != null)
                RefreshProductDetail(itemDetails);
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            var itemDetails = navigationContext.Parameters["ItemDetails"] as ItemDetails;
            if (itemDetails != null)
                return true;
            else
                return false;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
        }
    }
}