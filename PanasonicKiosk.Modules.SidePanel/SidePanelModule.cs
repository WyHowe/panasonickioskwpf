﻿using PanasonicKiosk.Core;
using PanasonicKiosk.Modules.SidePanel.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace PanasonicKiosk.Modules.SidePanel
{
    public class SidePanelModule : IModule
    {
        private readonly IRegionManager _regionManager;

        public SidePanelModule(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }

        public void OnInitialized(IContainerProvider containerProvider)
        {
            _regionManager.RegisterViewWithRegion(RegionNames.SidePanelRegion, typeof(SidePanelBar));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
        }
    }
}