﻿using PanasonicKiosk.Core;
using PanasonicKiosk.Modules.ProductDetails.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace PanasonicKiosk.Modules.ProductDetails
{
    public class ProductDetailsModule : IModule
    {
        private readonly IRegionManager _regionManager;

        public ProductDetailsModule(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }

        public void OnInitialized(IContainerProvider containerProvider)
        {
            _regionManager.RegisterViewWithRegion(RegionNames.TransactionPrimaryRegion, typeof(ProductDetailsView));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<ProductDetailsView>();
        }
    }
}