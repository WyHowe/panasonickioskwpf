﻿using PanasonicKiosk.Core;
using PanasonicKiosk.Modules.Landing.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace PanasonicKiosk.Modules.Landing
{
    public class LandingModule : IModule
    {
        private readonly IRegionManager _regionManager;

        public LandingModule(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }

        public void OnInitialized(IContainerProvider containerProvider)
        {
            _regionManager.RegisterViewWithRegion(RegionNames.SplashRegion, typeof(SplashScreenView));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
        }
    }
}