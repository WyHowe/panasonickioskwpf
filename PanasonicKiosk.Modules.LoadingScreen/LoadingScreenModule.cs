﻿using PanasonicKiosk.Core;
using PanasonicKiosk.Modules.LoadingScreen.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace PanasonicKiosk.Modules.LoadingScreen
{
    public class LoadingScreenModule : IModule
    {
        private readonly IRegionManager _regionManager;

        public LoadingScreenModule(IRegionManager regionManager)
        {
            _regionManager = regionManager;
        }

        public void OnInitialized(IContainerProvider containerProvider)
        {
            _regionManager.RegisterViewWithRegion(RegionNames.TransactionPrimaryRegion, typeof(LoadingView));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<LoadingView>();
        }
    }
}