﻿using PanasonicKiosk.Core.Models;
using Prism.Events;
using System;
using System.Collections.Generic;

namespace PanasonicKiosk.Core.Events
{
    public class ToggleLoadingScreenEvent : PubSubEvent<ToggleViewModel>
    { }

    public class OrderCompleteEvent : PubSubEvent
    { }

    public class ToggleErrorScreenEvent : PubSubEvent<ToggleViewModel>
    { }

    public class PaymentSuccessfulScreen : PubSubEvent<OrderSummaryViewModel>
    { }

    public class HideSplashScreenEvent : PubSubEvent
    { }

    public class ShowSplashScreenEvent : PubSubEvent
    { }

    public class HideClearConfirmationScreenEvent : PubSubEvent
    { }

    public class ShowClearConfirmationScreenEvent : PubSubEvent
    { }

    public class HideSuccessOrderScreenEvent : PubSubEvent
    { }

    public class ShowSuccessOrderScreenEvent : PubSubEvent
    { }

    public class RemoveItemFromCartEvent : PubSubEvent<ShoppingCartEntryViewModel>
    { }

    public class KeepItemFromCartEvent : PubSubEvent<ShoppingCartEntryViewModel>
    { }

    public class HideConfirmationScreenEvent : PubSubEvent
    { }

    public class ShowConfirmationScreenEvent : PubSubEvent
    { }

    public class ProductListingShowEvent : PubSubEvent
    { }

    public class ProductDetailShowEvent : PubSubEvent<ItemDetails>
    { }

    public class ShoppingCartShowEvent : PubSubEvent<ShoppingCartModel>
    { }

    public class CheckOutEvent : PubSubEvent<List<ShoppingCartEntryViewModel>>
    { }

    public class ProductDetailPopulateEvent : PubSubEvent<ItemDetails>
    { }

    public class AddToCartEvent : PubSubEvent<ShoppingCartEntryViewModel>
    { }

    public class UpdateCartDetailsEvent : PubSubEvent<ShoppingCartEntryViewModel>
    { }

    public class ClearCartCommandEvent : PubSubEvent
    { }

    public class SearchItemEvent : PubSubEvent<string>
    { }
    public class GetItemsByCampaignEvent : PubSubEvent<CampaignDetailViewModel>
    { }
    public class SubCategorySelectionEvent : PubSubEvent<SubCategorySearchModel>
    { }

    public class SubCategorySearchEvent : PubSubEvent<SubCategorySearchModel>
    { }

    public class UpdateSidePanelEvent : PubSubEvent
    { }

    public class LoadSidePanelEvent : PubSubEvent
    { }

    public class ClearSearchBoxEvent : PubSubEvent
    { }

    public class ClearCartEvent : PubSubEvent
    { }

    public class ClearAddressEvent: PubSubEvent
    {

    }
    public class ResetViewListingEvent : PubSubEvent
    { }

    public class SidePanelBarEvents : PubSubEvent<SidePanelItem>
    { }

    public class LastListingRequestEvent : PubSubEvent<Guid>
    { }
}

//public class SubCategorySelectionEvent: PubSubEvent<string> { }