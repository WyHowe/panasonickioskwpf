﻿using System;
using System.Runtime.CompilerServices;

namespace PanasonicKiosk.Core.K9Access
{
    public class K9ConnectorService : IK9ConnectorService
    {
        private IK9CommandService _k9CommandService;

        public K9ConnectorService(IK9CommandService k9CommandService)
        {
            _k9CommandService = k9CommandService;
            clsSerial = new K9SerialPortService();
        }

        private K9SerialPortService _clsSerial;

        private K9SerialPortService clsSerial
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return _clsSerial;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (_clsSerial != null)
                {
                    _clsSerial.Receive -= clsSerial_Receive;
                }

                _clsSerial = value;
                if (_clsSerial != null)
                {
                    _clsSerial.Receive += clsSerial_Receive;
                }
            }
        }

        public delegate void SetTextCallback(string strMessage);

        private int CmdState = 0;
        private string RESPCODE = "";
        private string SALE_TID = "";
        private string SALE_DT = "";
        private string MASK_PAN = "";
        private string HASH_PAN = "";
        private string RRN = "";
        private string STAN = "";
        private string APPR_CODE = "";
        private string ADD_DATA = "";
        private string data = "";
        private string Output = "";
        private string received = "";
        private int ComNo = 0;
        private bool KioskMode = false;
        private string txtFile = "";
        private string Mid = "01";

        private string SendData;

        public void GetPortSellection()
        {
            var PortsAvailable = _clsSerial.GetPorts();
        }

        public void ConnectToAvailablePorts()
        {
            if (clsSerial.IsOpen)
            {
                return;
            }
            try
            {
                string result = "";
                clsSerial.BaudRate = 115200;
                clsSerial.DataBits = 8;
                clsSerial.Parity = (int)System.IO.Ports.Parity.None;
                clsSerial.StopBits = 1;

                foreach (var Port in _clsSerial.GetPorts())
                {
                    clsSerial.PortNumber = int.Parse(Port.Substring(3));
                    if (clsSerial.Connect(ref result))
                    {
                        //ConnectionTest();
                        if (SendData == "")
                        {
                            break;
                        }
                        //if (KioskMode == true)
                        //{
                        //    try
                        //    {
                        //        if (this.BackgroundWorker1.IsBusy != true)
                        //        {
                        //            this.BackgroundWorker1.RunWorkerAsync();
                        //        }
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //    }
                        //}
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                //this.lblPortStat.Text = ex.Message;
            }
        }

        public void DisconnectPort()
        {
            clsSerial.Disconnect();
        }

        private void clsSerial_Receive(string msg)
        {
            msg = msg.Trim();
            received = msg.Trim();
            if (KioskMode)
            {
                if (CmdState == 21)
                {
                    // If (CmdState = 21) And (_k9CommandService.RespGetStatus(msg.Trim) = "00") Then

                    if (_k9CommandService.RespGetStatus(msg.Trim()) == "00")
                    {
                        RESPCODE = _k9CommandService.RespGetCard("21", msg.Trim(), ref SALE_TID, ref SALE_DT, ref MASK_PAN, ref HASH_PAN, ref RRN, ref STAN, ref APPR_CODE, ref ADD_DATA);
                        //Output = "CMD=21 (InitSale)" + Constants.vbCrLf + "RESPCODE=" + RESPCODE + Constants.vbCrLf + "SALE_TID=" + SALE_TID + Constants.vbCrLf + "SALE_DT=" + SALE_DT + Constants.vbCrLf + "MASK_PAN=" + MASK_PAN + Constants.vbCrLf + "HASH_PAN=" + HASH_PAN + Constants.vbCrLf + "RRN=" + RRN + Constants.vbCrLf + "STAN=" + STAN + Constants.vbCrLf + "APPR_CODE=" + APPR_CODE + Constants.vbCrLf + "ADD_DATA=" + ADD_DATA + Constants.vbCrLf;
                        //SetCardText(Output);
                        if (clsSerial.IsOpen)
                        {
                            CmdState = 22;
                            string Com = _k9CommandService.SendCmd("22", "00");
                            clsSerial.Send(Com);
                        }
                    }
                    else
                    {
                        RESPCODE = _k9CommandService.RespGetStatus(msg.Trim());
                    }
                }
                else if (CmdState == 22)
                {
                    if (_k9CommandService.RespGetStatus(msg.Trim()) == "00")
                    {
                        RESPCODE = _k9CommandService.RespGetCard("22", msg.Trim(), ref SALE_TID, ref SALE_DT, ref MASK_PAN, ref HASH_PAN, ref RRN, ref STAN, ref APPR_CODE, ref ADD_DATA);

                        // Send ACK
                        if (clsSerial.IsOpen)
                        {
                            CmdState = 89;
                            string Com = _k9CommandService.SendCmd("89", "22");

                            clsSerial.Send(Com);
                        }
                    }
                    else
                    {
                        RESPCODE = _k9CommandService.RespGetStatus(msg.Trim());
                    }

                    CmdState = 0;
                }
            }
            else if (CmdState == 21)
            {
                if (_k9CommandService.RespGetStatus(msg.Trim()) == "00")
                {
                    RESPCODE = _k9CommandService.RespGetCard("21", msg.Trim(), ref SALE_TID, ref SALE_DT, ref MASK_PAN, ref HASH_PAN, ref RRN, ref STAN, ref APPR_CODE, ref ADD_DATA);
                }
                else
                {
                    RESPCODE = _k9CommandService.RespGetStatus(msg.Trim());
                }
            }
            // ElseIf (CmdState = 4) Then

            // If (_k9CommandService.RespGetStatus(msg.Trim) = "00") Then
            // RESPCODE = _k9CommandService.RespGetStatus(msg.Trim)
            // Output = "CMD=4C (Connection Test)" & vbCrLf & "RESPCODE=" & RESPCODE & _k9CommandService.RespGetConnectionTest(msg.Trim) & vbCrLf
            // SetCardText(Output)
            // End If
            else if (CmdState == 22)
            {
                if (_k9CommandService.RespGetStatus(msg.Trim()) == "00")
                {
                    RESPCODE = _k9CommandService.RespGetCard("22", msg.Trim(), ref SALE_TID, ref SALE_DT, ref MASK_PAN, ref HASH_PAN, ref RRN, ref STAN, ref APPR_CODE, ref ADD_DATA);

                    // Send ACK
                    if (clsSerial.IsOpen)
                    {
                        CmdState = 89;
                        string Com = _k9CommandService.SendCmd("89", "22");

                        clsSerial.Send(Com);
                    }
                }
                else
                {
                    RESPCODE = _k9CommandService.RespGetStatus(msg.Trim());
                }

                // CmdState = 0
            }
        }

        public void RequestPayment()
        {
            string Amount = "199999";
            //this.BackgroundWorker1.WorkerSupportsCancellation = true;
            //if (this.BackgroundWorker1.CancellationPending)
            //{
            //    e.Cancel = true;
            //}

            try
            {
                if (KioskMode)
                {
                    if (clsSerial.IsOpen)
                    {
                        CmdState = 1;
                        string Com = _k9CommandService.SendCmd("01", _k9CommandService.SetCurrentDate());

                        clsSerial.Send(Com);
                        System.Threading.Thread.Sleep(1000);
                        if (CmdState == 1 & _k9CommandService.RespGetStatus(received) == "00")
                        {
                            CmdState = 21;
                            // Multi MID
                            // Dim Amount As String = TextBox2.Text.ToString.PadLeft(12, "0") & "DF310101DF320102"

                            string HexCommand = _k9CommandService.SendCmd("21", Amount);
                            clsSerial.Send(HexCommand);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //log.WriteToLogFile(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " - " + ex.Message + "," + ex.InnerException.Message, Application.StartupPath + @"\log\" + txtFile);
            }
        }

        public void ExecuteCommandState(string Amount = "0")
        {
            switch (CmdState)
            {
                case 1: // Enable
                    {
                        SendData = _k9CommandService.SendCmd("01", _k9CommandService.SetCurrentDate());
                        break;
                    }

                case 4: // Connection Test
                    {
                        SendData = _k9CommandService.SendCmd("4C", "02");
                        break;
                    }

                case 5: // Reboot Reader
                    {
                        SendData = _k9CommandService.SendCmd("4F", "");
                        break;
                    }

                case 8: // Settlement
                    {
                        SendData = _k9CommandService.SendCmd("08", _k9CommandService.GetToday());
                        break;
                    }

                case 21: // InitSale
                    {
                        // Mid = "01"
                        // Multi MID
                        // Dim Amount As String = TextBox2.Text.ToString.PadLeft(12, "0") & "DF310101DF3201" & Mid
                        //02000804210000000035001A03
                        Amount = "3500";
                        var strAmount = Amount.PadLeft(12, '0');
                        //000000003500
                        //000000003500
                        SendData = _k9CommandService.SendCmd("21", strAmount);
                        break;
                    }

                case 22: // ProcSale
                    {
                        SendData = _k9CommandService.SendCmd("22", "00");
                        break;
                    }

                case 89: // ACK
                    {
                        SendData = _k9CommandService.SendCmd("89", "22");
                        break;
                    }

                default:
                    {
                        SendData = _k9CommandService.SendCmd("01", _k9CommandService.SetCurrentDate());
                        break;
                    }
            }

            if (clsSerial.IsOpen)
            {
                var random = clsSerial.Send(SendData);
            }
            else
            {
                //SetText("COM Port closed");
            }
        }

        public void EnableK9()
        {
            // If clsSerial.IsOpen Then
            // CmdState = 1
            // Dim Com As String = _k9CommandService.SendCmd("01", _k9CommandService.SetCurrentDate)
            // SetText(Com)
            // clsSerial.Send(Com)
            // End If
            try
            {
                CmdState = 1;
                ExecuteCommandState();
            }
            catch (Exception ex)
            {
            }
        }

        public void InitializeSales()
        {
            CmdState = 21;
            try
            {
                ExecuteCommandState();
            }
            catch (Exception ex)
            {
            }
        }

        public void ProceedSales()
        {
            CmdState = 22;
            try
            {
                ExecuteCommandState();
            }
            catch (Exception ex)
            {
            }
        }

        public void ConnectionTest()
        {
            CmdState = 4; // 0x4C
            try
            {
                ExecuteCommandState();
            }
            catch (Exception ex)
            {
            }
        }

        public void RebootK9()
        {
            CmdState = 5; // 0x4F
            try
            {
                ExecuteCommandState();
            }
            catch (Exception ex)
            {
            }
        }

        public void Settlement()
        {
            CmdState = 8; // 0x08
            try
            {
                ExecuteCommandState();
            }
            catch (Exception ex)
            {
            }
        }
    }
}