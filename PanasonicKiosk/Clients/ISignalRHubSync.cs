﻿using PanasonicKiosk.Core.Models.SignalRModels;
using System;

namespace PanasonicKiosk.Clients
{
    public interface ISignalRHubSync
    {
        event Action<bool> ConnectionEvent;

        event Action<MyMessage> ReceiveMessageEvent;

        event Action<string> ReceivePingEvent;

        void Connect();

        void Disconnect();

        //void AddMessage(MyMessage message);

        void Heartbeat();

        //void Heartbeat2();
        void SendHelloObject(MyMessage message);
    }
}