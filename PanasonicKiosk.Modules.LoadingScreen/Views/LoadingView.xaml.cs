﻿using System.Windows.Controls;

namespace PanasonicKiosk.Modules.LoadingScreen.Views
{
    /// <summary>
    /// Interaction logic for LoadingView.xaml
    /// </summary>
    public partial class LoadingView : UserControl
    {
        public LoadingView()
        {
            InitializeComponent();
        }
    }
}