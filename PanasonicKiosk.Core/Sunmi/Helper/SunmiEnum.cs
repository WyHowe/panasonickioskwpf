﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanasonicKiosk.Core.Sunmi.Helper

{
    class SunmiEnums
    {
    }
    public static class SunmiTransType
    {
        public const string Sale = "00";
        public const string Refund = "09";
        public const string PreAuth = "03";
        public const string PreAuthComplete = "05";
        public const string Settlement = "07";
        public const string Print = "A1";
        public const string TransactionEnquiry = "A2";
    }
    public static class SunmiPlatformType
    {
        public const string BankCard = "card";
        public const string WeChatPay = "wxpay";
        public const string AliPay = "alipay";
        public const string UnionPay = "unionpay";
    }
    public static class SunmiAppType
    {
        public const string CardPayment = "00";
        public const string EWalletPayment = "01";
    }
    public static class SunmiCardType
    {
        public const string ChipContact = "IC";
        public const string WaveContactless = "NFG";
        public const string MagneticStripe = "MAG";
    }
    public static class SunmiAccountType
    {
        public const string ScanCode = "0A";
        public const string CreditCard = "CC";
        public const string DebitCard = "DC";
        public const string SemiCreditCard = "SCC";
        public const string ElectronicCash = "EC";
        public const string MagneticStripeCard = "MAG";
        public const string VisaCard = "VC";
        public const string MasterCard = "MC";
        public const string AmericanExpressCard = "AE";
        public const string JapanCard = "JBC";
        public const string IndiaCard = "RPC";
    }

}
