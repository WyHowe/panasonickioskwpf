﻿using System.Windows.Controls;

namespace PanasonicKiosk.Modules.OrderSuccess.Views
{
    /// <summary>
    /// Interaction logic for ViewA.xaml
    /// </summary>
    public partial class OrderSuccessView : UserControl
    {
        public OrderSuccessView()
        {
            InitializeComponent();
        }
    }
}