﻿using PanasonicKiosk.Core.Helper;
using System;
using System.Collections.Generic;

namespace PanasonicKiosk.Core.Models
{
    public class ToggleViewModel
    {
        public bool IsShow { get; set; }
        public string Header { get; set; }
        public string Description { get; set; }

        public ToggleViewModel()
        {
            IsShow = false;
        }
    }

    public class TerminalTransaction
    {
        public int CommandState { get; set; }
        public string ResponseCode { get; set; }
        public string SaleTransactionid { get; set; }
        public string SalesTransactionDateString { get; set; }
        public DateTime SalesTransactionDate { get; set; }
        public string MaskPan { get; set; }
        public string HashPan { get; set; }
        public string RRN { get; set; }
        public string STAN { get; set; }
        public string ApprovalCode { get; set; }
        public string AdditionalData { get; set; }
    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class Item
    {
        public string MarketplaceOrderItemId { get; set; }
        public string Sku { get; set; }
        public int Quantity { get; set; }
        public string UnitOfMeasurement { get; set; }
        public decimal FinalAmount { get; set; }
        public decimal DiscountAmount { get; set; }
        public int Position { get; set; }
        public decimal ShippingAmount { get; set; }
        public decimal ShippingVAT { get; set; }
        public decimal CampaignDiscount { get; set; }

        public Item()
        {
            MarketplaceOrderItemId = "";
        }
    }

    public class OrderSubmitionReturnModel
    {
        public string OrderId { get; set; }
        public string Result { get; set; }
        public OrderHeaderModel ResultObject { get; set; }
        public bool IsSuccess { get; set; }
    }

    public class OrderHeaderModel
    {
        public string Id { get; set; }
        public string Source { get; set; }
        public string PaymentTransactionId { get; set; }
        public string OrderStatus { get; set; }
        public List<Item> Items { get; set; }
        public string MarketplaceOrderId { get; set; }
        public string OrderDate { get; set; }
        public DateTime KioskOrderDate { get; set; }
        public string CustomerMembershipId { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerContact { get; set; }
        public string PaymentMethod { get; set; }
        public object CampaignCode { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal SubTotal { get; set; }
        public decimal ShippingTotal { get; set; }
        public decimal VATRate { get; set; }
        public decimal VATTotal { get; set; }
        public decimal NormalVAT { get; set; }
        public decimal ShippingVAT { get; set; }
        public decimal AdditionalInstallmentRate { get; set; }
        public decimal AdditionalInstallmentValue { get; set; }
        public string CustomerBillingFirstName { get; set; }
        public string CustomerBillingLastName { get; set; }
        public string CustomerBillingAddress1 { get; set; }
        public string CustomerBillingAddress2 { get; set; }
        public string CustomerBillingAddress3 { get; set; }
        public string CustomerBillingCity { get; set; }
        public string CustomerBillingState { get; set; }
        public string CustomerBillingZipCode { get; set; }
        public string CustomerBillingCountry { get; set; }
        public string CustomerDeliveryFirstName { get; set; }
        public string CustomerDeliveryLastName { get; set; }
        public string CustomerDeliveryAddress1 { get; set; }
        public string CustomerDeliveryAddress2 { get; set; }
        public string CustomerDeliveryAddress3 { get; set; }
        public string CustomerDeliveryCity { get; set; }
        public string CustomerDeliveryState { get; set; }
        public string CustomerDeliveryZipCode { get; set; }
        public string CustomerDeliveryCountry { get; set; }
        public string CustomerDeliveryContact { get; set; }
        public int CustomerTypeId { get; set; }

        public int OrderTypeId { get; set; }
        public string KioskMachineId { get; set; }
        public string PaymentTerminalReferenceId { get; set; }
        public OrderHeaderModel()
        {
            OrderStatus = "NEW";
            Items = new List<Item>();
            MarketplaceOrderId = "";
            Source = "KIOSK";
            PaymentMethod = "IPay88";
            CampaignCode = null;
            CustomerTypeId = 3; //DEALER
            OrderTypeId = 5; //DOM_DEALER
            AdditionalInstallmentRate = 0;
            AdditionalInstallmentValue = 0;
            KioskOrderDate = DateTime.Now;
            OrderDate = KioskOrderDate.ToString("yyyy-MM-dd HH:mm:ss");
            CustomerMembershipId = "";
            ShippingTotal = 0;
            VATRate = 0;
            VATTotal = 0;
            NormalVAT = 0;
            ShippingVAT = 0;
        }
    }

    public class CampaignDetailViewModel
    {
        public string CampaignId { get; set; }
        public string CampaignName { get; set; }
    }
    public class OrderSummaryViewModel
    {
        public string OrderNo { get; set; }
        public string OrderDate { get; set; }
        public string MerchantName { get; set; }
        public string MerchantAddress1 { get; set; }
        public string MerchantAddress2 { get; set; }
        public string MerchantAddress3 { get; set; }
        public string MerchantPostCode { get; set; }
        public string MerchantCountry { get; set; }
        public string MerchantState { get; set; }
        public string MerchantEmail { get; set; }
        public string MerchantContact { get; set; }

        public string CustomerDeliveryFirstName { get; set; }
        public string CustomerDeliveryLastName { get; set; }
        public string CustomerDeliveryAddress1 { get; set; }
        public string CustomerDeliveryAddress2 { get; set; }
        public string CustomerDeliveryAddress3 { get; set; }
        public string CustomerDeliveryCity { get; set; }
        public string CustomerDeliveryZipCode { get; set; }
        public string CustomerDeliveryState { get; set; }
        public string CustomerDeliveryCountry { get; set; }
        public string CustomerDeliveryContact { get; set; }
        public string CustomerDeliveryPostCode { get; set; }
        public string CustomerEmail { get; set; }
        public string TotalAmount { get; set; }
        public string PaymentReferenceNo { get; set; }
    }
}