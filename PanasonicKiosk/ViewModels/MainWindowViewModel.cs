﻿using PanasonicKiosk.Clients;
using PanasonicKiosk.Core;
using PanasonicKiosk.Core.Events;
using PanasonicKiosk.Core.Helper;
using PanasonicKiosk.Core.Models;
using PanasonicKiosk.Core.Models.SignalRModels;
using PanasonicKiosk.Core.Service;
using Prism.Events;
using Prism.Modularity;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Unity;

namespace PanasonicKiosk.ViewModels
{
    public class MainWindowViewModel : BindableBase, INavigationAware
    {
        private readonly DispatcherTimer _activityTimer;

        private IKioskConnectorService _kioskConnectorService;
        private readonly ISignalRHubSync _signalRHubSync;
        private readonly IContext _context;
        private IRegionManager _regionManager { get; set; }
        private IUnityContainer _unityContainer { get; set; }
        private IModuleManager _moduleManager { get; set; }
        private IEventAggregator _eventAggregator;
        private bool _LoadingScreenVisibility;
        private bool _SuccessScreenVisibility;
        private bool _ErrorScreenVisibility;
        public bool _SplashScreenVisibility;
        public bool _ConfirmationScreenVisibility;
        public bool _ClearConfirmationScreenVisibility;

        public bool _OrderSuccessfulVisibility;
        public bool _ProductScreenVisibility;
        public bool _ProductListingScreenVisibility;
        public bool _ProductDetailScreenVisibiliy;
        private string _LoaderGif;
        private string _SuccessImage;
        private string _ErrorImage;
        private bool _connectionActive;
        private string _title = "Panasonic Kiosk";
        private string _LoadingHeader;
        private string _LoadingDescriptions;
        private bool SplashScreenIsShow { get; set; }
        private bool ConfirmationScreenIsShow { get; set; }

        public MainWindowViewModel(IEventAggregator eventAggregator, IRegionManager regionManager, IKioskConnectorService kioskConnectorService,
            IModuleManager moduleManager, IUnityContainer container, ISignalRHubSync signalRHubSync, IContext context)
        {
            InputManager.Current.PreProcessInput += OnActivity;
            _activityTimer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(100), IsEnabled = true };
            _activityTimer.Tick += OnInactivity;
            _kioskConnectorService = kioskConnectorService;
            _signalRHubSync = signalRHubSync;
            _context = context;
            _regionManager = regionManager;
            _moduleManager = moduleManager;
            _unityContainer = container;
            SplashScreenVisibility = true;
            ConfirmationScreenVisibility = false;
            LoadingScreenVisibility = false;
            OrderSuccessfulVisibility = false;
            SuccessScreenVisibility = false;
            ErrorScreenVisibility = false;
            _eventAggregator = eventAggregator;

            eventAggregator.GetEvent<HideSplashScreenEvent>().Subscribe(HideSplashScreen);
            eventAggregator.GetEvent<ShowSplashScreenEvent>().Subscribe(ShowSplashScreen);

            eventAggregator.GetEvent<HideConfirmationScreenEvent>().Subscribe(HideConfirmationScreen);
            eventAggregator.GetEvent<ShowConfirmationScreenEvent>().Subscribe(ShowConfirmationScreen);

            eventAggregator.GetEvent<HideClearConfirmationScreenEvent>().Subscribe(HideClearConfirmationScreen);
            eventAggregator.GetEvent<ShowClearConfirmationScreenEvent>().Subscribe(ShowClearConfirmationScreen);

            eventAggregator.GetEvent<HideSuccessOrderScreenEvent>().Subscribe(HideSuccessOrderScreenScreen);
            eventAggregator.GetEvent<ShowSuccessOrderScreenEvent>().Subscribe(ShowSuccessOrderScreenScreen);

            eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Subscribe(ToggleLoaderScreen);
            eventAggregator.GetEvent<ToggleErrorScreenEvent>().Subscribe(ToggleErrorScreen);
            eventAggregator.GetEvent<ProductListingShowEvent>().Subscribe(ShowProductListingScreen);

            eventAggregator.GetEvent<PaymentSuccessfulScreen>().Subscribe(ToggleSuccessPayment);
            //eventAggregator.GetEvent<ProductDetailShowEvent>().Subscribe(ShowProductDetailScreen);

            _signalRHubSync.ConnectionEvent += _signalRHubSync_ConnectionEvent;
            _signalRHubSync.ReceiveMessageEvent += _signalRHubSync_ReceiveMessageEvent;
            _signalRHubSync.ReceivePingEvent += _signalRHubSync_ReceivePingEvent;

            LoaderGif = "pack://application:,,,/Assets/loader.gif";
            SuccessImage = "pack://application:,,,/Assets/approved.gif";
            ErrorImage = "pack://application:,,,/Assets/exclamation.gif";
            SplashScreenIsShow = true;
            ConfirmationScreenIsShow = false;

           _kioskConnectorService.GetKioskDetailAsync(FingerPrint.Value());
            
        }

        private void OnInactivity(object sender, EventArgs e)
        {
            if (!SplashScreenVisibility)
            {
                //SplashScreenIsShow = true;

                if (!ConfirmationScreenVisibility)
                {
                    // ConfirmationScreenIsShow = true;
                    TriggerConfirmationScreenAggregator();
                }
                // TriggerSplashScreenAggregator();
            }
            // remember mouse position
            //_inactiveMousePosition = Mouse.GetPosition(MainGrid);

            // set UI on inactivity
            // rectangle.Visibility = Visibility.Hidden;
        }

        private void OnActivity(object sender, PreProcessInputEventArgs e)
        {
            InputEventArgs inputEventArgs = e.StagingItem.Input;

            if (inputEventArgs is MouseEventArgs || inputEventArgs is KeyboardEventArgs)
            {
                SplashScreenIsShow = false;
                ConfirmationScreenIsShow = false;
                //if (e.StagingItem.Input is MouseEventArgs)
                //{
                //    MouseEventArgs mouseEventArgs = (MouseEventArgs)e.StagingItem.Input;

                //    // no button is pressed and the position is still the same as the application became inactive
                //    if (mouseEventArgs.LeftButton == MouseButtonState.Released &&
                //        mouseEventArgs.RightButton == MouseButtonState.Released &&
                //        mouseEventArgs.MiddleButton == MouseButtonState.Released &&
                //        mouseEventArgs.XButton1 == MouseButtonState.Released &&
                //        mouseEventArgs.XButton2 == MouseButtonState.Released)
                //       // _inactiveMousePosition == mouseEventArgs.GetPosition(MainGrid))
                //        return;
                //}

                // set UI on activity
                //rectangle.Visibility = Visibility.Visible;

                _activityTimer.Stop();
                _activityTimer.Start();
            }
        }

        private void _signalRHubSync_ReceivePingEvent(string obj)
        {
            _context.ReceivePingEvent(this, obj);
        }

        private void _signalRHubSync_ReceiveMessageEvent(MyMessage obj)
        {
            _context.ReceiveMessageEvent(this, obj);
        }

        private void _signalRHubSync_ConnectionEvent(bool obj)
        {
            _context.SendConnectionEvent(this, obj);
        }

        public bool ConnectionActive
        {
            get { return _connectionActive; }
            set
            {
                SetProperty(ref _connectionActive, value);
            }
        }

        public void Load()
        {
            //ConnectionActive
            if (!ConnectionActive)
            {
                _signalRHubSync.Connect();
            }

            Task.Run(async () =>
            {
                while (true)
                {
                    await Task.Delay(3000);
                    if (ConnectionActive)
                        _signalRHubSync.Heartbeat();
                    else
                        _signalRHubSync.Connect();

                    //_signalRHubSync.Heartbeat2();
                }
            });
        }

        public void alertsomething()
        {
            //ToggleSuccessPayment(new ToggleViewModel() { IsShow = true});
            //_signalRHubSync.Heartbeat();
            SplashScreenVisibility = true;
            RaisePropertyChanged("SplashScreenVisibility");
            ProductScreenVisibility = false;
            RaisePropertyChanged("ProductScreenVisibility");
        }

        public string LoaderGif
        {
            get { return _LoaderGif; }
            set { SetProperty(ref _LoaderGif, value); }
        }

        public string SuccessImage
        {
            get { return _SuccessImage; }
            set { SetProperty(ref _SuccessImage, value); }
        }

        public string ErrorImage
        {
            get { return _ErrorImage; }
            set { SetProperty(ref _ErrorImage, value); }
        }

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public string LoadingHeader
        {
            get { return _LoadingHeader; }
            set { SetProperty(ref _LoadingHeader, value); }
        }

        public string LoadingDescriptions
        {
            get { return _LoadingDescriptions; }
            set { SetProperty(ref _LoadingDescriptions, value); }
        }

        #region Visibility Attribute

        public bool LoadingScreenVisibility
        {
            get { return _LoadingScreenVisibility; }

            set
            {
                SetProperty(ref _LoadingScreenVisibility, value);
            }
        }

        public bool SuccessScreenVisibility
        {
            get { return _SuccessScreenVisibility; }

            set
            {
                SetProperty(ref _SuccessScreenVisibility, value);
            }
        }

        public bool ErrorScreenVisibility
        {
            get { return _ErrorScreenVisibility; }

            set
            {
                SetProperty(ref _ErrorScreenVisibility, value);
            }
        }

        public bool ConfirmationScreenVisibility
        {
            get { return _ConfirmationScreenVisibility; }

            set
            {
                SetProperty(ref _ConfirmationScreenVisibility, value);
            }
        }

        public bool ClearConfirmationScreenVisibility
        {
            get { return _ClearConfirmationScreenVisibility; }

            set
            {
                SetProperty(ref _ClearConfirmationScreenVisibility, value);
            }
        }

        public bool OrderSuccessfulVisibility
        {
            get { return _OrderSuccessfulVisibility; }

            set
            {
                SetProperty(ref _OrderSuccessfulVisibility, value);
            }
        }

        public bool SplashScreenVisibility
        {
            get { return _SplashScreenVisibility; }

            set
            {
                SetProperty(ref _SplashScreenVisibility, value);
            }
        }

        public bool ProductDetailScreenVisibility
        {
            get { return _ProductDetailScreenVisibiliy; }

            set
            {
                SetProperty(ref _ProductDetailScreenVisibiliy, value);
            }
        }

        public bool ProductListingScreenVisibility
        {
            get { return _ProductListingScreenVisibility; }

            set
            {
                SetProperty(ref _ProductListingScreenVisibility, value);
            }
        }

        public bool ProductScreenVisibility
        {
            get { return _ProductScreenVisibility; }

            set
            {
                SetProperty(ref _ProductScreenVisibility, value);
            }
        }

        #endregion Visibility Attribute

        public void ToggleLoaderScreen(ToggleViewModel fm)
        {
            LoadingScreenVisibility = fm.IsShow;
            LoadingDescriptions = fm.Description;
            LoadingHeader = fm.Header;
            RaisePropertyChanged("LoadingDescriptions");
            RaisePropertyChanged("LoadingHeader");
            RaisePropertyChanged("LoadingScreenVisibility");
        }

        public void ToggleSuccessPayment(OrderSummaryViewModel fm)
        {
            //SuccessScreenVisibility = fm.IsShow;
            //LoadingDescriptions = fm.Description;
            //LoadingHeader = fm.Header;
            //RaisePropertyChanged("LoadingDescriptions");
            //RaisePropertyChanged("LoadingHeader");
            //RaisePropertyChanged("SuccessScreenVisibility");

            OrderSuccessfulVisibility = true;
        }

        public void ToggleErrorScreen(ToggleViewModel fm)
        {
            ErrorScreenVisibility = fm.IsShow;
            LoadingDescriptions = fm.Description;
            LoadingHeader = fm.Header;
            RaisePropertyChanged("LoadingDescriptions");
            RaisePropertyChanged("LoadingHeader");
            RaisePropertyChanged("ErrorScreenVisibility");
        }

        public void HideClearConfirmationScreen()
        {
            ClearConfirmationScreenVisibility = false;
        }

        public void ShowClearConfirmationScreen()
        {
            ClearConfirmationScreenVisibility = true;

            RaisePropertyChanged("ClearConfirmationScreenVisibility");
        }

        public void HideSuccessOrderScreenScreen()
        {
            OrderSuccessfulVisibility = false;
        }

        public void ShowSuccessOrderScreenScreen()
        {
            OrderSuccessfulVisibility = true;
        }

        public void HideConfirmationScreen()
        {
            //_eventAggregator.GetEvent<LoadSidePanelEvent>().Publish();
            ConfirmationScreenVisibility = false;
            SplashScreenVisibility = false;
            ProductScreenVisibility = true;
            //loadProductListing();
        }

        public void ShowConfirmationScreen()
        {
            ConfirmationScreenVisibility = true;
            //ClearConfirmationScreenVisibility = false;
            ProductScreenVisibility = true;
            SplashScreenVisibility = false;

            RaisePropertyChanged("SplashScreenVisibility");
            RaisePropertyChanged("ProductScreenVisibility");
        }

        public void TriggerSplashScreenAggregator()
        {
            _eventAggregator.GetEvent<ShowSplashScreenEvent>().Publish();
        }

        public void TriggerConfirmationScreenAggregator()
        {
            _moduleManager.LoadModule(ModuleNames.IdleConfirmationModule);
            _eventAggregator.GetEvent<ShowConfirmationScreenEvent>().Publish();
            _regionManager.RequestNavigate(RegionNames.ConfirmationRegion, "IdleConfirmationView");
        }

        public void HideSplashScreen()
        {
            _eventAggregator.GetEvent<LoadSidePanelEvent>().Publish();
            SplashScreenVisibility = false;
            ProductScreenVisibility = true;
            loadProductListing();
        }

        public void ShowSplashScreen()
        {
            SplashScreenIsShow = true;
            SplashScreenVisibility = true;
            ConfirmationScreenVisibility = false;
            ClearConfirmationScreenVisibility = false;
            ProductScreenVisibility = false;
            RaisePropertyChanged("SplashScreenVisibility");
            RaisePropertyChanged("ProductScreenVisibility");
        }

        public void ShowContinueShopping()
        {
        }

        public void ShowProductListingScreen()
        {
            loadProductListing();
        }

        public void ShowProductDetailScreen(ItemDetails obj)
        {
            loadProductDetail(obj);
        }

        private void loadProductListing()
        {
            _moduleManager.LoadModule(ModuleNames.BannerModule);
            _moduleManager.LoadModule(ModuleNames.ProductListingModule);
            _moduleManager.LoadModule(ModuleNames.SidePanelModule);
            _moduleManager.LoadModule(ModuleNames.FooterModule);

            _regionManager.RequestNavigate(RegionNames.TransactionPrimaryRegion, "ProductListingView");
        }

        private void loadProductDetail(ItemDetails obj)
        {
            _moduleManager.LoadModule(ModuleNames.ProductDetailsModule);
            _eventAggregator.GetEvent<ProductDetailPopulateEvent>().Publish(obj);

            _regionManager.RequestNavigate(RegionNames.TransactionPrimaryRegion, "LoadingScreenView");
            _regionManager.RequestNavigate(RegionNames.TransactionPrimaryRegion, "ProductDetailsView");
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            //throw new System.NotImplementedException();
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            //throw new System.NotImplementedException();
        }
    }
}