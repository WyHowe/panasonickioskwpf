﻿namespace PanasonicKiosk.Core.Models.SignalRModels
{
    public class HelloModel
    {
        public string Molly { get; set; }
        public int Age { get; set; }
    }

    public class TerminalStatusModel
    {
        public string MachineId { get; set; }
        public bool IsWorking { get; set; }
        public string Description { get; set; }
        public string Remarks { get; set; }
    }
}