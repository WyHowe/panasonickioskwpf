﻿using PanasonicKiosk.Core;
using PanasonicKiosk.Core.Events;
using PanasonicKiosk.Core.Models;
using PanasonicKiosk.Core.Service;
using Prism.Commands;
using Prism.Events;
using Prism.Modularity;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace PanasonicKiosk.Modules.ProductListing.ViewModels
{
    public class ProductListingViewModel : BindableBase, INavigationAware
    {
        private bool _Loading;
        private bool _Adding;
        private string _GridTitle;

        //private string _SearchBox;
        private string _DiscountImageUrl;

        private Guid CurrentRequestId;
        private ObservableCollection<ItemDetails> _Product;
        private ObservableCollection<SubCategoryDetails> _SubCategories;
        private IProductService _productService;
        private IEventAggregator _eventAggregator;
        private IRegionManager _regionManager;
        private IModuleManager _moduleManager;

        private DelegateCommand<ItemDetails> _loadProductDetailCommand;
        private DelegateCommand<ItemDetails> _loadBuyNowCommand;
        private DelegateCommand<ItemDetails> _AddToCartCommand;

        public ProductListingViewModel(IRegionManager regionManager, IModuleManager moduleManager,
            IProductService productService, IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _moduleManager = moduleManager;
            _regionManager = regionManager;
            _loadProductDetailCommand = new DelegateCommand<ItemDetails>(loadProductDetail, canLoadProductDetail);
            _loadBuyNowCommand = new DelegateCommand<ItemDetails>(loadBuyNow, canloadBuyNow);
            _AddToCartCommand = new DelegateCommand<ItemDetails>(addToCart, canAddToCart);
            _productService = productService;
            Loading = true;
            _eventAggregator.GetEvent<ClearSearchBoxEvent>().Subscribe(ClearProductListing);
            _eventAggregator.GetEvent<LastListingRequestEvent>().Subscribe(UpdateLastRequestId);
            _DiscountImageUrl = "pack://application:,,,/Assets/sales_bookmark.png";
        }

        public bool Adding
        {
            get { return _Adding; }
            set
            {
                SetProperty(ref _Adding, value);
            }
        }

        public string DiscountImageUrl
        {
            get { return _DiscountImageUrl; }
            set
            {
                SetProperty(ref _DiscountImageUrl, value);
            }
        }
        private bool _Reset;
        public bool Reset
        {
            get { return _Reset; }
            set
            {
                SetProperty(ref _Reset, value);
                //_reset = value;
                //if (PropertyChanged != null)
                //    PropertyChanged(this, new PropertyChangedEventArgs("Reset"));
            }
        }
        public bool Loading
        {
            get { return _Loading; }
            set
            {
                SetProperty(ref _Loading, value);
            }
        }

        public string GridTitle
        {
            get { return _GridTitle; }
            set
            {
                SetProperty(ref _GridTitle, value);
            }
        }

        public ICommand LoadProductDetailCommand
        {
            get
            {
                return _loadProductDetailCommand;
            }
        }

        public ICommand LoadBuyNowCommand
        {
            get
            {
                return _loadBuyNowCommand;
            }
        }

        public ICommand AddToCartCommand
        {
            get
            {
                return _AddToCartCommand;
            }
        }

        public ObservableCollection<SubCategoryDetails> SubCategories
        {
            get
            {
                return _SubCategories;
            }
            set
            {
                SetProperty(ref _SubCategories, value);
            }
        }

        public ObservableCollection<ItemDetails> Product
        {
            get
            {
                return _Product;
            }
            set
            {
                SetProperty(ref _Product, value);
            }
        }

        private bool canAddToCart(ItemDetails itemDetails)
        {
            return true;
        }

        private void addToCart(ItemDetails itemDetails)
        {
            var Product = new ProductDetailsModel()
            {
                Id = itemDetails.Id,
                CategoryId = itemDetails.CategoryId,
                ParentCategoryName = itemDetails.ParentCategoryName,
                CategoryName = itemDetails.CategoryName,
                ParentCategoryId = itemDetails.ParentCategoryId,
                Name = itemDetails.Name,
                DescriptionFileName = itemDetails.DescriptionFileName,
                FeaturesFileName = itemDetails.FeaturesFileName,
                SpecificationsFileName = itemDetails.SpecificationImageFileName,
                OverviewFileName = itemDetails.OverviewFileName,
                SpecificationImageFileName = itemDetails.SpecificationImageFileName,
                VariatyItems = itemDetails.Items,
                Attributes = itemDetails.Attributes,
                ImageUrl = itemDetails.ImageUrl,
                MemberPrice = itemDetails.MemberPrice,
                VarietyImages = itemDetails.Items.Select(p => p.Images).SelectMany(m => m).ToList()
            };
            //check for varierty
            ProductModelViewModel selectedItem = new ProductModelViewModel();
            if (Product.VariatyItems.Count > 0)
            {
                selectedItem = Product.VariatyItems.Where(w => w.IsOutOfStock == false).ToList()[0];
            }

            ShoppingCartEntryViewModel sc = new ShoppingCartEntryViewModel();
            sc.Product = itemDetails; // send in whole data block
            sc.ProductModel = selectedItem;
            sc.Quantity = 1;
            //if (selectedItem.ProductModel != null)
            //{
            if (selectedItem.SpecialAmount != 0 && selectedItem.Amount > selectedItem.SpecialAmount)
            {
                sc.Price = selectedItem.SpecialAmount;
            }
            else
            {
                sc.Price = selectedItem.Amount;
            }

            _eventAggregator.GetEvent<AddToCartEvent>().Publish(sc);
            //}
        }

        private bool canloadBuyNow(ItemDetails itemDetails)
        {
            return true;
        }

        private void loadBuyNow(ItemDetails itemDetails)
        {
            var Product = new ProductDetailsModel()
            {
                Id = itemDetails.Id,
                CategoryId = itemDetails.CategoryId,
                ParentCategoryName = itemDetails.ParentCategoryName,
                CategoryName = itemDetails.CategoryName,
                ParentCategoryId = itemDetails.ParentCategoryId,
                Name = itemDetails.Name,
                DescriptionFileName = itemDetails.DescriptionFileName,
                FeaturesFileName = itemDetails.FeaturesFileName,
                SpecificationsFileName = itemDetails.SpecificationImageFileName,
                OverviewFileName = itemDetails.OverviewFileName,
                SpecificationImageFileName = itemDetails.SpecificationImageFileName,
                VariatyItems = itemDetails.Items,
                Attributes = itemDetails.Attributes,
                ImageUrl = itemDetails.ImageUrl,
                MemberPrice = itemDetails.MemberPrice,
                VarietyImages = itemDetails.Items.Select(p => p.Images).SelectMany(m => m).ToList()
            };
            //check for varierty
            ProductModelViewModel selectedItem = new ProductModelViewModel();
            if (Product.VariatyItems.Count > 0)
            {
                selectedItem = Product.VariatyItems.Where(w => w.IsOutOfStock == false).ToList()[0];
            }

            ShoppingCartEntryViewModel sc = new ShoppingCartEntryViewModel();
            sc.Product = itemDetails; // send in whole data block
            sc.ProductModel = selectedItem;
            sc.Quantity = 1;

            if (selectedItem.SpecialAmount != 0 && selectedItem.Amount > selectedItem.SpecialAmount)
            {
                sc.Price = selectedItem.SpecialAmount;
            }
            else
            {
                sc.Price = selectedItem.Amount;
            }

            _moduleManager.LoadModule(ModuleNames.CheckoutModule);
            var rm = new List<ShoppingCartEntryViewModel>();
            rm.Add(sc);
            var parameters = new NavigationParameters();
            parameters.Add("Cart", rm);

            if (rm != null)
                _regionManager.RequestNavigate(RegionNames.TransactionPrimaryRegion, "CheckoutView", parameters);

            //_eventAggregator.GetEvent<AddToCartEvent>().Publish(sc);
            //_moduleManager.LoadModule(ModuleNames.CheckoutModule);
            //_regionManager.RequestNavigate(RegionNames.TransactionPrimaryRegion, "CheckoutView");
            //_eventAggregator.GetEvent<CheckOutEvent>().Publish(new List<ShoppingCartEntryViewModel>(ShoppingEntries));

            //_moduleManager.LoadModule(ModuleNames.ProductDetailsModule);
            //var parameters = new NavigationParameters();
            //parameters.Add("ItemDetails", itemDetails);

            //if (itemDetails != null)
            //    _regionManager.RequestNavigate(RegionNames.TransactionPrimaryRegion, "ProductDetailsView", parameters);
        }

        private bool canLoadProductDetail(ItemDetails itemDetails)
        {
            return true;
        }

        private void loadProductDetail(ItemDetails itemDetails)
        {
            _moduleManager.LoadModule(ModuleNames.ProductDetailsModule);
            var parameters = new NavigationParameters();
            parameters.Add("ItemDetails", itemDetails);

            if (itemDetails != null)
                _regionManager.RequestNavigate(RegionNames.TransactionPrimaryRegion, "ProductDetailsView", parameters);
        }

        //private async void RefreshProductListingBySubCategory(SubCategorySearchModel obj)
        //{
        //    ItemRequestModel rm = new ItemRequestModel()
        //    {
        //        CategoryId = obj.SubCategoryDetails.CategoryId,
        //        SearchKey = obj.SearchKey
        //    };
        //    UpdateGridTitle(obj.SubCategoryDetails.ParentCategoryName, obj.SubCategoryDetails.CategoryName, obj.SearchKey);
        //    Product = new ObservableCollection<ItemDetails>(await _productService.GetItem(rm));
        //}
        private async void RefreshProductListingBySearch(SubCategorySearchModel vm)
        {
            Adding = true;
            
            RaisePropertyChanged("Adding"); 
            if (vm.SubCategoryDetails != null)
            {
                ItemRequestModel rm = new ItemRequestModel()
                {
                    CategoryId = vm.SubCategoryDetails.CategoryId,
                    SearchKey = vm.SearchKey,
                    CampaignId = vm.CampaignId
                };
                if (string.IsNullOrEmpty(vm.CampaignId))
                {
                    UpdateGridTitle(vm.SubCategoryDetails.ParentCategoryName, vm.SubCategoryDetails.CategoryName, vm.SearchKey);
                }
                else
                {
                    UpdateGridTitle(vm.CampaignName, vm.SubCategoryDetails.CategoryName, vm.SearchKey);
                }
                
                if (CurrentRequestId == vm.LastRequestId)
                {
                    var Products = await _productService.GetItem(rm);
                    Products = Products.OrderBy(x => x.IsAllOutOfStock).ThenBy(x => x.Name).ToList();
                    Product = new ObservableCollection<ItemDetails>(Products);
                }
            }
            else
            {
                UpdateGridTitle(string.Empty, string.Empty, vm.SearchKey, true);
                if (CurrentRequestId == vm.LastRequestId)
                {
                    Product = new ObservableCollection<ItemDetails>();
                }
            }
            _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(new ToggleViewModel());
            Reset = true;
            RaisePropertyChanged("Reset");
        }

        private void UpdateGridTitle(string PanelCategory, string SubCategory, string SearchKey, bool NoRecord = false)
        {
            if (!NoRecord)
            {
                if (!string.IsNullOrEmpty(SearchKey))
                {
                    GridTitle = "Product Search : " + SearchKey;
                }
                else
                {
                    GridTitle = PanelCategory + " / " + SubCategory;
                }
            }
            else
            {
                GridTitle = "No Item Found For : " + SearchKey;
            }
        }

        public void ClearProductListing()
        {
            Product = new ObservableCollection<ItemDetails>();
        }

        public void UpdateLastRequestId(Guid LastRequestId)
        {
            CurrentRequestId = LastRequestId;
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            var Filters = navigationContext.Parameters["Filter"] as SubCategorySearchModel;
            if (Filters != null)
                RefreshProductListingBySearch(Filters);
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            var itemDetails = navigationContext.Parameters["Filter"] as SubCategorySearchModel;
            if (itemDetails != null)
                return true;
            else
                return false;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
        }
    }
}