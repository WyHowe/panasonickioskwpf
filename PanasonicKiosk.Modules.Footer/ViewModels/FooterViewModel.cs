﻿using PanasonicKiosk.Core;
using PanasonicKiosk.Core.Events;
using PanasonicKiosk.Core.Models;
using Prism.Commands;
using Prism.Events;
using Prism.Modularity;
using Prism.Mvvm;
using Prism.Regions;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace PanasonicKiosk.Modules.Footer.ViewModels
{
    public class FooterViewModel : BindableBase
    {
        private string _SearchBox;
        private int _totalitem;

        private IEventAggregator _eventAggregator;
        private IRegionManager _regionManager;
        private IModuleManager _moduleManager;
        private DelegateCommand _ViewCardCommand;
        private DelegateCommand _SearchItemCommand;
        private ShoppingCartModel _ShoppingCart;
        private bool _totalitemvisibility;

        public FooterViewModel(IEventAggregator eventAggregator, IRegionManager regionManager, IModuleManager moduleManager)
        {
            _eventAggregator = eventAggregator;
            _regionManager = regionManager;
            _moduleManager = moduleManager;
            _ViewCardCommand = new DelegateCommand(loadShoppingCart, canLoadShoppingCard);
            _SearchItemCommand = new DelegateCommand(loadSearchItem, canSearchitem);
            _eventAggregator.GetEvent<AddToCartEvent>().Subscribe(AddToCart);
            _eventAggregator.GetEvent<ClearCartCommandEvent>().Subscribe(ClearCart);
            _eventAggregator.GetEvent<ClearCartEvent>().Subscribe(ClearCart);
            _eventAggregator.GetEvent<UpdateCartDetailsEvent>().Subscribe(UpdateCart);
            _eventAggregator.GetEvent<ClearSearchBoxEvent>().Subscribe(ClearSearchBox);
            ShoppingCart = new ShoppingCartModel();
            CartImageUrl = "pack://application:,,,/Assets/add_to_cart.png";
        }

        private bool canLoadShoppingCard()
        {
            if (ShoppingCart.ShoppingEntries.Count > 0)
                return true;
            else
                return false;
        }

        private void loadShoppingCart()
        {
            _moduleManager.LoadModule(ModuleNames.ShoppingCartModule);
            _eventAggregator.GetEvent<ShoppingCartShowEvent>().Publish(ShoppingCart);
            _regionManager.RequestNavigate(RegionNames.TransactionPrimaryRegion, "LoadingScreenView");
            //await Task.Delay(1000);
            _regionManager.RequestNavigate(RegionNames.TransactionPrimaryRegion, "ShoppingCartView");

            //_regionManager.RequestNavigate(RegionNames.TransactionPrimaryRegion, "ShoppingCartView");

            //var content = JsonConvert.SerializeObject(itemDetails);
        }

        private void loadSearchItem()
        {
            if(!string.IsNullOrEmpty(SearchBox) && !string.IsNullOrWhiteSpace(SearchBox))
            {
                ToggleViewModel ps = new ToggleViewModel()
                {
                    IsShow = true,
                    Header = "Searching",
                    Description = "Key word :" + SearchBox
                };

                _eventAggregator.GetEvent<ToggleLoadingScreenEvent>().Publish(ps);
                _eventAggregator.GetEvent<SearchItemEvent>().Publish(SearchBox);
                //_regionManager.RequestNavigate(RegionNames.TransactionPrimaryRegion, "ProductListingView");
            }
            // _moduleManager.LoadModule(ModuleNames.ProductListingModule);

        }

        private string _CartImageUrl;

        public string CartImageUrl
        {
            get { return _CartImageUrl; }
            set
            {
                SetProperty(ref _CartImageUrl, value);
            }
        }

        public string SearchBox
        {
            get { return _SearchBox; }
            set { SetProperty(ref _SearchBox, value);
                _SearchItemCommand.RaiseCanExecuteChanged();
            }

            
        }

        private bool canSearchitem()
        {
            //return !string.IsNullOrEmpty(SearchBox) && !string.IsNullOrWhiteSpace(SearchBox);
            return true;
        }

        public void ClearSearchBox()
        {
            SearchBox = "";
        }

        public bool TotalItemVisibility
        {
            get { return _totalitemvisibility; }
            set { SetProperty(ref _totalitemvisibility, value); }
        }

        public int TotalItem
        {
            get
            {
                return _totalitem;
            }
            set
            {
                SetProperty(ref _totalitem, value);
                _ViewCardCommand.RaiseCanExecuteChanged();
            }
            //set { SetProperty(ref _totalitem, value); }
            //set
            //{
            //    //_totalitem = value;
            //    //RaisePropertyChanged("TotalItem");
            //    //_ViewCardCommand.RaiseCanExecuteChanged();
            //}
        }

        public ICommand ViewCartCommand
        {
            get
            {
                return _ViewCardCommand;
            }
        }

        public ICommand SearchItemCommand
        {
            get
            {
                return _SearchItemCommand;
            }
        }

        public ShoppingCartModel ShoppingCart
        {
            get
            {
                return _ShoppingCart;
            }
            set
            {
                SetProperty(ref _ShoppingCart, value);
            }
        }

        public void ClearCart()
        {
            ShoppingCart.ShoppingEntries = new List<ShoppingCartEntryViewModel>();
            ReCalculateCost();
        }

        public void UpdateCart(ShoppingCartEntryViewModel ExistingEntry)
        {
            var ExistingItem = ShoppingCart.ShoppingEntries
                .Where(x => x.ProductModel.Id == ExistingEntry.ProductModel.Id)
                .FirstOrDefault();
            if (ExistingItem != null)
            {
                if (ExistingEntry.Quantity == 0)
                {
                    ShoppingCart.ShoppingEntries.Remove(ExistingItem);
                }
                else
                {
                    if(ExistingItem.Quantity + ExistingEntry.Quantity < ExistingItem.ProductModel.AvailableQuantity)
                    {
                        ExistingItem.Quantity = ExistingEntry.Quantity;
                    }
                }
            }
            ReCalculateCost();
        }

        public void AddToCart(ShoppingCartEntryViewModel NewEntry)
        {
            //currently no shipping
            var FixedHardCodedShipping = 0;
            var ItemTaxPercent = 0;
            var ShoppingTaxPercentage = 0;
            NewEntry.ItemTaxPercent = ItemTaxPercent;
            NewEntry.ShippingCost = FixedHardCodedShipping;
            NewEntry.ShoppingTaxPercentage = ShoppingTaxPercentage;

            var ExistingItem = ShoppingCart.ShoppingEntries.Where(x => x.ProductModel.Id == NewEntry.ProductModel.Id).FirstOrDefault();
            if (ExistingItem == null)
            {
                if (NewEntry.ProductModel.Images.Count == 0)
                {
                    NewEntry.ProductModel.Images.Add("pack://application:,,,/Assets/no_image_available.png");
                }
                ShoppingCart.ShoppingEntries.Add(NewEntry);

                //ExistingItem = NewEntry;
            }
            else
            {
                if (ExistingItem.Quantity + NewEntry.Quantity <= ExistingItem.ProductModel.AvailableQuantity)
                {
                    ExistingItem.Quantity += NewEntry.Quantity;
                }
                //ExistingItem.Quantity += NewEntry.Quantity;
            }
            ReCalculateCost();
        }

        private void ReCalculateCost()
        {
            ShoppingCart.Summary = new ShoppingCartCostsSummary();
            if (ShoppingCart.ShoppingEntries.Count == 0)
            {
                TotalItem = 0;
                TotalItemVisibility = false;
            }
            foreach (var Item in ShoppingCart.ShoppingEntries)
            {
                var MemberAmount = Item.Price;
                Item.PriceString = "RM " + Item.Price.ToString("N2");
                Item.TotalPriceString = "RM " + (Item.Price * Item.Quantity).ToString("N2");

                var Amount = MemberAmount;
                var ItemSubTotal = (Item.Quantity * Amount);
                var ItemTaxTotal = ItemSubTotal * Item.ItemTaxPercent / 100;
                var ItemShippingTotal = Item.ShippingCost;
                var ItemShippingTax = ItemShippingTotal * (Item.ShoppingTaxPercentage / 100);

                ShoppingCart.Summary.ItemsSubtotal += ItemSubTotal;
                ShoppingCart.Summary.ItemsTax += ItemTaxTotal;
                ShoppingCart.Summary.Shipping += ItemShippingTotal;
                ShoppingCart.Summary.ShippingTax += ItemShippingTax;
                ShoppingCart.Summary.Total += (ItemSubTotal + ItemShippingTotal);
                ShoppingCart.Summary.TotalTax += (ItemShippingTax + ItemTaxTotal);
                TotalItem = ShoppingCart.ShoppingEntries.Sum(x => x.Quantity);

                if (TotalItem > 0)
                {
                    TotalItemVisibility = true;
                }
                else
                {
                    TotalItemVisibility = false;
                }
            }
        }
    }
}