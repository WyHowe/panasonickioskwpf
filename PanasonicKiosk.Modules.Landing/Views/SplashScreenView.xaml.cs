﻿using PanasonicKiosk.Core.Models;
using PanasonicKiosk.Modules.Landing.Helper;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace PanasonicKiosk.Modules.Landing.Views
{
    /// <summary>
    /// Interaction logic for ViewA.xaml
    /// </summary>
    public partial class SplashScreenView : UserControl
    {
        public CarouselImageViewModel LandingCarousel = new CarouselImageViewModel();
        private static string[] TransitionEffects = new[] { "Fade" };
        private string TransitionType;

        private int EffectIndex = 0, IntervalTimer = 5;

        //private int CurrentSourceIndex, CurrentCtrlIndex, EffectIndex = 0, IntervalTimer = 1;
        protected Point SwipeStart;

        protected Point SwipeEnd;
        protected bool AlreadySwipe;

        public SplashScreenView()
        {
            InitializeComponent();

            //IntervalTimer = Convert.ToInt32(ConfigurationManager.AppSettings["IntervalTime"]);

            // External Image Path
            //LandingCarousel.ImagesAddresses = new List<string>()
            //{
            //    "Assets/PanasonicAdvert/landing1.png",
            //    "Assets/PanasonicAdvert/landing2.jpg",
            //    "Assets/PanasonicAdvert/landing3.jpg",
            //};
            //LandingCarousel.CarouselImageControls = new[] { LandingSliderImageOne, LandingSliderImageTwo };

            //LoadImageFolder("", LandingCarousel);

            //LandingCarousel.timerCarouselImageChange = new DispatcherTimer();
            //LandingCarousel.timerCarouselImageChange.Interval = new TimeSpan(0, 0, IntervalTimer);
            //LandingCarousel.timerCarouselImageChange.Tick += new EventHandler(timerLandingImageChange_Tick);
            //Loaded += Window_Loaded;
        }

        //private void Window_Loaded(object sender, RoutedEventArgs e)
        //{
        //    PlaySlideShow(LandingCarousel);
        //    LandingCarousel.timerCarouselImageChange.IsEnabled = true;
        //}

        #region View Specific Slide Show.. no MVVM

        private void LoadImageFolder(string folder, CarouselImageViewModel ImageCarousel)
        {
            var sw = System.Diagnostics.Stopwatch.StartNew();

            Random r = new Random();

            var sources = from filepath in ImageCarousel.ImagesAddresses
                          select ImageHelper.CreateImageSource("pack://application:,,,/" + filepath, true);

            ImageCarousel.ImagesSources.Clear();
            ImageCarousel.ImagesSources.AddRange(sources);
            sw.Stop();
            Console.WriteLine("Total time to load {0} images: {1}ms", ImageCarousel.ImagesSources.Count, sw.ElapsedMilliseconds);
        }

        private void timerLandingImageChange_Tick(object sender, EventArgs e)
        {
            PlaySlideShow(LandingCarousel);
        }

        private void PlaySlideShow(CarouselImageViewModel ImageCarousel)
        {
            try
            {
                if (ImageCarousel.ImagesSources.Count == 0)
                    return;
                var oldCtrlIndex = ImageCarousel.CurrentCtrlIndex;
                ImageCarousel.CurrentCtrlIndex = (ImageCarousel.CurrentCtrlIndex + 1) % 2;
                ImageCarousel.CurrentSourceIndex = (ImageCarousel.CurrentSourceIndex + 1) % ImageCarousel.ImagesSources.Count;

                Image imgFadeOut = ImageCarousel.CarouselImageControls[oldCtrlIndex];
                Image imgFadeIn = ImageCarousel.CarouselImageControls[ImageCarousel.CurrentCtrlIndex];
                ImageSource newSource = ImageCarousel.ImagesSources[ImageCarousel.CurrentSourceIndex];
                imgFadeIn.Source = newSource;

                TransitionType = TransitionEffects[EffectIndex].ToString();

                Storyboard StboardFadeOut = (Resources[string.Format("{0}Out", TransitionType.ToString())] as Storyboard).Clone();
                StboardFadeOut.Begin(imgFadeOut);
                Storyboard StboardFadeIn = Resources[string.Format("{0}In", TransitionType.ToString())] as Storyboard;
                StboardFadeIn.Begin(imgFadeIn);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        public void SlideShowNext(CarouselImageViewModel ImageCarousel)
        {
            ImageCarousel.timerCarouselImageChange.Stop();
            PlaySlideShow(ImageCarousel);
            ImageCarousel.timerCarouselImageChange.Start();
        }

        private void mePlayer_MediaEnded(object sender, RoutedEventArgs e)
        {
            mePlayer.Position = new TimeSpan(0, 0, 1);
        }

        private void mePlayer_Loaded(object sender, RoutedEventArgs e)
        {
            mePlayer.Play();
        }

        public void SlideShowPrevious(CarouselImageViewModel ImageCarousel)
        {
            ImageCarousel.timerCarouselImageChange.Stop();
            if (ImageCarousel.CurrentSourceIndex - 2 < 0 && ImageCarousel.CurrentSourceIndex != 1)
            {
                ImageCarousel.CurrentSourceIndex = ImageCarousel.ImagesSources.Count - 2;
            }
            else if (ImageCarousel.CurrentSourceIndex == 1)
            {
                ImageCarousel.CurrentSourceIndex = ImageCarousel.ImagesSources.Count - 1;
            }
            else
            {
                ImageCarousel.CurrentSourceIndex = ImageCarousel.CurrentSourceIndex - 2;
            }
            PlaySlideShow(ImageCarousel);
            ImageCarousel.timerCarouselImageChange.Start();
        }

        #endregion View Specific Slide Show.. no MVVM
    }
}