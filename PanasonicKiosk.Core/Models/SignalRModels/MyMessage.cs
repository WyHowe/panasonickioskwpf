﻿namespace PanasonicKiosk.Core.Models.SignalRModels
{
    public class MyMessage
    {
        public string Name { get; set; }
        public string Message { get; set; }
    }
}