﻿using Microsoft.AspNet.SignalR.Client;
using PanasonicKiosk.Core.Helper;
using PanasonicKiosk.Core.IHubSync.Client;
using PanasonicKiosk.Core.Models.SignalRModels;
using System;

namespace PanasonicKiosk.Core.HubClients
{
    public class PanasonicKioskHubClient : BaseHubClient, ISendHubSync, IReceiveHubSync
    {
        public event Action<MyMessage> ReceivedMessageEvent;

        public event Action<string> ReceivedPingEvent;

        public PanasonicKioskHubClient()
        {
            Init();
        }

        public new void Init()
        {
            HubConnectionUrl = "https://localhost:44369";
            HubProxyName = "KioskNotificationHub";
            HubTraceLevel = TraceLevels.None;
            HubTraceWriter = Console.Out;
            MachineId = FingerPrint.Value();
            base.Init();

            //_myHubProxy.On<string, string>("AddMessage", Receive_AddMessage);
            //_myHubProxy.On("Heartbeat", Receive_Heartbeat);
            //_myHubProxy.On("Heartbeat", Receive_Heartbeat);
            //_myHubProxy.On<HelloModel>("SendHelloObject", Receive_SendHelloObject);
            _myHubProxy.On<string>("GetPinged", Receive_Ping);
            StartHubInternal();
        }

        public override void StartHub()
        {
            _hubConnection.Dispose();
            Init();
        }

        public void Receive_Ping(string ping)
        {
            if (ReceivedPingEvent != null) ReceivedPingEvent.Invoke(ping);
        }

        //public void Receive_AddMessage(string name, string message)
        //{
        //    if (ReceivedMessageEvent != null) ReceivedMessageEvent.Invoke(new MyMessage { Name = name , Message = message});
        //}

        //public void Receive_Heartbeat()
        //{
        //    if (ReceivedMessageEvent != null) ReceivedMessageEvent.Invoke(new MyMessage { Name = "Heartbeat", Message = "recieved" });
        //}

        //public void Receive_SendHelloObject(HelloModel hello)
        //{
        //    if (ReceivedMessageEvent != null) ReceivedMessageEvent.Invoke(new MyMessage { Name = hello.Age.ToString(), Message = hello.Molly });
        //}

        //public void AddMessage(string name, string message)
        //{
        //    _myHubProxy.Invoke("AddMessage", name, message).ContinueWith(task =>
        //    {
        //        if (task.IsFaulted)
        //        {
        //        }
        //    }).Wait();
        //}

        public void Heartbeat()
        {
            _myHubProxy.Invoke("Heartbeat").ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                }
            }).Wait();
        }

        public void PaymentTerminalHeartBeat(TerminalStatusModel rm)
        {
            _myHubProxy.Invoke<TerminalStatusModel>("TerminalStatus", rm).ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                }
            }).Wait();
        }

        public void SendHelloObject(HelloModel hello)
        {
            _myHubProxy.Invoke<HelloModel>("SendHelloObject", hello).ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                }
            }).Wait();
        }
    }
}