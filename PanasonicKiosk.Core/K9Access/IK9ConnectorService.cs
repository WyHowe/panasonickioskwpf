﻿namespace PanasonicKiosk.Core.K9Access
{
    public interface IK9ConnectorService
    {
        void RebootK9();

        void ConnectionTest();

        void ConnectToAvailablePorts();

        void DisconnectPort();

        void EnableK9();

        void ExecuteCommandState(string Amount = "0");

        void GetPortSellection();

        void InitializeSales();

        void ProceedSales();

        void RequestPayment();

        void Settlement();
    }
}