﻿using PanasonicKiosk.Core;
using PanasonicKiosk.Core.Events;
using PanasonicKiosk.Core.Models;
using Prism.Commands;
using Prism.Events;
using Prism.Modularity;
using Prism.Mvvm;
using Prism.Regions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace PanasonicKiosk.Modules.ShoppingCart.ViewModels
{
    public class ShoppingCartViewModel : BindableBase, INavigationAware
    {
        private IModuleManager _moduleManager;
        private IEventAggregator _eventAggregator;
        private IRegionManager _regionManager;
        private string _ShippingAndTax;
        private string _SubTotal;
        private string _GrandTotal;
        private bool _CartIsEmpty;
        private ObservableCollection<ShoppingCartEntryViewModel> _ShoppingEntries;
        private DelegateCommand<ItemDetails> _loadProductDetailCommand;
        private DelegateCommand<ShoppingCartEntryViewModel> _removeFromCartCommand;
        private DelegateCommand _ClearCartCommand;

        private string _DiscountImageUrl;

        public ShoppingCartViewModel(IEventAggregator eventAggregator, IRegionManager regionManager, IModuleManager moduleManager)
        {
            _moduleManager = moduleManager;
            _regionManager = regionManager;
            _eventAggregator = eventAggregator;
            _eventAggregator.GetEvent<ShoppingCartShowEvent>().Subscribe(PopulateShoppingCart);
            _eventAggregator.GetEvent<RemoveItemFromCartEvent>().Subscribe(RemoveItem);
            _eventAggregator.GetEvent<KeepItemFromCartEvent>().Subscribe(KeepItem);

            _MinusQuantityCommand = new DelegateCommand<ShoppingCartEntryViewModel>(MinusQuantity, canMinusQuantity);
            _AddQuantityCommand = new DelegateCommand<ShoppingCartEntryViewModel>(AddQuantity, canAddQuantity);
            _loadProductDetailCommand = new DelegateCommand<ItemDetails>(loadProductDetail, canLoadProductDetail);
            _CheckOutCommand = new DelegateCommand(CheckOut, CanCheckOut);
            _ClearCartCommand = new DelegateCommand(ClearCart);
            _removeFromCartCommand = new DelegateCommand<ShoppingCartEntryViewModel>(removeFromShoppingCart, canRemoveFromShoppingCart);

            _DiscountImageUrl = "pack://application:,,,/Assets/sales_bookmark.png";
        }

        public AddressViewModel DeliveryAddress { get; set; }
        public ShoppingCartCostsSummary Summary { get; set; }

        public ObservableCollection<ShoppingCartEntryViewModel> ShoppingEntries
        {
            get
            {
                return _ShoppingEntries;
            }
            set
            {
                SetProperty(ref _ShoppingEntries, value);
            }
        }

        public string DiscountImageUrl
        {
            get { return _DiscountImageUrl; }
            set
            {
                SetProperty(ref _DiscountImageUrl, value);
            }
        }

        public bool CartIsEmpty
        {
            get { return _CartIsEmpty; }

            set
            {
                SetProperty(ref _CartIsEmpty, value);
            }
        }

        public string ShippingAndTax
        {
            get { return _ShippingAndTax; }

            set
            {
                SetProperty(ref _ShippingAndTax, value);
            }
        }

        public string SubTotal
        {
            get { return _SubTotal; }

            set
            {
                SetProperty(ref _SubTotal, value);
            }
        }

        public string GrandTotal
        {
            get { return _GrandTotal; }

            set
            {
                SetProperty(ref _GrandTotal, value);
            }
        }

        public ICommand LoadProductDetailCommand
        {
            get
            {
                return _loadProductDetailCommand;
            }
        }

        public ICommand ClearCartCommand
        {
            get
            {
                return _ClearCartCommand;
            }
        }

        private bool canLoadProductDetail(ItemDetails itemDetails)
        {
            return true;
        }

        private void loadProductDetail(ItemDetails itemDetails)
        {
            //_eventAggregator.GetEvent<ProductDetailShowEvent>().Publish(itemDetails);

            _moduleManager.LoadModule(ModuleNames.ProductDetailsModule);
            var parameters = new NavigationParameters();
            parameters.Add("ItemDetails", itemDetails);

            if (itemDetails != null)
                _regionManager.RequestNavigate(RegionNames.TransactionPrimaryRegion, "ProductDetailsView", parameters);
        }

        public ICommand RemoveFromShoppingCart
        {
            get
            {
                return _removeFromCartCommand;
            }
        }

        private bool canRemoveFromShoppingCart(ShoppingCartEntryViewModel itemDetails)
        {
            return true;
        }

        private void removeFromShoppingCart(ShoppingCartEntryViewModel itemDetails)
        {
            //_eventAggregator.GetEvent<ProductDetailShowEvent>().Publish(itemDetails);
            itemDetails.Quantity = 0;
            ShoppingEntries.Remove(itemDetails);
            _eventAggregator.GetEvent<UpdateCartDetailsEvent>().Publish(itemDetails);
            _CheckOutCommand.RaiseCanExecuteChanged();
            //RaisePropertyChanged("ShoppingEntries");
            if (ShoppingEntries.Count == 0)
            {
                CartIsEmpty = true;
            }

            ReCalculateTotal(ShoppingEntries);
        }

        private DelegateCommand<ShoppingCartEntryViewModel> _AddQuantityCommand;

        public ICommand AddQuantityCommand
        {
            get
            {
                return _AddQuantityCommand;
            }
        }

        private DelegateCommand<ShoppingCartEntryViewModel> _MinusQuantityCommand;

        public ICommand MinusQuantityCommand
        {
            get
            {
                return _MinusQuantityCommand;
            }
        }

        private DelegateCommand _CheckOutCommand;

        public ICommand CheckOutCommand
        {
            get
            {
                return _CheckOutCommand;
            }
        }

        public bool canMinusQuantity(ShoppingCartEntryViewModel obj)
        {
            if (obj.Quantity >= 1)
            {
                return true;
            }
            return false;
        }

        public bool CanCheckOut()
        {
            if (ShoppingEntries != null && ShoppingEntries.Count > 0)
            {
                return true;
            }
            return false;
        }

        public void CheckOut()
        {
            _moduleManager.LoadModule(ModuleNames.CheckoutModule);
            var rm = new List<ShoppingCartEntryViewModel>(ShoppingEntries);
            var parameters = new NavigationParameters();
            parameters.Add("Cart", rm);

            if (rm != null)
                _regionManager.RequestNavigate(RegionNames.TransactionPrimaryRegion, "CheckoutView", parameters);

            //_moduleManager.LoadModule(ModuleNames.CheckoutModule);
            //_regionManager.RequestNavigate(RegionNames.TransactionPrimaryRegion, "CheckoutView");
            //_eventAggregator.GetEvent<CheckOutEvent>().Publish(new List<ShoppingCartEntryViewModel>(ShoppingEntries));
        }

        public void ClearCart()
        {
            ShoppingEntries = new ObservableCollection<ShoppingCartEntryViewModel>();
            CartIsEmpty = true;
            _eventAggregator.GetEvent<ClearCartCommandEvent>().Publish();
            _CheckOutCommand.RaiseCanExecuteChanged();

            ReCalculateTotal(ShoppingEntries);
        }

        public bool canAddQuantity(ShoppingCartEntryViewModel obj)
        {
            //return true;
            if (obj.ProductModel.AvailableQuantity > obj.Quantity)
            {
                return true;
            }
            return false;
        }

        public void MinusQuantity(ShoppingCartEntryViewModel obj)
        {
            AddMinusQuantity(obj, true);

            _MinusQuantityCommand.RaiseCanExecuteChanged();
            _AddQuantityCommand.RaiseCanExecuteChanged();
        }

        public void AddQuantity(ShoppingCartEntryViewModel obj)
        {
            AddMinusQuantity(obj, false);

            _AddQuantityCommand.RaiseCanExecuteChanged();
            _MinusQuantityCommand.RaiseCanExecuteChanged();
        }

        public void AddMinusQuantity(ShoppingCartEntryViewModel obj, bool IsMinus)
        {
            if (IsMinus)
            {
                obj.Quantity = obj.Quantity - 1;
            }
            else
            {
                obj.Quantity = obj.Quantity + 1;
            }
            if (obj.Quantity == 0)
            {
                _moduleManager.LoadModule(ModuleNames.ClearConfirmationModule);
                _eventAggregator.GetEvent<ShowClearConfirmationScreenEvent>().Publish();
                var parameters = new NavigationParameters();
                parameters.Add("Entry", obj);
                _regionManager.RequestNavigate(RegionNames.ClearConfirmationRegion, "ClearConfirmationView", parameters);
                //removeFromShoppingCart(obj);
                return;
            }
            var ItemSubTotal = obj.Price * obj.Quantity;
            obj.TotalPriceString = "RM " + ItemSubTotal.ToString("N2");

            _eventAggregator.GetEvent<UpdateCartDetailsEvent>().Publish(obj);
            RaisePropertyChanged("ShoppingEntries");
            ReCalculateTotal(ShoppingEntries);
        }

        public void RemoveItem(ShoppingCartEntryViewModel Entry)
        {
            removeFromShoppingCart(Entry);
        }

        public void KeepItem(ShoppingCartEntryViewModel Entry)
        {
            var KeepedEntry = ShoppingEntries
                .Where(x => x.SelectedProductModel == Entry.SelectedProductModel)
                .FirstOrDefault();
            KeepedEntry.Quantity = 1;

            _MinusQuantityCommand.RaiseCanExecuteChanged();
            //_AddQuantityCommand.RaiseCanExecuteChanged();
        }

        private void PopulateShoppingCart(ShoppingCartModel obj)
        {
            CartIsEmpty = false;
            ShoppingEntries = new ObservableCollection<ShoppingCartEntryViewModel>(obj.ShoppingEntries);
            foreach (var Entries in ShoppingEntries)
            {
                Entries.IsMultiProductModel = true;
                Entries.IsSingleProductModel = false;
                Entries.SelectedProductModel = Entries.ProductModel.Id;
                if (Entries.Product.Items.Count == 1)
                {
                    Entries.IsMultiProductModel = false;
                    Entries.IsSingleProductModel = true;
                }
            }
            ReCalculateTotal(ShoppingEntries);
            _CheckOutCommand.RaiseCanExecuteChanged();
            RaisePropertyChanged("ShoppingEntries");
        }

        private void ReCalculateTotal(ObservableCollection<ShoppingCartEntryViewModel> entries)
        {
            Summary = new ShoppingCartCostsSummary();
            foreach (var Item in entries)
            {
                var MemberAmount = Item.Price;
                Item.PriceString = "RM " + Item.Price.ToString("N2");
                Item.TotalPriceString = "RM " + (Item.Price * Item.Quantity).ToString("N2");

                var Amount = MemberAmount;
                var ItemSubTotal = (Item.Quantity * Amount);
                var ItemTaxTotal = ItemSubTotal * Item.ItemTaxPercent / 100;
                var ItemShippingTotal = Item.ShippingCost;
                var ItemShippingTax = ItemShippingTotal * (Item.ShoppingTaxPercentage / 100);

                Summary.ItemsSubtotal += ItemSubTotal;
                Summary.ItemsTax += ItemTaxTotal;
                Summary.Shipping += ItemShippingTotal;
                Summary.ShippingTax += ItemShippingTax;
                Summary.Total += (ItemSubTotal + ItemShippingTotal);
                Summary.TotalTax += (ItemShippingTax + ItemTaxTotal);
            }

            GrandTotal = "RM " + ((Summary.Total + Summary.TotalTax)).ToString("N2");
            SubTotal = "RM " + (Summary.ItemsSubtotal).ToString("N2");
            ShippingAndTax = "RM " + (Summary.Shipping + Summary.ShippingTax + Summary.TotalTax).ToString("N2");
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
        }
    }
}